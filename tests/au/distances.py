from __future__ import division
# -*- coding: utf-8 -*-
from time import sleep
from astropy import units as u
from math import log
# radii = [
#     {'name': 'Planet radius', 'radius': 3500 * u.km},
#     {'name': 'Planetary system radius', 'radius': 7 * u.au},
#     {'name': 'Galaxy radius', 'radius': 33000 * u.lyr},
#     {'name': 'Galaxy cluster radius', 'radius': 3033 * u.kpc},
# ]
radii = [
    {'name': 'Heliosphere', 'radius': 61 * u.au},
    {'name': 'Galaxy radius', 'radius': 30000 * u.lyr},
    {'name': 'Galaxy cluster radius', 'radius': 3033 * u.kpc},
]

diameters = {
    'Universe': 14000 * u.Mpc * 2,
    'Sun': 1391000 * u.km,
    'Earth': 12756.2 * u.km,
    'Moon': 3474.8 * u.km,
    'Mars': 6792 * u.km
}

print u.Mpc.to(u.lyr)
s

facts = {
    'galaxy_density': 0.01 * u.Mpc**(-3)
}

print facts

# total = radii[1]['radius'].to(u.Mpc) / radii[0]['radius'].to(u.Mpc)

# for i in xrange(1, 100):
#     print (i**log(i))
#     result = radii[0]['radius'] * (i**i) * 1.
#     if result < u.lyr * 1:
#         print result.to(u.au)
#     elif result < u.Mpc * 1:
#         print result.to(u.lyr)
#     elif result < u.Gpc * 1:
#         print result.to(u.Mpc)
#     if result < 0:
#         print result, (i**i)
#     if result > radii[-1]['radius']:
#         break


def main():
    print '\n== Sizes ================='
    for i, val in enumerate(radii):
        if i == len(radii)-1:
            print '{0} = {1} = {2}'.format(val['name'], val['radius'], val['radius'].to(u.Mpc))
        else:
            print '{0} = {1}'.format(val['name'], val['radius'])
    print '-- Ratios ----------------'


    last = {}
    ratios = []
    for i in range(1, len(radii)):
        cur = radii[i-1]
        nxt = radii[i]
        ratio = nxt['radius'].to(u.pc) / cur['radius'].to(u.pc)
        last.update({
            'ratio': ratio,
            'name': nxt['name'],
            'value': nxt['radius']
        })
        ratios.append(ratio)
        print '{0} / {1} = {2}, [ratio^(-1) = {3}]'.format(nxt['name'], cur['name'], ratio, 1/ratio)
    print '--------------------------'
    mean_ratio = sum(ratios) / len(ratios)
    print 'Mean ratio = {0}'.format(mean_ratio)


    print '==========================\n'
    observable_universe_radius = 14.3 * u.Gpc
    print 'Whole Universe = Mean ratio * {0}:'.format(radii[-1]['name'])
    whole_universe = (last['value'] * mean_ratio).to(u.Gpc)
    print '= {0}\n= {1}'.format(whole_universe, whole_universe.to(u.lyr))
    print '--------------------------'
    print 'Observable universe radius = {0}'.format(observable_universe_radius)
    print 'Whole Universe / Observable Universe: {0}'.format(whole_universe / observable_universe_radius)


    print '\n\n\n=== Additional small quantities ==='

    n_small = (radii[0]['radius'] / mean_ratio).to(u.meter)
    nn_small = (n_small / mean_ratio).to(u.micron)
    nnn_small = (nn_small / mean_ratio).to(u.picometer)
    nnnn_small = (nnn_small / mean_ratio).to(u.femtometer)
    nnnnn_small = (nnnn_small / mean_ratio).to(u.zm)
    nnnnnn_small = (nnnnn_small / mean_ratio).to(u.yoctometer)
    nnnnnnn_small = (nnnnnn_small / mean_ratio).to(u.meter)
    nnnnnnnn_small = (nnnnnnn_small / mean_ratio).to(u.meter)

    print n_small, '(meters) 10^1'
    print nn_small, '(microns) 10^-6 = Diameter of a minicolumn in the human cortex'
    print nnn_small, '(picometers) 10^-12 ~= Van der Waals radius of a neutral hydrogen atom'
    print nnnn_small, '(femtometers) 10^-15 = Size of proton / 2'
    print nnnnn_small, '(zeptometers) 10^-21 = Radius of effective cross section for a 20 GeV neutrino scattering off a nucleon'
    print nnnnnn_small, '(yoctometers) 10^-24 = ?'
    print nnnnnnn_small, '= ?'
    print nnnnnnnn_small, '= (5.4741 meters * 10^-38) < Planck length'
    print '==================================='

main()