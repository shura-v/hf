from matplotlib.pyplot import show

from market.market import Market


m = Market('nyse_60')

keys = ['most|indexes/RUA_X|L', 'most|indexes/RUA_X|H', 'most|indexes/RUA_X|C', 'most|indexes/RUA_X|O']

for k in keys:
    m.get_source(*k.split('|')).plot(legend=k)
show()