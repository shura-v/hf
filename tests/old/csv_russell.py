import os, re

folder = 'd:\\data\\lists\\'

paths = [
    "russell1000.txt",
    "russell2000.txt",
    "russell3000.txt"
]


def main():
    quotes_all = set()
    for path in paths:
        quotes = convert(os.path.join(folder, path))
        quotes_all = quotes_all.union(quotes)
    save(quotes_all)


def convert(path):
    s = set()
    sep = '___'
    with open(path) as f:
        data = f.read()
    data = data.replace('\n', sep).split("Company Ticker")
    data.pop(0)
    for i in range(len(data)):
        m = re.search('As of .* .', data[i])
        if m:
            data[i] = data[i][:data[i].index(m.group(0))]
    data = [line.strip() for line in str.join('', data).split(sep)]
    for line in data:
        if len(line) == 0: continue
        s.add(line.split(' ')[-1])
    return s


def save(quotes):
    quotes = list(quotes)
    quotes.sort()
    save_path = os.path.join(folder, 'russell.csv')
    if os.path.isfile(save_path):
        os.unlink(save_path)
    string = str.join('\n', quotes)
    with open(save_path, "w") as f:
        f.write(string)


if __name__ == '__main__':
    main()