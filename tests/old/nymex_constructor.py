from datetime import datetime
from calendar import month_name
from pandas import date_range

current_month = datetime.now().month
months = ['F', 'G', 'H', 'J', 'K', 'M', 'N', 'Q', 'U', 'V', 'X', 'Z']
years = range(14, 16)
prefixes = {
}

d_range = date_range(datetime.now(), freq='M', periods=6)


# def prefix(p):
#     print '=========', prefixes[p], '========='
#     for d in d_range:
#         string = '{prefix}{month}{year}'.format(year=str(d.year)[-2:], prefix=p, month=months[d.month-1])
#         string2 = '{year:4}-{month:02}'.format(year=d.year, month=d.month)
#         print string
#
# prefix('+PA')

_keys = prefixes.keys()
keys = []
for k in _keys:
    arr = ['Q'+k, '+'+k,]
    keys += []
keys.sort()
keys = '#\n'.join(keys)
