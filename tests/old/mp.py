import multiprocessing
from multiprocessing import cpu_count
import numpy as np
from timeit import timeit


def proc(i):
    return {'i': i * 10}


def run_pool():
    pool = multiprocessing.Pool(processes=cpu_count())
    a = range(cpu_count())
    outputs = pool.map(proc, a)
    print outputs


class Process(multiprocessing.Process):
    def __init__(self, id):
        super(Process, self).__init__()
        self.id = id
        self.res = None

    def run(self):
        self.res = self.id


def run_processes():
    processes = [Process(i) for i in range(cpu_count())]
    [p.start() for p in processes]
    [p.join() for p in processes]
    print [p.res for p in processes]


if __name__ == '__main__':
    run_pool()
    print cpu_count()
    #run_processes()
