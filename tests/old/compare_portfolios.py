import os


folder = 'd:\\data\\portfolios'
portfolios = ['russell_d', 'russell3000']


def main():
    sets = []
    for i in range(len(portfolios)):
        s = set()
        for ff in os.listdir(os.path.join(folder, portfolios[i])):
            s.add(ff.split('_')[0])
        sets.append(s)
    diff = list(sets[0].symmetric_difference(sets[1]))
    diff.sort()
    print diff

if __name__ == "__main__":
    main()