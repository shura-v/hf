import pandas, numpy, h5py
from pandas import Timestamp
from pprint import pprint


store_path = 'd:\\data\\db\\m_NYSE.h5'
GROUP = '/raw/_{raw}'.format(raw='1')


def duplicates(path, q):
    for i, key in enumerate(q):
        q_path = '{0}/{1}'.format(path, key)
        with pandas.get_store(store_path) as store:
            if len(store[q_path].index.get_duplicates()):
                print 'duplicates in "{0}"'.format(q_path)


def print_nodes(array):
    for i, node in enumerate(array):
        print '[{i:4}] {date} - {length} ({name}) [{pathname}]'.format(i=i,
                                                                       name=node['name'], pathname=node['pathname'],
                                                                       date=node['dates'][0],
                                                                       length=node['len'])


def print_quotes(data):
    quotes = []
    for node in data:
        quotes.append(node['pathname'].split('/')[-1])
    print ' '.join(quotes)
    print len(quotes)


def valid_from(data, dt):
    ts = Timestamp(dt)
    array = []
    for i, node in enumerate(reversed(data)):
        start = node['dates'][0]
        if start <= ts:
            array.append(node)
    return array


def valid_to(data, dt):
    ts = Timestamp(dt)
    array = []
    for i, node in enumerate(reversed(data)):
        start = node['dates'][-1]
        if start >= ts:
            array.append(node)
    return array


def main():
    data = []
    with pandas.get_store(store_path, mode='r+') as store:
        node = store.get_node('/processed/_1/_60')
        node._f_setattr('last_date', pandas.Timestamp('2014-03-13 16:00:00'))
    #     for node in store.get_node(GROUP)._f_iterNodes():
    #         data.append({
    #             'name': node._v_name,
    #             'pathname': node._v_pathname,
    #             'dates': [store.select(node._v_pathname, end=1).index[0], store.select(node._v_pathname, start=-1).index[-1]],
    #             'len': store.get_storer(node._v_pathname).nrows,
    #             'q': '/' + '/'.join(node._v_pathname.split('/')[4:])
    #         })
    #
    # data = sorted(data, key=lambda x: x['dates'][0])
    #
    # valid = valid_from(data, '2012-04-13 00:00:00')
    # valid = valid_to(valid, '2014-03-07 00:00:00')
    #
    # print_quotes(valid)

main()
