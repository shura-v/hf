import os
import time

import pandas
from matplotlib import pyplot as plt
from matplotlib.pyplot import plot
import h5py

from tests.old.dt import datetime, timedelta


pandas.set_option('display.height', 1000)
pandas.set_option('display.max_rows', 1000)


path = 'd:\\dropbox\\hf\\data\\db\\markets\\'


def get_time(dlt):
    min_date = datetime.min
    return (min_date + dlt).time()


def get_range(frames, freq="D"):
    start_dates = []
    finish_dates = []
    for f in frames:
        start_dates.append(f['frame'].index[0])
        finish_dates.append(f['frame'].index[-1])
    start_date = min(start_dates)
    finish_date = max(finish_dates)
    rng = pandas.bdate_range(start=start_date, end=finish_date, freq="H" if freq == "D" else freq)
    t_start = get_time(timedelta(hours=9, minutes=30))
    t_finish = get_time(timedelta(hours=16, minutes=0))
    if freq != "D":
        rng = rng[rng.indexer_between_time(t_start, t_finish)]
    else:
        rng = rng[rng.indexer_between_time(t_finish, t_finish)]
    return rng


def get_frames(frames, rng, freq=1):
    #for i in range(len(frames)):
        #frames[i]['frame'] = frames[i]['frame'].asfreq(freq)
        #frames[i]['frame'] = frames[i]['frame'].reindex(rng)
        #if 'raw' in frames[i]['name'] or '_source' in frames[i]['name']:
        #    frames[i]['frame'] = (frames[i]['frame'] - frames[i]['frame'].mean()) / frames[i]['frame'].max()
    return frames


def plot_frames(frames, col):
    plt.figure()
    for f in frames:
        f['frame'][col].plot(label=f['name'])
        #plt.plot(f['frame'][col], label=f['name'])
    plt.legend()
    plt.show()


def compare_bars(market, col, keys):
    p = os.path.join(path, market + '.h5')
    with pandas.get_store(p) as store:
        frames = [{'frame': store[key], 'name': key} for key in keys]
        #rng = get_range(frames, freq=bar)
        #frames = get_frames(frames, rng, freq=bar)
        plot_frames(frames, col)


def load(market, key):
    p = os.path.join(path, market + '.h5')
    with pandas.get_store(p) as store:
        #process_start = datetime.strptime("09:36:00", "%H:%M:%S").time()
        #end = datetime.strptime("14:24:00", "%H:%M:%S").time()
        #df = df.between_time(process_start, end, include_start=False, include_end=True)
        #df.to_hdf('d:\\s.h5', 'data')
        #df = pandas.read_hdf('d:\\s.h5', 'data')
        #df.resample('48min', closed='right')
        # df_min = store.select(key)
        df_min = store.select('/processed/_1/_40/last/pct_change/TAM')
        plot(df_min)
        # frames = [
        #     {'frame': df_min, 'name': '/raw/_1/COMPX_X'},
        #     {'frame': df_4min, 'name': '/processed/_1/_4/last/_source/COMPX_X'}
        # ]
        #
        # plot_frames(frames, 'Close')

        df_min_close = df_min['Close']
        # df_4min_close = df_4min['Close']

        print df_min[-50:]
        # print df_4min_close

        #print '---'


def remove_keys():
    p = os.path.join(path, 'NYSE.h5')
    with h5py.File(p, 'r+') as store:
        print store['/processed'].keys()





def date_range():
    rng = pandas.date_range("2012-01-01", "2013-12-31", freq='1min')
    keys = ["2013-02-01", "2013-02-02", "2013-02-03", "2013-02-04", "2013-02-05"]
    t = time.time()
    for key in keys:
        try:
            rng = rng.delete(rng.get_loc(key))
        except KeyError:
            pass
    print time.time() - t


def main():
    #date_range()
    load('NYSE', '/processed/_1/_40/last/_source/XAX_X')
    a = datetime.now()
    print a.replace(hour=0, minute=0, second=0, microsecond=0)
    print a
    # remove_keys()
    # #compare_bars('NYSE', 'Close', ['/raw/_D/VIRD_Z', '/processed/_D/_D/last/_source/VIRD_Z'])

main()
