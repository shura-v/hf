import pandas, os
from pprint import pprint

path = os.path.join(os.getenv('NETWORKS_CONFIG_DB'), 'm_NYSE.h5')
key = '/processed/_1/_40/last/_source'

if __name__ == '__main__':
    with pandas.get_store(path, mode='r') as store:
        pprint(dir(store.get_node(key)))
        for node in store.get_node(key)._f_iter_nodes():
            print '"/'+ "/".join(node._v_pathname.split('/')[4:])+'",'
