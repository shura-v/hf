import pandas, numpy
from pandas.stats import moments


def main():
    data = numpy.array(range(4, 100, 3)).T
    df = pandas.DataFrame(data)
    print data[-2:]
    #d = moments.ewma(df, span=5, min_periods=5)
    print df
    print df.pct_change(periods=1)
    print df.pct_change(periods=3)

main()