from matplotlib import pyplot as plt
import os
import pandas


base = "d:\\data\\portfolios\\"
quotes = ['AEP']
folder = "russell_1"
col = "Close"


def main():
    fld = os.path.join(base, folder)
    if os.path.isdir(fld):
        for q in quotes:
            f = q + '_' + folder.split('_')[1] + ".csv"
            path = os.path.join(fld, f)
            if os.path.isfile(path):
                df = pandas.read_csv(path, sep=" ", parse_dates=['Date'], index_col=0)
                plt.plot(df[col])
    plt.show()

main()