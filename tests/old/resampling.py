import pandas
import time
from datetime import datetime, timedelta
from pandas.io.pytables import Term
import numpy as np

path = 'd:\\dropbox\\hf\\data\\db\\markets\\nyse.h5'


def grp(x):
    return np.mean(x)


def main(store):
    open_time = timedelta(hours=10, minutes=00)
    close_time = timedelta(hours=16, minutes=0)
    midnight_time = timedelta(days=1, minutes=0)
    open = datetime.min + open_time
    close = datetime.min + close_time
    dt1 = datetime(2013, 10, 30, 15, 0)
    dt2 = datetime(2013, 10, 30, 15, 13)
    df = store.select('/raw/_1/COMPX_X')
    t = time.time()

    #df = df.between_time(close, open, include_start=False, include_end=True)
    ##print '=========== BETWEEN_TIME\n', df
    #
    #df = df.shift(freq=midnight_time-close_time)
    ##print '=========== SHIFT\n', df
    #
    #df = df.groupby(pandas.TimeGrouper(freq='D', closed='right'))
    ##print '=========== GROUP\n', df
    #
    #df = df.aggregate(grp)
    ##print '=========== AGGREGATE\n', df


    df = df.between_time(close, open, include_start=False, include_end=True)\
        .shift(freq=midnight_time-close_time)\
        .groupby(pandas.TimeGrouper(freq='D', closed='right'))\
        .aggregate(grp)\
        .shift(freq=open_time)

    #print '=========== SHIFT\n', df
    print df

    print time.time() - t


with pandas.get_store(path) as store:
    main(store)
