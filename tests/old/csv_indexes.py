import os, re

folder = 'd:\\data\\lists\\'

paths = [
    "indexes.txt",
    "indexes_russell.txt"
]


def main():
    quotes_all = set()
    for path in paths:
        quotes = get(os.path.join(folder, path))
        quotes_all = quotes_all.union(quotes)
    save(quotes_all)


def get(path):
    sep = '___'
    with open(path) as f:
        data = f.read()
    data = data.replace('\n', sep).split(sep)
    data = set(data)
    data.remove('')
    return list(data)


def save(quotes):
    quotes = list(quotes)
    quotes.sort()
    save_path = os.path.join(folder, 'indexes.csv')
    if os.path.isfile(save_path):
        os.unlink(save_path)
    string = str.join('\n', quotes)
    with open(save_path, "w") as f:
        f.write(string)


if __name__ == '__main__':
    main()