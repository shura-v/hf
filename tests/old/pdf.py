import re
import os
from pandas import read_csv, Timestamp
from pandas.lib import try_parse_date_and_time
from numpy import array
from matplotlib.pyplot import show


a = """
+GC#C
+GC#
+HG#C
+HG#
+SI#C
+SI#
QMGC#C
QMGC#
@QO#C
@QO#
@QI#C
@QI#
@QC#C
@QC#
QSIL#C
QSIL#
+CL#
+HO#
+NG#
+PA#
+PL#
+RB#
@QG#
@QM#
+CL#C
+HO#C
+NG#C
+PA#C
+PL#C
+RB#C
@QG#C
@QM#C
"""

lines = a.split('\n')
symbols = []

for line in lines:
    try:
        symbol = line.strip()
        # symbol = line.split('	')[1].strip()
        if symbol:
            symbols.append(symbol)
    except IndexError, e:
        pass

print len(symbols)

symbols = list(set(symbols))
symbols.sort()

print len(symbols)
s


# for symbol in symbols:
#     print symbol + '#' + 'C'

__dir = os.path.join(os.getenv('NETWORKS_CONFIG_CSV'), 'comex')
# a = read_csv(os.path.join(__dir, '+GC#C_1.csv'), sep=' ', index_col=0, parse_dates=[['Date', 'Time']])['Close']
# b = read_csv(os.path.join(__dir, 'QGC#C_1.csv'), sep=' ', index_col=0, parse_dates=[['Date', 'Time']])['Close']
# a.plot()
# b.plot()
# show()




files = os.listdir(__dir)

files.sort(key=lambda x: os.path.getsize(os.path.join(__dir, x)), reverse=True)
print len(files)
for f in files:
    name = f.split('_')[0]
    print name

# print '\n'.join(symbols), len(symbols)