from pandas import get_store
from matplotlib.pyplot import show

f = 'd:/data/db/NYSE_1_120.h5'
path = '/data/last/indicators/ewma_fast/AMTG'

# with get_store(f) as store:
#     for node in store.get_node(path):
#         length = len(store[path + node._v_name])
#         if length < 2000:
#             print path + node._v_name, length

with get_store(f) as store:
    store['/data/last/indicators/ewma_slow/AMTG']['Close'].plot()
    store['/data/last/source/AMTG']['Close'].plot()
    show()
    # for node in store.get_node(path):
    #     print store[path + node._v_name].index[-1]