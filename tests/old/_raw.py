from datetime import datetime
from time import sleep

from pandas.stats import moments
from pandas import set_option

from tests.old._raw import Raw

set_option('display.max_rows', 200)
set_option('display.line_width', 200)

raw = Raw()
with raw.store('C', 'r') as store:
    df = store['data'].fillna(1.)
    t = datetime.now()
    df = df.apply(lambda x: moments.ewma(x, span=10))
print datetime.now() - t
print df
sleep(100)