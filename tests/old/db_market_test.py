# python d:\dropbox\hf\db_market_test.py -- --csv "d:\dropbox\data\csv" --json "d:\dropbox\hf\data\json" --db "d:\dropbox\hf\data\db" --name="NYSE" --rawbars="1,4,8,24,48,D" --log "d:\dropbox\hf\data\logs"

import sys
import os
import optparse

from confirm import confirm
from market import market


def array_opt(option, opt, value, parser):
    setattr(parser.values, option.dest, value.split(','))


def update(options):
    import time
    db_path = os.path.join(options.db, 'markets', options.name + '.h5')

    if not os.path.isfile(db_path):
        print "WRONG PATH: db:", db_path
        confirmed = confirm("Create DB? Path: " + db_path)
        if confirmed:
            os.unlink(db_path)
        else:
            return

    m = market.Market(options, name=options.name, log_prefix='test')
    ind = m.indexes['24']

    t = time.time()
    print ind.index[40]
    print ind.at(ind.index[40], shift=-6)
    for i in range(0, 7000):
        pass
        # a = ind.get_loc(ind[i])
        # np.random.rand(100, 100).dot(np.random.rand(100, 100))
    print time.time() - t


def main():
    argv = sys.argv
    if "--" not in argv:
        argv = []
    else:
        argv = argv[argv.index("--") + 1:]

    parser = optparse.OptionParser()
    parser.add_option("-c", "--csv", dest="csv", help="Path to raw CSV data")
    parser.add_option("-j", "--config", dest="config", help="Update JSON")
    parser.add_option("-d", "--db", dest="db", help="Path to HDF5 database")
    parser.add_option("-n", "--name", dest="name", help="Market name")
    parser.add_option("-b", "--rawbars", dest="rawbars", help="Update JSON", type="string", action="callback", callback=array_opt)
    parser.add_option("-l", "--log", dest="log", help="Logs folder")

    options, args = parser.parse_args(argv)

    if not argv:
        parser.print_help()
        return

    update(options)

if __name__ == '__main__':
    main()
