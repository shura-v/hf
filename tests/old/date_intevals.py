from __future__ import division
import time

from tests.old.dt import datetime, timedelta


times = {
    'open': time.strptime("09:30:00", '%H:%M:%S'),
    'close': time.strptime("16:00:00", '%H:%M:%S')
}


def market_time(dt, key):
    t = times[key]
    return datetime(year=dt.year, month=dt.month, day=dt.day,
                    hour=t.tm_hour, minute=t.tm_min, second=t.tm_sec)


def valid_datetime(dt, bar):
    bar = int(bar)
    m_times = times
    minutes_from_open = (dt.hour * 60 + dt.minute) - (m_times['open'].tm_hour * 60 + m_times['open'].tm_min)
    substract = minutes_from_open % int(bar)
    return dt - timedelta(minutes=substract)


def dt_ceil(dt, bar):
    today_open = market_time(dt, 'open')
    today_close = market_time(dt, 'close')

    if dt < today_open:     # pre market
        dt_new = today_open
    elif dt > today_close:  # post market
        dt_new = today_open + timedelta(days=1)
    else:                   # in market
        if bar == 'D':          # in market daily
            dt_new = today_close
        else:                   # in market minutes
            bar = int(bar)
            seconds_from_open = (dt.hour * 60 * 60 + dt.minute * 60 + dt.second) - (today_open.hour * 60 * 60 + today_open.minute * 60)
            add = -seconds_from_open % (int(bar) * 60)
            dt_new = dt + timedelta(seconds=add, microseconds=dt.microsecond)
    return dt_new


def dt_floor(dt, bar):
    today_open = market_time(dt, 'open')
    today_close = market_time(dt, 'close')

    if dt < today_open:     # pre market
        dt_new = today_close - timedelta(days=1)
    elif dt > today_close:  # post market
        dt_new = today_close
    else:                   # in market
        if bar == 'D':          # in market daily
            dt_new = today_close - timedelta(days=1)
        else:                   # in market minutes
            bar = int(bar)
            seconds_from_open = (dt.hour * 60 * 60 + dt.minute * 60 + dt.second) - (today_open.hour * 60 * 60 + today_open.minute * 60)
            substract = seconds_from_open % (int(bar) * 60)
            dt_new = dt - timedelta(seconds=substract, microseconds=dt.microsecond)

    return dt_new


def dt_next(dt, bar):
    return dt_ceil(dt + timedelta(seconds=1), bar)


def dt_prev(dt, bar):
    return dt_floor(dt - timedelta(seconds=1), bar)


def dt_list():
    #dt_start = datetime(2014, 1, 18, 0, 0)
    #dt_end = datetime(2014, 1, 18, 16, 0)
    total = 24 * 60

    for i in range(100, 0, -1):
        if total % i == 0:
            dt = get_time(timedelta(minutes=total / i))
            t_str = dt.strftime('%H:%M')
            print '{0} ({1})'.format(t_str, dt.hour * 60 + dt.minute)


def get_time(dt):
    dt_min = datetime(2000, 1, 1)
    return dt_min + dt


def dt_show(delta):
    delta = timedelta(minutes=delta)
    for i in range(0, 10):
        dt = get_time(delta*i)
        print '{0}'.format(dt.strftime('%H:%M'))


def dt_round():
    dt = datetime(2014, 1, 3, 9, 30)
    bar = 30
    current_floor = dt_floor(dt, bar)
    #print 'dt   ', dt
    #print 'floor', current_floor
    #print 'prev ', dt_prev(current_floor, bar)
    #print 'next ', dt_next(current_floor, bar)

#dt_list()
print '----'
dt_show(720)
#dt_round()