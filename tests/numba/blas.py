from numbapro.cudalib.cublas import Blas
import numpy as np
from time import time


dtype = 'f8'


def cpu(A, B):
    t = time()
    a = np.dot(A.T, B)
    return time() - t


def gemm(A, B):
    """
    m = A rows
    n = B cols = C cols
    k = C rows = B rows
    """
    t = time()
    m = A.shape[0]
    n = B.shape[1]
    k = B.shape[0]
    C = np.empty([m, n]).astype(dtype)
    blas = Blas()
    blas.gemm('N', 'N', m, n, k, 1.0, A, B, 1.0, C)
    # print 'gpu', C
    return time() - t


def gemv(A, B):
    t = time()
    blas = Blas()
    C = np.empty([A.shape[0], 1]).astype(dtype)
    blas.gemv('N', A.shape[1], B.shape[1], 1.0, A, B, 1.0, C)
    return time() - t


def main():
    t_gpu_func = gemm
    t_cpu = 0
    t_gpu = 0
    inputs = 2000
    hidden = 2000
    for i in xrange(100):
        W = np.random.random([inputs, hidden]).astype(dtype)
        x = np.random.random([inputs, 2000]).astype(dtype)
        gW = W.copy()
        gx = x.copy()
        t_cpu += cpu(W, x)
        t_gpu += t_gpu_func(gW.T, gx)
    print 'GPU', t_gpu
    print 'CPU', t_cpu
    print 'speedup', t_cpu / (t_gpu or 1) - 1
    print t_gpu_func


if __name__ == '__main__':
    main()