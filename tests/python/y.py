from numpy import arange


def yrange(z):
    for x in arange(5):
        yield x
    for x in arange(5) + 100:
        yield x
    if z == 1:
        yield 'zzzzz'

rng = yrange(0)

for x in rng:
    print x

for x in rng:
    print x
