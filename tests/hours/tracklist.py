import re

s = """1.One Summer Day vs Ong Namo
byAnna Lee vs Alex TeeB vs Snatam Kaur
2.Humming The Lights In The Deep
byArmin van Buuren pres. Gaia vs The Doppler Effect
3.Capetown Crossroads
byZoo Brazil Vs Ilan Bluestone
4.Nowhere To Go This Night With Lost And Found Lotus
byW&W feat. Bree, Filo & Peri feat. Audrey Gallagher, 7 Skies & Kiholm & Suncatcher, Shogun
5.My Inner Angel
byOmnia & The Blizzard vs Gareth Emery & Christina Novelli
6.Save Us Sea Tides
byAndy Blueman vs Christina Novelli
7.If I Lose Myself Coming Home
byArmin van Buuren vs Arctic Moon vs OneRepublic
8.Your World Stands Still
bySimon O'Shine vs Mark Pledger & Melinda Gareh
9.The World To Come vs Open Your Eyes
byReconceal & Andy Blueman vs Marcus Santoro
10.Brush Strokes In Big Sky (Shura Vlasov Mashup)-
bySimon Patterson vs John O'Callaghan & Audrey Gallagher
11.Still Empty Night
bySoundLift & Daniel Kandi vs Lisa Miskovsky
12.Chasing Neptune's Siren
byDarren Porter & Ferry Tayle & Sean Tyas vs Snow Patrol
13.Nothing But Mana
bySebastian Brandt vs Paul van Dyk
14.Satellite vs Till The Sky Falls Down
byDash Berlin & Arctic Moon & Oceanlab feat. Justine Suissa
15.Poltergeist Animals
byIndecent Noise vs Martin Garrix
16.Believe In Gangnam Style
byChapter XJ & Matt Bukovski vs PSY
17.Because Of Airport
byPhotographer vs Kelly Clarkson
18.Stresstest Rapture
byJohn O'Callaghan & John Askew vs Nadia Ali
19.Ping Pong Once Waiting
by(BONUS) Too Many Artists""".split('\n')

r = re.compile('^by')
artists = [l[r.match(l).span()[1]:] for l in s[1::2]]
titles = [''.join(l.split('.')[1:]) + " (Shura Vlasov Mashup)" for l in s[0::2]]

for i, t in enumerate(zip(artists, titles)):
    print '{0}. {1} - {2}'.format(i + 1, t[0], t[1])
