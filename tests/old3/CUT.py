from pylab import *
from pandas import *
from datetime import datetime

index = DatetimeIndex([datetime(2014, 1, 11, 11, 30), datetime(2014, 1, 12, 15), datetime(2014, 1, 15, 22)])

s = Series({
    datetime(2014, 1, 10, 0): 1,
    datetime(2014, 1, 11, 10): 2,
    datetime(2014, 1, 11, 11): 3,
    datetime(2014, 1, 12, 12): 3,
    datetime(2014, 1, 15, 16): 4,
    datetime(2014, 1, 15, 22): 5
})

print s.groupby(lambda x: index[(index >= x).argmax()]).agg('last')

print s.groupby([1] * 3 + [2] + [3] * 2).sum()

"""
    Initially I wanted to make this, and this answers the second part of my question:

    s = Series({
        datetime(2014, 1, 10, 0): 1,
        datetime(2014, 1, 11, 10): 2,
        datetime(2014, 1, 11, 11): 3,
        datetime(2014, 1, 12, 12): 3,
        datetime(2014, 1, 15, 16): 4,
        datetime(2014, 1, 15, 22): 5
    })

    index = DatetimeIndex([datetime(2014, 1, 11, 11), datetime(2014, 1, 12, 12), datetime(2014, 1, 15, 22)])

    print s.groupby(lambda x: index[(index >= x).argmax()]).sum()

---

    2014-01-11 11:00:00    6
    2014-01-12 12:00:00    3
    2014-01-15 22:00:00    9
    dtype: int64


Here I have predefined index and I'm grouping by datetime's in it, aggregating all preceding values.
"""