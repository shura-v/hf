from markets.market import Market
from matplotlib.pyplot import show

m = Market('nyse.32t')

q = m['mean|pct_change_24|SPX.XO|C']


times = ['17:00']
times = None
period = ['2013-04-01', '2013-05-01']

if times:
    for t in times:
        qt = q.at_time(t).ix[period[0]:period[1]]
        qt.plot(label=t)
else:
    q.resample('D', how='median').ix[period[0]:period[1]].plot()

show()