from matplotlib.pyplot import show, plot
import numpy as np
from math import pi, e
from pandas import *


def plot_n(__mean, __std, n):
    x = np.random.normal(__mean, __std, n)
    x.sort()
    numer = np.exp(-0.5 * ((x - x.mean()) / x.std()) ** 2)
    denom = x.std() * (2 * pi) ** 0.5
    plot(x, numer / denom)

# plot_n(-e, e / pi, 1000)
# plot_n(0, 1, 1000)
# plot_n(pi, pi / e, 1000)
# show()


def correlation():
    x = np.array([1,0,-1,0,-1,0,1,0])
    y = np.array([-1,0,-1,0,1,0,1,0])

    min_size = min(x.size, y.size)

    x_minus_mu = (x - x.mean())[:min_size]
    y_minus_mu = (y - y.mean())[:min_size]

    numer = (x_minus_mu * y_minus_mu).sum()
    denom = np.sqrt(
        (x_minus_mu ** 2).sum() * (y_minus_mu ** 2).sum()
    )
    print numer / denom
    # plot(numer / denom)
    plot(x[:min_size])
    plot(y[:min_size])
    show()

correlation()