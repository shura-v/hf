from tests.old3.MAX_DATA import get


__sampler = 'mean'
__indicators = ['ma_5||0', 'ma_5||-3', 'ma_5||-6', 'ma_5||-9', 'ma_5||-12']
__col = 'C'


def p_arr(arr):
    print str(arr).replace('\'', '"').replace('[', '').replace(']', '') + ','


def print_all():
    data = get(45000, 'nyse.5T.removed', drop_only=[])
    __all = []
    for exchange, lst in data.iteritems():
        for i, indicator in enumerate(__indicators):
            keys = [get_key(dct['key'], indicator) for dct in lst]
            __all.extend(keys)
            if i == 0:
                print '===== {0} ====='.format(exchange).ljust(20), len(keys)
            p_arr(keys)
    print '========= EVERYTHING =======\n'
    p_arr(__all)
    print len(__all)


def get_key(key, indicator):
    roll = None
    if '||' in indicator:
        indicator, roll = indicator.split('||')
    s = '|'.join([__sampler, indicator, key, __col])
    if roll is not None:
        s += '||' + roll
    return s


print_all()