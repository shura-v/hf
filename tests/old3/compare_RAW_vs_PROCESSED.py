from __future__ import division
from markets.market import Market
from markets.dataprovider import DataProvider
from matplotlib.pyplot import plot, show


m = Market('fx.60T')
p = DataProvider('fx.1T')

m = m['most|USDJPY.COMP|C']
p = p['USDJPY.COMP', 'C']

# m.plot()
# p.plot()

print m[~m.notnull()]

show()