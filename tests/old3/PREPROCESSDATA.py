from markets.dataprovider import DataProvider
import numpy as np
from matplotlib.pyplot import plot, show
from time import time, sleep

def l(x):
    return 1 / (1 + np.exp(-x))

t = time()
p = DataProvider('nyse.5T')

data = p['EURUSD.COMP']['C'].dropna()
print time() - t
sleep(10)


data = data[data != 0]


def process(x, f):
    x = x.pct_change()
    x = x - x.min() + 1
    x = x.apply(np.log)
    x = (x - x.mean()) / x.std()
    return x.apply(f)


def process2(x, f):
    x = x.pct_change() + 1
    print x.index[0]
    print x[~x.notnull()]
    x = x.apply(np.log)
    x = (x - x.mean()) / x.std()
    return x.apply(f)

d1 = process(data, np.tanh)
d2 = process2(data, np.tanh)

d1.plot()
d2.plot()
show()