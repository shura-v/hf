from __future__ import division
from datetime import datetime

from pandas import *
from matplotlib.pyplot import show

set_option('display.height', 500)


start = datetime(2014, 1, 1)
t = 48

# s1 = Series(range(t), date_range(start=start, periods=t, freq='H'))
# s1.drop(s1.ix['2014-01-01 09:00':'2014-01-01 14:00'].index, inplace=True)
# s2 = s1.pct_change(freq='D')
# s3 = s1.pct_change()
# print DataFrame({'s1': s1, 's2': s2, 's3': s3})


def summer(x):
    return x.ix['2013-06':'2013-07']


def winter(x):
    return x.ix['2013-01':'2013-02']


def count_time(x):
    return x.groupby(x.index.time).count()


def day(x, _d):
    return x[x.index.dayofweek == _d]


from pandas.tseries.offsets import *

print LastWeekOfMonth(weekday=0)
# p = DataProvider('fx.1t')
# d = p['USDJPY.COMP']['C']
# s = count_time(day(d, 6))
#
# s.plot()
# fri_s = fri.ix['2013-06':'2013-07']
# print fri_w
# fri_w = fri_w.groupby(fri_w.index.time).sum().plot()
# fri_s = fri_s.groupby(fri_s.index.time).sum().plot()
#
# fri_w.plot()
# fri_s.plot()
# sun.plot()

show()
