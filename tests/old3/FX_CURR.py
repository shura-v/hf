arr = ["mean|###|AUDNZD.COMP|HL", "mean|###|CHFJPY.COMP|HL", "mean|###|EURAUD.COMP|HL", "mean|###|AUDJPY.COMP|HL", "mean|###|GBPJPY.COMP|HL", "mean|###|USDJPY.COMP|HL", "mean|###|EURUSD.COMP|HL", "mean|###|EURJPY.COMP|HL", "mean|###|USDCAD.COMP|HL", "mean|###|AUDUSD.COMP|HL", "mean|###|NZDUSD.COMP|HL", "mean|###|EURGBP.COMP|HL", "mean|###|USDCHF.COMP|HL", "mean|###|EURCHF.COMP|HL", "mean|###|GBPUSD.COMP|HL", "mean|###|AUDCAD.COMP|HL", "mean|###|GBPCHF.COMP|HL", "mean|###|EURCAD.COMP|HL", "mean|###|XAUCHFK.COMP|HL", "mean|###|XAUPLNO.COMP|HL", "mean|###|XPTCHFK.COMP|HL", "mean|###|XAUEURO.COMP|HL", "mean|###|XAUAUDO.COMP|HL", "mean|###|XAUJPYO.COMP|HL", "mean|###|XAUUSDO.COMP|HL", "mean|###|XAUKRWO.COMP|HL", "mean|###|XAUARSO.COMP|HL", "mean|###|XAUMXNO.COMP|HL", "mean|###|XAURUBO.COMP|HL", "mean|###|XAUCNYO.COMP|HL", "mean|###|XAUIDRO.COMP|HL", "mean|###|XAUINRO.COMP|HL", "mean|###|XAUZARO.COMP|HL", "mean|###|XAUCADO.COMP|HL", "mean|###|XAUSARO.COMP|HL", "mean|###|XAUBRLO.COMP|HL", "mean|###|XPDCHFK.COMP|HL", "mean|###|XAUTRYO.COMP|HL", "mean|###|XAUGBPO.COMP|HL", "mean|###|XAUCHFO.COMP|HL", "mean|###|XPTUSDO.COMP|HL", "mean|###|XPTEURO.COMP|HL", "mean|###|XPDUSDO.COMP|HL", "mean|###|XPTGBPO.COMP|HL", "mean|###|XPDEURO.COMP|HL",]
__currs = ['XAU', 'USD', 'EUR']
__sim = 2
used = []
indic = 'pct_change'


def pp():
    for q in arr:
        sims = 0
        for cur in __currs:
            if cur in q:
                sims += 1
        if sims >= __sim and q not in used:
            used.append(q)
            print '"{0}",'.format(q).replace('###', indic)

def get_all_indicators(sample, q, col):
    indicators = ['ewma_5', 'ma_5', 'ewma_12', 'ewma_26', 'macd', 'pct_change', 'sto', 'stok']
    for indi in indicators:
        print '"{0}|{1}|{2}|{3}",'.format(sample, indi, q, col)

pp()
get_all_indicators('most', 'EURUSD.COMP', 'HL')