from pandas import *
import os


# path = 'z:/info/symbols/mktsymbols_v2.txt'
#
# # read_csv(path, sep='\t', dtype=str).to_hdf('z:/info/symbols/mktsymbols_v2.h5', 'symbols')
# with get_store('z:/info/symbols/mktsymbols_v2.h5') as store:
#     s = store['symbols']
#
# sy = s['SYMBOL'].str
# market = s['LISTED MARKET']
#
#
# print s[sy.startswith('+') & sy.endswith('#')]
# print s[sy.endswith('#') & (market != 'EEXP')]
a = """USD--USA Dollar
EUR--European Euro
JPY--Japan Yen
GBP--UK Pound Sterling
AUD--Australia Dollar
CHF--Switzerland Franc
CAD--Canada Dollar
MXN--Mexico New Peso
CNY--China Yuan Renminbi
NZD--New Zealand Dollar
SEK--Sweden Krona
RUB--Russia Ruble
HKD--Hong Kong Dollar
SGD--Singapore Dollar
TRY--Turkey Lira
AUG--Gold/Silver Ratio
XAG--Silver
XAU--Gold
XPD--Palladium
XPG--Platinum-Gold
XPT--Platinum""".split('\n')

al = []
for x in a:
    d = x.split('--')
    print d
    al.append({'symbol': d[0], 'description': d[1]})
print al