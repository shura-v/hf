from __future__ import division

from numpy import array, median

from markets.dataprovider import DataProvider
from tests.old3 import KEYS


everything = []
__min_length = 0


def get(min_length=None, provider='nyse.5t', drop_only=False):
    prov = DataProvider(name=provider)
    data = {}
    if min_length is None:
        min_length = __min_length
    with prov.db.get('r') as db:
        # items = db[prov.db.key('end_dates')].index
        df = db[prov.db.key('data')].ix[:, :, 'C']
        for exchange, symbols in KEYS.iteritems():
            data[exchange] = []
            keys = KEYS[exchange]
            for key in keys:
                if key in df.columns:
                    __dct = {'key': key, 'len': len(df[key].dropna())}
                    if __dct['len'] >= min_length or (drop_only and exchange not in drop_only):
                        data[exchange].append(__dct)
                        everything.append(__dct)
            data[exchange] = sorted(data[exchange], key=lambda v: v['len'], reverse=True)
    return data


def _print_all(d):
    for k, arr in d.iteritems():
        print '=== {0} ==='.format(k)
        _print_ex(arr)


def _print_ex(arr):
    __arr = []
    lengths = []
    for dct in arr:
        __arr.append(dct['key'])
        lengths.append(dct['len'])
    lns = array(lengths)
    if len(lengths):
        print str(__arr).replace('\'', '"').replace('[', '').replace(']', '') + ','
        print '[{total:10}] Max: {_max}, Min: {_min}, Mean: {0}, Median: {1}'.format(lns.mean(), median(lns), _max=lns.max(), _min=lns.min(), total=len(lns))
    else:
        print 'EMPTY'


if __name__ == '__main__':
    data = get()
    print '========='
    print 'EVERYTHING'
    _print_ex(sorted(everything, key=lambda v: v['len'], reverse=True))

    print '========='
    print 'EXCHANGES'
    _print_all(data)
