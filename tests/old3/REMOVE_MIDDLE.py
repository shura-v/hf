from __future__ import division
from math import floor, ceil
from numpy import *


def remove_middle(neurons, middle):
    arr = ones((neurons, ), dtype=int)
    arr[:middle] = False
    return roll(arr, (neurons - middle) // 2)


def remove_middle_2(neurons, middle):
    return (neurons - middle) // 2 - 1, neurons - (neurons - middle) // 2

a = (7, 1)

print remove_middle(*a)
print arange(a[0])
print remove_middle_2(*a)
