from matplotlib.pyplot import plot, show
from pandas import *


n = 5000
spx = Series(np.random.normal(5, 1, n), index=date_range(start=0, periods=n))
spx = (spx - spx.mean() / spx.std())

plot(spx)


def res(series, z):
    index = [series.index[0]]

    for i, x in series.ix[1:].iteritems():
        last = series[index[-1]]
        sign_change = True if len(index) == 1 or (x - last) * (last - series[index[-2]]) < 0 else False
        if sign_change is False:
            index[-1] = i
        elif abs(x - last) > z:
            index.append(i)

    if index[-1] != series.index[-1]:
        index.append(series.index[-1])

    return series.ix[index].reindex_like(series).interpolate()

zz = res(spx, 2)
plot(zz)
show()
