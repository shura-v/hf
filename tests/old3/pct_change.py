from pandas import *


s = Series(range(10), date_range('2014', periods=10, freq='H'))

d = DataFrame({'a': range(10), 'b': range(10)}, date_range('2014', periods=10, freq='H'))

print s.pct_change(periods=5)
print s.pct_change(periods=1, freq='5H')