from markets.dataprovider import DataProvider
import matplotlib.pyplot as plt

# d = DataProvider('fx.1t')
d = DataProvider('nyse.5T.removed')
periods = [
#     ['2013-01-01', '2013-02-01'],
#     ['2013-02-01', '2013-03-01'],
#     ['2013-03-01', '2013-04-01'],
    ['2014-03-01', '2014-04-01'],
#     ['2013-05-01', '2013-06-01'],
#     ['2013-06-01', '2013-07-01'],
#     ['2013-07-01', '2013-08-01'],
#     ['2013-08-01', '2013-09-01'],
#     ['2013-09-01', '2013-10-01'],
#     ['2013-10-01', '2013-11-01'],
#     ['2013-11-01', '2013-12-01']
]
__q = d['RUA.X']['PV']

plt.figure()

sums = []

for p in periods:
    q = __q.ix[p[0]:p[1]]
    # sums.append({'sum': q.sum(), 'name': p})
    q = q.resample('30T', how='mean')
    q.groupby(q.index.time).mean().plot(label=str(p))


sums = sorted(sums, key=lambda x: x['sum'], reverse=True)
for x in sums:
    print x['name'], x['sum']


plt.legend()
plt.show()