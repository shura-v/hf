from pandas import *

index = DatetimeIndex([datetime(2014, 1, 11, 11, 30), datetime(2014, 1, 12, 15), datetime(2014, 1, 15, 22)])

s = Series({
    datetime(2014, 1, 10, 0): 1,
    datetime(2014, 1, 11, 10): 2,
    datetime(2014, 1, 11, 11): 3,
    datetime(2014, 1, 12, 12): 3,
    datetime(2014, 1, 13, 1): 100,
    datetime(2014, 1, 15, 16): 4,
    datetime(2014, 1, 15, 22): 5
})

def ct(x):
    return x.hour*3600+x.minute*60+x.second


print cut(map(ct,s.index.time),bins=[0] + map(ct,index.time))
print s.groupby(cut(map(ct,s.index.time),bins=[0] + map(ct,index.time))).sum()


index = DatetimeIndex([datetime(2014, 1, 11, 11, 30), datetime(2014, 1, 12, 15), datetime(2014, 1, 15, 22)])

s = Series({
    datetime(2014, 1, 10, 0): 1,
    datetime(2014, 1, 11, 10): 2,
    datetime(2014, 1, 11, 11): 3,
    datetime(2014, 1, 12, 12): 3,
    datetime(2014, 1, 13, 1): 100,
    datetime(2014, 1, 15, 16): 4,
    datetime(2014, 1, 15, 22): 5
})

print s.groupby(lambda x: index[(index >= x).argmax()]).sum()
