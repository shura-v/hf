from markets.dataprovider import DataProvider

d = DataProvider('nyse.5T.removed')

d.drop_key('XJO.XA')
d.drop_key('XMM.XA')
d.drop_key('XTL.XA')
d.drop_key('XTO.XA')