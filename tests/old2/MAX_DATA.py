from numpy import median, mean

r = Raw()

with r.store('C', 'r') as store:
    d = store['data']

info = []

# between_time = ['9:30', '16:00']
between_time = None  # ALREADY BETWEEN TIME IN RAW
exclude_exchanges = ['etf', 'indexes_daily', 'nymex', 'indexes', 'comex', 'lme']
# exclude_exchanges = []

for key in d.columns:
    x = d[key].dropna()
    if between_time is not None:
        x = x.between_time(*between_time)
    exch = key.split('/')[0]
    if exch in exclude_exchanges:
        continue
    info.append({
        "key": key,
        "len": x.size
    })

info.sort(key=lambda x: x['len'], reverse=True)

total = {}
cut = {}
exchange_lengths = {}
minimum = 15000


keys = {
    'total': [],
    'cut': []
}


def collect():
    for i in info:
        exchange, name = i['key'].split('/')
        if exchange not in exchange_lengths.keys():
            exchange_lengths[exchange] = []
        if exchange not in total.keys():
            total[exchange] = 0
        if exchange not in cut.keys():
            cut[exchange] = 0

        total[exchange] += 1
        keys['total'].append({'key': i['key'], 'len': i['len']})
        exchange_lengths[exchange].append(i['len'])
        if i['len'] >= minimum:  # or exchange not in ['etf']:
            cut[exchange] += 1
            keys['cut'].append({'key': i['key'], 'len': i['len']})


def print_count(name, x):
    print '------', name
    for ex, length in x.iteritems():
        print '{0:15} {1}'.format(ex, length)


def print_keys(k):
    for i in keys[k]:
        print '{0:15} {1}'.format(i['key'], i['len'])

    print [i['key'] for i in keys[k]]


def print_exchange_lengths(exch):
    print exchange_lengths[exch]


collect()
print_count('Total', total)
print_count('Cut', cut)

lengths = [i['len'] for i in keys['total']]
print 'Lengths: median = {0}, mean = {1}'.format(median(lengths), mean(lengths))
# print_exchange_lengths('lme')

print_keys('cut')
