from pandas import Panel, DataFrame, DatetimeIndex, date_range, get_store, Timestamp
from sys import getsizeof
import numpy
import string
import random
from time import sleep
from datetime import datetime
from datetime import timedelta
from pandas.stats import moments as pandas_moments


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


path = 'z:/__tests/panel.h5'

cols = 6
freq = 1
# n_keys = 50
n_keys = 3900  # 3913

keys = [id_generator() for x in range(n_keys)]
keys = list(set(keys))

start = Timestamp('2010-01-01')
end = start + timedelta(days=60)
__dict = {}


index = DatetimeIndex(date_range(start=start, end=end, freq=str(freq) + 'T'))


def memory(__panel):
    print 'Size:', __panel.values.nbytes



def create():
    print 'Creating...'
    t = datetime.now()
    for key in keys:
        # __index = index[random.randint(0, 20):]
        __index = index
        df = DataFrame(data=numpy.random.random([len(__index), cols]).astype(numpy.float32),
                       index=__index,
                       columns=range(cols))
        __dict.update({
            key: df
        })
    __panel = Panel(__dict)
    print 'Panel created in:', datetime.now() - t
    print 'Shape: {0} x {1} x {2}'.format(len(keys), len(index), cols)
    memory(__panel)
    return __panel


def _apply(__panel):
    print 'Apply...'
    t = datetime.now()
    __panel = __panel.apply(__apply, axis='major')
    print 'Apply in:', datetime.now() - t
    return __panel


def __apply(x):
    return pandas_moments.ewma(x, span=10, min_periods=10)


def to_frame(__panel):
    print 'To frame...'
    t = datetime.now()
    __panel = __panel.to_frame()
    print 'To frame:', datetime.now() - t
    return __panel


def save(__panel):
    print 'Saving...'
    t = datetime.now()
    with get_store(path, mode='w') as store:
        # store.append('panel', __panel)
        store.put('panel', __panel)
    print 'DB put:', datetime.now() - t


def load():
    print 'Loading...'
    t = datetime.now()
    with get_store(path, mode='r') as store:
        __panel = store.get('panel')
    print 'DB get:', datetime.now() - t
    return __panel


panel = create()
# panel = to_frame(panel)
save(panel)
panel = load()
memory(panel)
sleep(100)

# panel: 1:33, 0:52
# panel: 1:50, 0:08
# df: 1:53, 0:47
# df: 1:49, 0:48