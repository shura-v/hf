def get_extremes(extremes=None, neurons=5):
    config_extremes = extremes
    __extremes = None
    if config_extremes is True:
        config_extremes = 1
    if config_extremes:
        __extremes = [int(i) for i in [config_extremes - 1, neurons - config_extremes]]
    if __extremes and __extremes[0] > __extremes[1]:
        raise ValueError('Too large extremes value [{0}] for {1} neurons'.format(config_extremes, neurons))
    if __extremes is None:
        __extremes = [0, 0]

    return [x + 1 for x in range(neurons) if (x <= __extremes[0] or x >= __extremes[1])]

print get_extremes(3, 11)