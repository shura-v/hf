from pandas import *
from numpy import *
from datetime import datetime, timedelta


path = 'z:/db/__overlap.h5'


dr1 = date_range('2011-05-05', periods=10)

t = tile(array(range(1, 5)), (10, 1))
df1 = DataFrame(t * 10.1, index=dr1)
df1.ix[-1][0] = nan
df2 = DataFrame(t * 1.1, index=dr1 + timedelta(days=5), dtype=float)

df2.ix[0][1] = nan

print df2.combine_first(df1)
df1.update(df2)
print df1


# with get_store(path, mode='w') as store:
#     store.append('df', df1)
#     store.append('df', df2)
#     print store['df']