import os

from pandas import read_csv
from matplotlib.pyplot import show

from __old.raw import Raw


key = 'forex/USDCHF_COMP'


r = Raw()

data_csv = read_csv(os.path.join(r.csv_path, key.replace('_', '.')) + '_1.csv',
                    sep=' ',
                    parse_dates=[['D', 'T']],
                    index_col=0)['C']

with r.store('C') as store:
    data_store = store['data'][key].ix[-3000:]

data_csv = data_csv.ix[data_store.index[0]:]

print data_csv.size, data_store.size


data_csv.plot()
data_store.plot()
show()
