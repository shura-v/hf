from pandas import Panel, DataFrame, DatetimeIndex, date_range, get_store, Timestamp
from sys import getsizeof
import numpy
import string
import random
from datetime import datetime
from time import sleep
from datetime import timedelta
from pandas.stats import moments as pandas_moments

n_index = 10
freq = 30
start = Timestamp('2010-01-01')
end = start + timedelta(minutes=int(freq)) * (n_index - 1)
end_long = start + timedelta(minutes=int(freq)) * (n_index * 2 - 1)
# end = start + timedelta(days=2)

cols = ['Open', 'Close', 'High', 'Low', 'Volume']

index = DatetimeIndex(date_range(start=start, end=end, freq=str(freq) + 'T'))

slices = [
    [slice(0, 7), slice(0, 6)],
    [slice(4, 10), slice(3, 10), slice(0, 10)]
]

panels = []

for i, sub_slices in enumerate(slices):
    __dict = {
        'df_' + str(j): DataFrame(numpy.random.random([len(index[sub_slices[j]]), len(cols)]),
                                  index=index[sub_slices[j]], columns=cols)
        for j, __slice in enumerate(sub_slices)
    }
    panels.append(Panel(__dict))

t = datetime.now()

__items = panels[0].axes[0].union(panels[1].axes[0])
__index = panels[0].axes[1].union(panels[1].axes[1])
panels[0] = panels[0].reindex(items=__items, major_axis=index)
panels[0].update(panels[1])
print datetime.now() - t
