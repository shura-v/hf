from matplotlib.pyplot import show, plot

from market.market import Market


m = Market('nyse_60')

keys = 'most|ma_3|indexes/RUT_X|OHLC',\
       'most|pct_change|indexes/RUT_X|OHLC'

for key in keys:
    __d = m.get_indicator(*key.split('|'))
    plot(__d)
show()
