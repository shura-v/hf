from pandas import get_store, DataFrame, date_range
import numpy as np
from datetime import datetime, timedelta
import pandas as pd
from pandas import Series
from pandas.stats.moments import ewma


size = 20

def tit():
    # arr = np.random.random((size,))
    # arr = np.ones((size,))
    arr = np.arange(size, dtype=float)
    ix = np.random.randint(0, size, size)
    arr[:] = np.nan

    start = datetime.now().replace(second=0, microsecond=0, minute=0, hour=0)
    series = Series(arr, index=date_range(start, periods=size))
    # series.dropna(inplace=True)
    return series

s = tit()
s.dropna(inplace=True)
print s