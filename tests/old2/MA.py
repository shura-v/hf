from __future__ import division
import pandas as pd
import numpy as np
from datetime import datetime
from pandas.stats.moments import *
from matplotlib.pyplot import plot, show

# arr = np.random.random((20, 1)).flatten()

arr = np.arange(1000, dtype=float) * 1
arr = np.random.permutation(arr)
n = len(arr)
w = 10

s = pd.Series(arr, index=pd.date_range(datetime.now().replace(microsecond=0, second=0, minute=0),
                                       periods=n, freq='T'))

# print rolling_mean(s, w), s

# plot(rolling_mean(s, w))
# plot(s)
# show()

g_ix = int(n / 2)
ix = s.index[g_ix]

print 'actual', s.ix[ix]

r_mean = rolling_mean(s, w)
__span = None
__com = 0.5
r_ewma = ewma(s, span=__span, com=__com, adjust=False)


def from_ma(original_series, current_value, current_index, window):
    loc = original_series.index.get_loc(current_index)
    slc = slice(loc - window + 1, loc)
    return window * current_value - original_series[slc].sum()


def from_ewma(current_value, prev_value, span=None, com=None):
    if span is not None:
        alpha = 2 / (1 + span)
    else:
        alpha = 1 / (1 + com)
    return (current_value + prev_value * (alpha - 1)) / alpha


# print 'from ma', from_ma(s, r_mean.ix[g_ix], ix, w)
print 'from ewma', from_ewma(r_ewma.ix[g_ix], r_ewma.ix[g_ix - 1], span=__span, com=__com)