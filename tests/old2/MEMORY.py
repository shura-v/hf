from datetime import datetime

from matplotlib.pyplot import show
from pandas import DataFrame

from __old.raw import Raw

r = Raw()
#
# stores = []
# tt = datetime.now()
# for col in 'O,H,L,C,OHLC'.split(',')[::-1]:
#     t = datetime.now()
#     with r.store(col) as store:
#         stores.append(store['data'])
#     print col, datetime.now() - t
# print '-----------'
# print 'total', datetime.now() - tt
# sleep(50)

df = {}
for col in ['O', 'C', 'OHLC', 'H', 'L']:
    with r.store(col) as store:
        print '====', col
        t = datetime.now()
        d = store['data']['etf/GOOG']
        d = d.ix[-2500:].dropna()
        df[col] = d
df = DataFrame(df)
df.plot()

show()
