from matplotlib.pyplot import show

from market.market import Market


m = Market('nyse_240')
m["mean|pct_change|forex/XPTCHFK_COMP|C"].plot()
show()