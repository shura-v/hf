from matplotlib.pyplot import show, plot

from market.market import Market
from __old.raw import Raw


key = 'etf/G'
s_name = 'most'
col = 'O'

markets = {
    'nyse_60': None,
    'nyse_480': None
}

data = {
}

r = Raw()
# data['raw'] = r[key:col]

for m in markets.keys():
    markets[m] = Market(m)


# data['csv'] = read_csv(os.path.join(r.csv_path, key.replace('_', '.')) + '_1.csv', sep=' ', usecols=['D', 'T', col],
#                        parse_dates=[['D', 'T']], index_col=0, )[col]

for name, market in markets.iteritems():
    data[name] = market.get_source(s_name, key, col)

index_as = 'nyse_480'
index = data[index_as].index


def plot_lesser():
    plot(data[index_as])
    for k, v in data.iteritems():
        if k != index_as:
            plot(v.reindex(index, method='pad'))
    show()


def plot_pandas():
    for k, v in data.iteritems():
        v.plot()
    show()


plot_pandas()
