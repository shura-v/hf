from matplotlib.pyplot import show, plot

from market.market import Market


m = 'nyse_480'
key = 'etf/G'
s_names = 'most', 'ewma', 'mean'
col = 'C'


data = {}

# r = Raw()
# data['raw'] = r[key:col]

market = Market(m)

for s_name in s_names:
    data[s_name] = market.get_source(s_name, key, col)


def _plot():
    for k, v in data.iteritems():
        plot(v)
    show()

_plot()
