from matplotlib.pyplot import show, plot

from market.market import Market


m = 'nyse_480'
key = 'etf/G'
s_names = 'mean',
i_names = 'ma_7', 'ma_17'
col = 'C'


data = []

# r = Raw()
# data['raw'] = r[key:col]

market = Market(m)

for s_name in s_names:
    for i_name in i_names:
        __d = market.get_indicator(s_name, i_name, key, col)
        data.append({
            "name": s_name + '_' + i_name,
            "data": __d
        })


def _plot():
    for d in data:
        print d['name']
        plot(d['data'])
    show()

_plot()
