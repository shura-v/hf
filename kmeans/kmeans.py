import numpy as np
from pandas import *
from tools import Access, Config, Logger, Path, error


class KMeans(object):
    def __init__(self, config=None):
        self.name = config['name']
        self.sub_folder = 'kmeans'
        self.config = Config(self, config=config)


if __name__ == '__main__':
    config = {
        'name': 'zhopa',
        'market': ''
    }
    km = KMeans(config)
    print km.config.save