from pandas import get_store
import os


class DataBase(object):
    def __init__(self, instance):
        self.instance = instance

    def update(self, key, data):
        pass


class Store(DataBase):
    __keys = {
        'data': '/panel'
    }

    def __init__(self, instance=None, path=None):
        super(Store, self).__init__(instance)
        self.path = path or self.instance.path.db()

    def __getitem__(self, item):
        if item in self.__keys:
            return self.__keys[item]

    def get(self, mode):
        if mode != 'r' and not os.path.isfile(self.path):
            mode = 'w'
        return get_store(self.path, mode=mode)