import os

import numpy as np
from numpy import dot
import pandas as pd
from datetime import datetime


# from tools import error
# import time
from networks import functions as fn


class Weights(object):
    def __init__(self, network, packed=None, load=None):
        self.network = network
        self.dtype = network.dtype
        self.log = self.network.log
        self.neurons = network.neurons
        self.config = network.config['layers']

        if load and packed is None:
            packed = self.load(network, load)

        self.__layers = [
            Layer(self, i, config)
            for i, config in enumerate(self.config)
        ]

        self.total = self.__count()
        if packed is not None:
            self.fill(packed.flatten().copy())
        else:
            np.random.seed()
            self.create()
            self.pack()

    def copy(self):
        return Weights(self.network, packed=self.pack())

    def fill(self, packed):
        source = WeightsSource(packed)
        for i, layer in enumerate(self.__layers):
            layer.fill(source)

    def create(self):
        for layer in self.__layers:
            layer.create()

    @staticmethod
    def load(network, wid):
        # if isinstance(network, str):
        #     network = RNN(network)
        if not os.path.isfile(network.path.db()):
            network.log.warning('No DB: creating weights...')
            return None
        with network.store('r') as store:
            if wid is True:
                ids = store.get('ids')
                wid = ids.ix[-1]
            packed = store.get('weights/{0}'.format(wid))
            return packed.values

    def save(self):
        if not os.path.isfile(self.network.db_path):
            self.log.warning('Creating DB...')
        self.log.info('Saving weights...')
        with self.network.store('r+') as store:
            store.put('weights/{0}'.format(self.network.id), self.series())
            try:
                ids = store.get('ids')
            except KeyError:
                ids = pd.Series()
            ids.append(pd.Series({self.network.id: datetime.now()}))
            store.put('ids', ids)
        return self

    def series(self):
        return pd.Series(self.pack().flatten())

    def zeros(self):
        """
        @return: np.zeros, shape = (self.total, 1)
        """
        return np.zeros((self.total, 1), dtype=self.dtype).ravel()

    def clear_derivatives(self):
        for layer in self.__layers:
            layer.clear_derivatives()

    def pack(self, dw=False):
        """
        @return: np.array, shape = (n, 1)
        """
        w = []
        for layer in self.__layers:
            w += layer.pack(dw=dw)
        return np.array(w, dtype=self.dtype).reshape(-1, 1)

    def dw(self):
        return Weights(self.network, packed=self.pack(dw=True))

    def __count(self):
        return sum([layer.total for layer in self.__layers])

    def __getitem__(self, item):
        return self.__layers[item]

    def __iter__(self):
        return self.__layers.__iter__()

    def __len__(self):
        return self.total

    def __add__(self, other):
        return Weights(self.network, packed=self.pack() + other)

    def __sub__(self, other):
        return Weights(self.network, packed=self.pack() - other)

    def __mul__(self, other):
        return Weights(self.network, packed=self.pack() * other)

    def __repr__(self):
        return self.pack().__repr__()

    def print_struct(self):
        from pprint import pformat
        self.log.info('======= Weights =======')
        self.log.info('Layers {0}'.format(self.network.neurons))
        self.log.info('Total: {0}'.format(self.total))
        struct = []
        for i, layer in enumerate(self.__layers):
            struct.append(layer.struct())
        self.log.info('Struct:\n{0}'.format(pformat(struct)))


class Layer(object):
    def __init__(self, weights, n, config):
        self.n = n
        self.weights = weights
        self.log = weights.log
        self.dtype = weights.dtype
        self.__config = config
        self.neurons = config['neurons']
        self.sigmoid = config['sigmoid']
        self.bias = Bias(self, config.get('bias', {}), (1, self.neurons))
        self.input_layers = [InputLayer(self, cfg, i) for i, cfg in enumerate(config.get('inputs', [])) if cfg]
        self.external_blocks = [External(self, cfg) for cfg in config['external']] if config.get('external') else []
        # self.output_blocks = []
        self.total = self.__count()

    def create(self):
        self.bias.create()
        for input_layer in self.input_layers:
            input_layer.create()
        for external in self.external_blocks:
            external.create()

    def fill(self, source):
        self.bias.fill(source)
        for layer in self.input_layers:
            layer.fill(source)
        for external in self.external_blocks:
            external.fill(source)

    def pack(self, dw=False):
        w = []
        w += self.bias.pack(dw=dw)
        for layer in self.input_layers:
            w += layer.pack(dw=dw)
        for external in self.external_blocks:
            w += external.pack(dw=dw)
        return w

    def clear_derivatives(self):
        self.bias.clear_derivatives()
        for input_layer in self.input_layers:
            input_layer.clear_derivatives()
        for external in self.external_blocks:
            external.clear_derivatives()

    def struct(self):
        struct = {'input': [], 'external': []}
        for input_layer in self.input_layers:
            if input_layer:
                struct['input'].append(input_layer.struct())
        for external in self.external_blocks:
            struct['external'].append(external.shape)
        return struct

    def __count(self):
        total = self.bias.total
        for input_layer in self.input_layers:
            if input_layer:
                total += input_layer.total
        for external in self.external_blocks:
            total += external.total
        return total

    def __len__(self):
        return self.neurons


class Block(object):
    def __init__(self, layer, config, shape):
        """
        self.config: = dict({
            "eps": float or None,
            "radius": float or 0.12
        })
        """
        self.layer = layer
        self.log = layer.log
        self.dtype = self.layer.dtype
        self.config = config.get('weights', {})
        self.shape = shape
        self.lag = config.get('lag', 0)

        self.params = None
        self.weights = None
        self.dw = Derivative(self)
        self.total = self.shape[0] * self.shape[1]

    def create(self):
        """
        @return: np.array, shape = self.shape
        """

        self.params = self.get_params()

        if self.params['normal']:
            w = np.random.normal(self.params['mu'], self.params['std'], self.shape).astype(self.dtype)
        else:
            w = np.random.uniform(-self.params['eps'], self.params['eps'], self.shape).astype(self.dtype)

        w *= self.params['mult']

        if 'radius' in self.params:
            if w.shape[0] == w.shape[1]:
                eigenvalues = np.linalg.eigvalsh(w)
                max_eigenvalue = np.absolute(eigenvalues).max()
                w /= max_eigenvalue / self.params['radius']
            else:
                w_squared = fn.dot(np.array(w).transpose(), w)
                max_eigenvalue = np.linalg.eigvalsh(w_squared).max()
                w /= max_eigenvalue ** 0.5 / self.params['radius'] ** 0.5

        log = '[L{0}] '.format(self.layer.n)
        cls = self.__class__.__name__
        if cls == 'Block' and self.lag > 0:
            cls = 'Reccurent'
        log += cls.ljust(10)
        log += str(self.shape).rjust(15)
        log += ' {'

        i = 0
        for key, value in self.params.iteritems():
            i += 1
            if type(value) is float:
                value = '{0:.3f}'.format(value)
            elif type(value) is int:
                value = '{0}'.format(value).rjust(5)
            elif type(value) is bool:
                value = str(value).rjust(5)
            log += '"{0}": '.format(key)
            log += '{0}'.format(value)
            if i <= len(self.params.keys()) - 1:
                log += ', '
        log += '}'
        self.log.info(log)

        self.weights = w

    def clear_derivatives(self):
        self.dw.clear()

    def get_params(self):
        if self.__class__ is Bias:  # bias
            eps_default = 0.0
        elif self.__class__ is External:
            eps_default = 0.1
        elif self.lag > 0:          # recurrent
            eps_default = 0.1
        else:                       # normal
            eps_default = 0.2

        # eps_default = self.config.get('denom', 2) / np.sqrt(self.shape[1])
        params = {
            'eps': self.config.get('eps', eps_default),
            'mu': self.config.get('mu', 0),
            'std': self.config.get('std'),
            'variance': self.config.get('variance', 0.001),
            'normal': self.config.get('normal') or 'mu' in self.config or 'std' in self.config,
            'mult': self.config.get('mult', 1)
        }

        if params['std'] is None:
            params['std'] = params['variance'] ** 0.5
        if 'radius' in self.config:
            params['radius'] = self.config['radius']
        return params

    def pack(self, dw=False):
        if dw:
            return self.dw.pack()
        else:
            return self.weights.ravel().tolist()

    def fill(self, source):
        self.weights = np.array(source[0:self.total], dtype=self.dtype).reshape(self.shape)

    # noinspection PyUnusedLocal
    def __getitem__(self, item):
        return self.weights

    def get(self):
        return self.weights

    def transpose(self):
        return self.weights.T.copy()

    def dot(self, other):
        return dot(self.weights, other)
        # return self.__weights.dot(other)

    def transpose_dot(self, other):
        return dot(self.weights.T, other)
        # return self.__weights.T.dot(other)


class Bias(Block):
    def __init__(self, layer, config, shape):
        super(Bias, self).__init__(layer, config, shape)


class InputLayer(object):
    def __init__(self, layer, configs, n):
        self.layer = layer
        self.n = n
        self.shape = tuple([self.layer.weights.neurons[n], self.layer.neurons])
        self._blocks = [Block(self.layer, config, self.shape) for config in configs]
        self.total = self.shape[0] * self.shape[1] * len(self._blocks)

    def create(self):
        for block in self._blocks:
            block.create()

    def pack(self, dw=False):
        w = []
        for block in self._blocks:
            w += block.pack(dw=dw)
        return w

    def fill(self, source):
        for block in self._blocks:
            block.fill(source)

    def clear_derivatives(self):
        for block in self._blocks:
            block.clear_derivatives()

    def struct(self):
        struct = []
        for block in self._blocks:
            struct.append(block.shape)
        return struct

    def __getitem__(self, item):
        return self._blocks[item]

    def __iter__(self):
        return self._blocks.__iter__()


class External(Block):
    def __init__(self, layer, config):
        super(External, self).__init__(layer, config, tuple([len(config['quotes']), layer.neurons]))


class WeightsSource(object):
    def __init__(self, packed):
        self.array = packed.tolist()
        self.total = len(self.array)

    def __getitem__(self, __slice):
        arr = self.array[__slice]
        del self.array[__slice]
        return arr


class Derivative(object):
    def __init__(self, block):
        self.block = block
        self.shape = block.shape
        self.__sum = None
        self.clear()

    def clear(self):
        self.__sum = np.zeros(self.shape, dtype=self.block.dtype)

    def pack(self):
        if self.__sum is not None:
            return self.__sum.ravel().tolist()

    # noinspection PyUnusedLocal
    def __setitem__(self, key, value):
        self.__sum = value

    def __iadd__(self, other):
        self.__sum += other
        # self.__sum = add(self.__sum, other)
        return self

    # noinspection PyUnusedLocal
    def __getitem__(self, item):
        return self.__sum