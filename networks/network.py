# -*- coding: utf-8 -*-

"""
Даниил Домащенко
У меня в советнике рассчитывался индекс нестабильности рынка (Choppy Market Index). По нему смотрел флет или тренд.
Для флета действительно использовал простой RSI, а для тренда две стратегии - одна для прорыва максимума (простые зигзаги, без заморочек), а одна для прорыва волатильности (использовалась стратегия на основании индекатора Чайкина).

https://www.dropbox.com/s/ztbwqq9vz0c3zyn/neuro.js
"""


from __future__ import division
# from numba import autojit

import os, optparse

from markets.market import Market
from networks.context import Context
from networks import optimizers as opt
from networks.weights import Weights

from tools.tools import Access, Compression, Config, Logger, Path, error
from tools.mypprint import pformat

import numpy as np
from pandas import get_store, set_option, Timestamp, DataFrame, Series
from networks.statistics import Collector, Predictor, MultiCollector, MultiPredictor
from datetime import datetime, timedelta


class Network(object):
    def __init__(self, name=None, config=None, argv=None, create=False, nolog=False, no_print=False, sub_folder=None):
        self.options = self.__parse_options(argv)
        self.nolog = nolog

        self.sub_folder = sub_folder or 'networks'
        self.name = self.options.name or name
        self.config = Config(self, config=config, create=create)
        self.dtype = self.config['dtype'] or np.float64

        self.path = Path(self)
        self.db_path = self.path.db()
        self.busy_path = self.path.busy()

        self.compression = Compression(self)

        self.access = lambda *args, **kwargs: Access(self, *args, **kwargs)
        self.market = Market(name=self.config['market'], log='ERROR')
        self.market_indexer = self.market.indexer
        self.learn_indexer = self.market_indexer.at_times(self.times) if self.times else self.market_indexer
        self.log = Logger(self)

        self.recount()
        self.neurons = tuple(layer['neurons'] for layer in self.config['layers'])

        self.context = None
        self.optimizer = None
        self.id = self.config['id'] or datetime.now().strftime('%Y%m%d%H%M%S%f')

        self.log.info('Network current ID: "{0}|{1}"'.format(self.name, self.id))

        self.collector = Collector(self, _id=self.id)
        self.weights = Weights(self, load=self.config['load']['weights']).save()

        if not no_print:
            self.weights.print_struct()
            self.log_config()

    @staticmethod
    def __parse_options(argv=None):
        if argv is None:
            argv = []
        parser = optparse.OptionParser()
        parser.add_option("-n", "--name", dest="name")
        options, args = parser.parse_args(argv)
        return options

    def store(self, mode='r'):
        if mode != 'r' and not os.path.isfile(self.db_path):
            mode = 'w'
        return get_store(self.db_path, mode=mode)

    # recount inputs and targets
    def recount(self):
        layers = self.config['layers']
        for i in range(len(layers)):
            layer = layers[i]
            ins = layer.get('inputs')
            target = layer.get('target')
            targets = layer.get('targets')
            if ins:
                for j in range(len(ins)):
                    if ins[j] is None:
                        continue
                    for k in range(len(ins[j])):
                        lag = ins[j][k]['lag']
                        if lag <= 0 and i <= j:
                            error("Err: [future feedback] from Layer_{1} to Layer_{0} with zero lag".format(i, j),
                                  self.log)

            sigmoid = layer['sigmoid']
            if target:
                if layer.get('neurons'):
                    error('[NOT SUPPORTED] Excessive neurons: {0} (layer: {0})'.format(layer['neurons'], i), self.log)
                if sigmoid == 'tanh':
                    error('[NOT SUPPORTED] Sigmoid \'tanh\' on targets (layer: {0})'.format(i), self.log)
                layer['neurons'] = target['classes']
            elif targets:
                if layer.get('neurons'):
                    error('[NOT SUPPORTED] Excessive neurons: {0} (layer: {0})'.format(layer['neurons'], i), self.log)
                if sigmoid != 'logistic':
                    error('[NOT SUPPORTED] Sigmoid \'{sig}\' on targets (layer: {0})'.format(i, sig=sigmoid), self.log)
                batch = self.config['learner'].get('batch', 1)
                if batch > 1:
                    error('[NOT SUPPORTED] Predict batch [{0}] > 1 in multi targets (layer: {1})'
                          .format(batch, self.log))
                layer['neurons'] = len(targets['quotes'])
            if 'neurons' not in layer:
                raise KeyError('No neurons in layer {0}'.format(i))

    @staticmethod
    def set_print_options(max_rows=5000, height=500, width=1400, precision=4):
        set_option('display.height', height)
        set_option('display.max_rows', max_rows)
        set_option('display.width', width)
        set_option('display.precision', precision)

    @property
    def times(self):
        return self.config['learner'].get('times')

    def log_config(self):
        self.log.info('Config:\n{0}'.format(pformat(self.config)))


class RNN(Network):
    def __init__(self, *args, **kwargs):
        super(RNN, self).__init__(*args, **kwargs)

    def accuracy(self, weights=None):
        if weights is None:
            weights = self.weights
        self.context.forward(weights, test_time=True)
        arr = []
        for i, __layer in enumerate(self.weights):
            layer = self.context[i]
            if not layer.targets:
                continue
            arr.extend(np.array([
                np.equal(layer.y[dt].argmax(), layer.targets[dt].argmax()).flatten()
                for dt in self.context.learn_index
            ], dtype=self.dtype).flatten())
        return np.mean(arr) if len(arr) else -1

    def learn(self):
        learner = LearnIntervals(self, **self.config['learner'])

        # connection type       sparsity        scale
        # in-to-hid (add,mul)   dense           0.001 * N(0;1)
        # in-to-hid (mem)       dense           0.1* N(0;1)
        # hid-to-hid            15 fan-in       spectral radius of 1.1
        # hid-to-out            dense           0.1* N(0;1)
        # hid-bias              dense           0
        # out-bias              dense           average of outputs

        all_losses = []
        all_accuracies = []

        weights = self.weights
        # self.weights = Weights(self, weights.pack().copy())

        self.context = Context(self, learner[0].learn[0], learner[-1].predict[-1],
                               learn=True, learn_length=learner[0].learn_length)
        with self.market.access('r'):
            dt_start = datetime.now()
            for i, interval in enumerate(learner):
                self.context.clear(True)
                self.log.info('[{current:3}/{total:3}] [{0} - {1}] {2}' .format(interval.learn[0],
                                                                                interval.learn[-1],
                                                                                interval.predict[-1],
                                                                                current=i, total=len(learner) - 1))
                self.weights = Weights(self, weights.pack().copy())
                losses, accuracies = self.epochs(interval, i, len(learner))
                # all_losses.append(losses)
                # all_accuracies.append(accuracies)
                self.remaining(i, len(learner), dt_start)
                # __outputs, __targets = self.context.predict(*dict['predict'][0])
                # if n > 0 and n % 10 == 0:
                #     plot(np.array(all_losses).T)
                #     show()
            self.log.info('ponyachka molodets!')

    def remaining(self, n, total, start):
        td_elapsed = datetime.now() - start
        td_mean = td_elapsed // (n + 1)
        remaining_iterations = total - (n + 1)
        td_remaining = td_mean * remaining_iterations
        dt_remaining = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0) + td_remaining
        self.log.info('[{0:3}/{1:3}] Done [mean time: {m}] [remaining...] {2} [{3}]'
                      .format(n, total - 1, dt_remaining.strftime('%H:%M:%S'), remaining_iterations, m=td_mean))

    def epochs(self, interval, n, total):
        self.optimizer = self.new_optimizer()
        prev_loss = None

        configs = self.config['predictor'] or [
            # {'choose': 3, 'force_keep_middle': True, 'name': 'c3'},
            # {'choose': 3, 'name': 'c3'},
            # {'choose': 2, 'force_keep_middle': True, 'name': 'c2'},
            # {'choose': 2, 'name': 'c2'},
            # {'choose': 1, 'force_keep_middle': True, 'name': 'all'},
            {'choose': 1, 'name': 'all'}
        ]

        losses = []
        accuracies = []

        for i in [-1] + range(self.optimizer.epochs):
            self.context.learn_index = self.learn_indexer.between(*interval.learn)
            if i >= 0:
                self.weights = self.optimizer.optimize(n, i)

            loss = self.context.forward(test_time=True).loss()
            accuracy = self.accuracy()
            losses.append(loss)
            accuracies.append(accuracy)

            self.context.forward(index=self.learn_indexer.between(interval.learn[0], interval.predict[-1]),
                                 test_time=True)
            self.context.learn_index = self.learn_indexer.between(*interval.predict)

            for j, layer in enumerate(self.context[:]):
                if layer.targets is None:
                    continue
                outputs, target = self.collector.append(total_loss=loss, accuracy=accuracy,
                                                        learn_start=interval.learn[0], learn_end=interval.learn[-1],
                                                        predict_start=interval.predict[0], predict_end=interval.predict[-1],
                                                        layer=layer, epoch=i)
                __dict = {}
                for __c, cfg in enumerate(configs):
                    __cfg = dict(cfg)
                    name = __cfg.pop('name') if 'name' in __cfg else str(__c)
                    series = self.predict(i, loss, layer, total, name=name, current_id=True, epoch=i, **__cfg)
                    if series is not None:
                        __dict[name] = series
                if not len(__dict.keys()):
                    continue

                # df = DataFrame(__dict).transpose().sort(columns=['a__trans', 'a_binned', 'a_exact'], ascending=False)
                df = DataFrame(__dict).transpose()
                # self.log.info('[{0:3}/{1:3}] [{2:2}] results\n{df}'.format(n, total - 1, i, df=df))
                log = '[{0:3}/{1:3}] [{2:2}] {3:4.3f} ({length}) [{4:4.4f}] [{5:.3f}]'\
                    .format(n, total - 1, i, df['binned'].ix[0], loss, accuracy, length=df['a__n'].ix[0]).ljust(50)
                self.log.info(log + ' ({1}) [{0} : {2:.3f}] {3}'
                              .format(outputs.argmax(), target.values[0], float(outputs.max()), outputs.values.flatten()))
            if self.optimizer.stop.get('accuracy'):
                if ((prev_loss is not None and 0 <= (prev_loss - loss) / prev_loss < 0.005) or
                        (accuracy >= self.optimizer.stop.get('accuracy'))):
                    break
            if self.optimizer.stop.get('loss') and loss < self.optimizer.stop['loss']:
                break
            prev_loss = loss
        return losses, accuracies

    def predict(self, i, loss, layer, total, name=None, **kwargs):
        predictor = Predictor(self.collector, layer, **kwargs)
        results, back_transformed_dataframe = predictor.results
        if results is not None and len(results):
            if 'all' in name:
                if len(results) == total:
                    self.log.debug('Back transformed for {0}\n{1}'.format(name, back_transformed_dataframe))
                else:
                    self.log.debug('Back transformed for {0}\n{1}'.format(name, back_transformed_dataframe.tail(20)))
            __dict = {
                'l_acc': self.collector.last_learn_accuracy,
                'l_loss': loss,
                'a__n': len(results)
            }
            __dict.update(predictor.accuracy)
            return Series(__dict)

    def add_to_market(self):
        market_name = self.config['market']
        market = Market(name=market_name)
        market.config.add_network(self.name)

    def new_optimizer(self):
        return getattr(opt, self.config['optimizer']['name'])(self)

    def _check_gradient(self):
        self.context.forward(test_time=True)
        analytical = self.context.backward()

        weights = self.weights.copy()
        packed = weights.pack()
        numerical = np.zeros(packed.shape)
        perturb = np.zeros(packed.shape)
        e = 2e-5

        for p in xrange(len(weights)):
            perturb[p] = e
            loss_minus = self.context.forward(weights=self.weights - perturb, test_time=True).loss()
            loss_plus = self.context.forward(weights=self.weights + perturb, test_time=True).loss()
            numerical[p] = (loss_plus - loss_minus) / (2 * e)
            perturb[p] = 0

        self.log.info('CHECKING GRADIENTS...')
        self.log.info('Analytical - Numerical')
        self.log.info('{0} - {1}'.format(np.shape(analytical), np.shape(numerical)))
        self.log.info(np.hstack([analytical, numerical.reshape(-1, 1)]))
        self.log.info('Relative diff: {0}'.format(np.linalg.norm(numerical - analytical)
                                                  / np.linalg.norm(numerical + analytical)))
        self.log.info('Diff: {0}'.format(np.linalg.norm(numerical - analytical)))

    def _check_gv(self, v):
        packed = self.weights.pack()

        self.context.forward(test_time=True)

        jm = {
            i: self.context[i].y.all()
            for i, layer in enumerate(self.weights) if self.context[i].targets
        }

        analytical = self.context.gv(v)

        jn = {
            i: {
                dt: np.zeros([layer.neurons, len(self.weights)])
                for dt in self.context
            }
            for i, layer in enumerate(self.weights) if self.context[i].targets
        }

        perturb = np.zeros(packed.shape)
        e = 2e-5

        for w in xrange(len(self.weights)):
            perturb[w] = e
            net_minus = self.context.forward(self.weights - perturb, test_time=True).x()
            net_plus = self.context.forward(self.weights + perturb, test_time=True).x()
            for i, layer_jn in jn.iteritems():
                for dt in layer_jn:
                    layer_jn[dt][:, w:w + 1] = (net_plus[i][dt] - net_minus[i][dt]) / (2 * e)
            perturb[w] = 0

        g = np.zeros([len(self.weights), len(self.weights)])
        for i, layer in enumerate(self.weights):
            if not self.context[i].targets:
                continue
            for dt in jn[i]:
                g += jn[i][dt].T.dot(self.context[i].prime(jm[i][dt])).dot(jn[i][dt])

        numerical = g.dot(v.reshape(-1, 1))

        self.log.info('CHECKING GAUSS NEWTON APPROX...')
        self.log.info('Analytical - Numerical')
        self.log.info('{0} - {1}'.format(np.shape(analytical), np.shape(numerical)))
        self.log.info(np.hstack([analytical, numerical.reshape(-1, 1)]))
        self.log.info('Relative diff: {0}'.format(np.linalg.norm(numerical - analytical)
                                                  / np.linalg.norm(numerical + analytical)))
        self.log.info('Diff: {0}'.format(np.linalg.norm(numerical - analytical)))

    def __len__(self):
        return len(self.neurons)


class RNNAutoEncoder(RNN):
    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, sub_folder='autoencoders', **kwargs)
        self.log = Logger(self)
        self.collector = MultiCollector(self, overwrite=False)

    def epochs(self, interval, n, total):
        self.optimizer = self.new_optimizer()
        prev_loss = None

        losses = []
        accuracies = []

        self.__predict(n, total, self.accuracy(), interval, epoch=-1)
        for i in xrange(self.optimizer.epochs):
            self.context.learn_index = self.learn_indexer.between(interval.learn[0], interval.learn[-1])
            loss = self.context.forward(test_time=True).loss()
            accuracy = self.accuracy()
            losses.append(loss)
            accuracies.append(accuracy)

            self.weights = self.optimizer.optimize(n, i)
            if type(self.optimizer) is opt.HF:
                self.log.info('CG iter: {0}, rate: {1}'.format(self.optimizer.iter, self.optimizer.rate))
            self.__predict(n, total, self.accuracy(), interval, epoch=i)

            (stop, reason, loss, accuracy) = self.stop(prev_loss)
            if stop:
                break
            prev_loss = loss
        return losses, accuracies

    def stop(self, prev_loss):
        loss = self.context.forward(test_time=True).loss()
        accuracy = self.accuracy()
        if self.optimizer.stop.get('accuracy'):
            if ((prev_loss is not None and 0 <= (prev_loss - loss) / prev_loss < 0.005) or
                    (accuracy >= self.optimizer.stop.get('accuracy'))):
                return True, 'accuracy', loss, accuracy
        if self.optimizer.stop.get('loss') and loss < self.optimizer.stop['loss']:
            return True, 'loss', loss, accuracy
        return False, None, loss, accuracy

    def accuracy(self, weights=None):
        if weights is None:
            weights = self.weights
        self.context.forward(weights, test_time=True)
        arr = []
        for i, __layer in enumerate(self.weights):
            layer = self.context[i]
            if not layer.targets:
                continue
            arr.extend(np.array([
                np.equal(layer.y[dt].round(), layer.targets[dt]).flatten()
                for dt in self.context.learn_index
            ], dtype=self.dtype).flatten())
        return np.mean(arr) if len(arr) else -1

    def remaining(self, n, total, start):
        td_elapsed = datetime.now() - start
        td_mean = td_elapsed // (n + 1)
        remaining_iterations = total - (n + 1)
        td_remaining = td_mean * remaining_iterations
        dt_remaining = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0) + td_remaining
        self.log.info('[{0:3}/{1:3}] Done [mean time: {m}] [remaining...] {2} [{3}]'
                      .format(n, total - 1, dt_remaining.strftime('%H:%M:%S'), remaining_iterations, m=td_mean))

    def __predict(self, n, total, accuracy, interval, **kwargs):
        # self.log.info('Mean weights: {0}'.format(np.absolute(self.weights.pack()).sum()))
        self.context.learn_index = self.market_indexer.between(*interval.predict)
        self.context.forward(test_time=True)
        for layer in self.context[:]:
            if layer.targets is None:
                continue
            self.collector.append(learn_start=interval.learn[0], learn_end=interval.learn[-1],
                                  predict_start=interval.predict[0], predict_end=interval.predict[-1],
                                  layer=layer, epoch=kwargs['epoch'], accuracy=accuracy)
            predictor = MultiPredictor(self.collector, layer, **kwargs)
            df, df_mean = predictor.results
            if n == total - 1:
                self.set_print_options(max_rows=int(1e5))
                self.log.info('[{0:3}] [{1:2}] [top ]\n{df}'.format(n, kwargs['epoch'], df=df))
            else:
                head = 10
                tail = 3
                self.set_print_options(max_rows=max(head, tail))
                self.log.info('[{0:3}] [{1:2}] [top ]\n{df}'.format(n, kwargs['epoch'], df=df.head(head)))
                if len(df) > head:
                    self.log.info('[{0:3}] [bottom]\n{df}'.format(n, kwargs['epoch'], df=df.tail(tail)))
                if len(df) > 1:
                    self.log.info('[{0:3}] [{1:2}] [mean]\n{df}'.format(n, kwargs['epoch'], df=df_mean))
            self.log.info('[{0:3}] [{1:2}] L-accuracy: {acc:.3f}'.format(n, kwargs['epoch'], acc=accuracy))

    def __len__(self):
        return len(self.neurons)


class LearnIntervals(object):
    def __init__(self, network, learn=None, start=None, end=None, total=100, times=None):
        # TODO: batch
        """
        :param network:
        :param learn:
        :param start:
        :param end:
        :param step:
        :param total:
        :param batch:
        :param times:
        :return:
        """
        self.learn_length = learn
        self.network = network
        self.intervals = network.config['intervals']
        self.market = network.market
        self.indexer = network.learn_indexer
        self.list = self.__get_list(start, end, total)

    def __len__(self):
        return len(self.list)

    def __iter__(self):
        return self.list.__iter__()

    def __getitem__(self, item):
        return self.list.__getitem__(item)

    def __get_list(self, start, end, total):
        if end:
            end = self.indexer.floor(Timestamp(end) - timedelta(microseconds=1))
        else:
            end = self.last_possible()
        if start:
            start = self.indexer.ceil(Timestamp(start))
        else:
            start = self.indexer.shift(end, -(total - 1))

        predict_index = self.indexer.between(start, end)

        indexes = []
        for i, dt in enumerate(predict_index):
            __end = self.indexer.shift(dt, -1)
            __start = self.indexer.shift(__end, -(self.learn_length - 1))
            indexes.append(self.Datetime(self, i, [dt, dt], [__start, __end]))
        return indexes

    class Datetime(object):
        def __init__(self, learner, i, predict, learn):
            self.i = i
            self.learner = learner
            self.predict = predict
            self.learn = learn
            self.learn_length = learner.learn_length

    def last_possible(self):
        return self.indexer.floor(self.market.provider.updated)


class RBF(Network):
    def __init__(self, *args, **kwargs):
        super(RBF, self).__init__(*args, **kwargs)