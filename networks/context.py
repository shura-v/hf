# -*- coding: utf-8 -*-
from __future__ import division

import numpy as np
import pandas as pd
from pandas import Term
from markets.indicators import Indicators

from networks import functions as fn, transform
from tools.tools import error

from weights import Weights
from numpy import add, multiply as mult, dot


class Context(object):
    def __init__(self, network, start, end, learn=False, learn_length=100):
        if end is None:
            error('Context: end cannot be None')

        self.learn = learn

        self.network = network
        self.log = network.log

        self.market = network.market
        self.market_indexer = network.market_indexer
        self.learn_indexer = network.learn_indexer

        self.learn_length = learn_length

        self.start = Timestamp(start)
        self.end = Timestamp(end)
        self.market_index = self.market_indexer.between(self.start, self.end)
        self.learn_index = self.learn_indexer.between(self.start, self.end)

        with self.market.access('r'):
            self.__layers = [Layer(self, n, config=config)
                             for n, config in enumerate(self.network.config['layers'])]

    @property
    def size(self):
        return len(self.__layers)

    def loss(self, index=None):
        if index is None:
            index = self.learn_index
        total_cost = 0
        for layer in self.__layers:
            if layer.targets is not None:
                for dt in index:
                    cost = layer.loss(dt)
                    total_cost += cost
        return total_cost if not np.isnan(total_cost) else float('infinity')

    def forward(self, weights=None, index=None, test_time=False):
        if weights is None:
            weights = self.network.weights
        if index is None:
            index = self.learn_index
        for dt in index:
            for i, layer in enumerate(weights):
                self.__layers[i].x[dt] = layer.bias.transpose()
                for input_layer in layer.input_layers:
                    for block in input_layer:
                        shifted_dt = self.learn_indexer.shift(dt, -block.lag)
                        self.__layers[i].x[dt] = add(self.__layers[i].x[dt],
                                                     block.transpose_dot(self.__layers[input_layer.n].y[shifted_dt]))

                for j, external in enumerate(layer.external_blocks):
                    self.__layers[i].x[dt] = add(self.__layers[i].x[dt],
                                                 external.transpose_dot(self.__layers[i].external[j][dt]))

                self.__layers[i].y[dt] = self.__layers[i].sigmoid(self.__layers[i].x[dt])
                if self.__layers[i].dropout:
                    if test_time:
                        self.__layers[i].y[dt] *= (1 - self.__layers[i].dropout.probability)
                    else:
                        self.__layers[i].y[dt] = mult(self.__layers[i].y[dt], self.__layers[i].dropout[dt])
        return self

    def backward(self, weights=None, ry=False, damping=None):
        self.clear(dy=True)
        if weights is None:
            weights = self.network.weights
        weights.clear_derivatives()

        for dt in self.learn_index[::-1]:
            for layer in weights[::-1]:
                i = layer.n

                if self[i].dropout:
                    self[i].dy[dt] = mult(self[i].dy[dt], self[i].dropout[dt])
                self[i].dx[dt] = dot(self[i].y.prime(dt), self[i].dy[dt])               # dx += y_prime.dot(dy)

                # Targets
                if ry:
                    if self[i].targets and dt in self[i].targets:                       # dx = ry
                        self[i].dx[dt] = add(self[i].dx[dt], self[i].ry[dt])
                    elif damping:                                                       # damping
                        self[i].dx[dt] = add(self[i].dx[dt],
                                             damping.lmbda * damping.mu * dot(self[i].y.prime(dt), self[i].rx[dt]))
                elif self[i].targets and dt in self[i].targets:
                    self[i].dx[dt] = add(self[i].dx[dt],
                                         self[i].y[dt] - self[i].targets[dt])           # dx = y - t

                # External
                for j, block in enumerate(layer.external_blocks):
                    block.dw += dot(self[i].external[j][dt], self[i].dx[dt].T)

                # Input
                for input_layer in layer.input_layers:
                    j = input_layer.n
                    for block in input_layer:
                        shifted_dt = self.learn_indexer.shift(dt, -block.lag)
                        block.dw += dot(self[j].y[shifted_dt], self[i].dx[dt].T)        # dw = y.dot(dx.T)
                        self[j].dy[shifted_dt] = add(self[j].dy[shifted_dt],
                                                     block.dot(self[i].dx[dt]))         # dy = w.dot(dx)
                        if self[j].dropout:
                            self[j].dy[shifted_dt] = mult(self[j].dy[shifted_dt], self[j].dropout[shifted_dt])

                layer.bias.dw += self[i].dx[dt].T
        return self.network.weights.pack(dw=True)

    def rop(self, v, weights=None):
        """
            @param v:
            @param weights:

            Nic Schraudolph: http://nic.schraudolph.org/pubs/Schraudolph01.pdf
            Barak Pearlmutter: http://bcl.hamilton.ie/~barak/papers/nc-hessian.pdf

            Saving "ry" to context:
            ry = J_M * J_N * v
            ry = dy/dx * dx/dw * v
        """
        v = Weights(self.network, packed=v)

        if weights is None:
            weights = self.network.weights

        for dt in self.learn_index:
            for i, layer in enumerate(weights):
                v_layer = v[i]
                self.__layers[i].x[dt] = layer.bias.transpose().copy()
                self.__layers[i].rx[dt] = v_layer.bias.transpose().copy()

                for j, input_blocks in enumerate(layer.input_layers):
                    n = input_blocks.n
                    input_layer = self.__layers[n]
                    for k, block in enumerate(input_blocks):
                        shifted_dt = self.learn_indexer.shift(dt, -block.lag)
                        y_prev = input_layer.y[shifted_dt]
                        self.__layers[i].x[dt] = add(self.__layers[i].x[dt],
                                                     block.transpose_dot(y_prev))
                        v_block = v_layer.input_layers[j][k]
                        # R{x} = w.T * R{y_prev} + v.T * y_prev
                        self.__layers[i].rx[dt] = add(self.__layers[i].rx[dt],
                                                      add(block.transpose_dot(input_layer.ry[shifted_dt]),
                                                          v_block.transpose_dot(input_layer.y[shifted_dt])))

                for j, external in enumerate(layer.external_blocks):
                    y_prev = self.__layers[i].external[j][dt]
                    self.__layers[i].x[dt] = add(self.__layers[i].x[dt],
                                                 layer.external_blocks[j].transpose_dot(y_prev))
                    # R{x} = v.T * y_prev
                    self.__layers[i].rx[dt] = add(self.__layers[i].rx[dt],
                                                  v_layer.external_blocks[j].transpose_dot(y_prev))

                self.__layers[i].y[dt] = self.__layers[i].sigmoid(self.__layers[i].x[dt])
                if self.__layers[i].dropout:
                    self.__layers[i].y[dt] = mult(self.__layers[i].y[dt], self.__layers[i].dropout[dt])

                # R{y} = sigmoid_prime(y_current) .* R{x_current}
                self.__layers[i].ry[dt] = self.__layers[i].prime(self.__layers[i].y[dt]).dot(self.__layers[i].rx[dt])
                if self.__layers[i].dropout:
                    self.__layers[i].ry[dt] = mult(self.__layers[i].ry[dt], self.__layers[i].dropout[dt])
        return self

    def gv(self, v, weights=None):
        """
            @param v: candidate CG solution, shape = (n_weights, 1)
            @return: Gauss-Newton matrix (packed weights)

            Nic Schraudolph: http://nic.schraudolph.org/pubs/Schraudolph01.pdf

            Sizes of matrices:
                            (ROWS,          COLUMNS)
            J_N' = dx/dw':  (all_weights,   output_nodes)
            J_M  = dy/dx:   (output_nodes,  output_nodes)
            J_N  = dx/dw:   (output_nodes, all_weights)
            v:              (all_weights,  1)
        """

        if weights is None:
            weights = self.network.weights

        self.rop(v, weights)                                                        # J_M * J_N * v
        self.backward(weights, ry=True, damping=self.network.optimizer.damping)     # J_N' * J_M * J_N * v = Gv
        # self.backward(weights, ry=True, damping=None)     # J_N' * J_M * J_N * v = Gv
        return weights.pack(dw=True)

    def clear(self, everything=False, rx=False, dx=False, ry=False, dy=False, x=False, y=False):
        for layer in self.__layers:
            if dy or everything:
                layer.dy.clear()
            if dx or everything:
                layer.dx.clear()
            if ry or everything:
                layer.ry.clear()
            if rx or everything:
                layer.rx.clear()
            if x or everything:
                layer.x.clear()
            if y or everything:
                layer.y.clear()

    @staticmethod
    def term(start, end):
        return [
            Term('index', '>=', start),
            Term('index', '<=', end)
        ]

    def x(self):
        return {
            i: layer.x.all()
            for i, layer in enumerate(self.__layers)
        }

    def y(self):
        return {
            i: layer.y.all()
            for i, layer in enumerate(self.__layers)
        }

    def __iter__(self):
        return self.learn_index.__iter__()

    def __getitem__(self, __slice):
        return self.__layers[__slice]

    def __len__(self):
        return len(self.learn_index)


class Layer(object):
    def __init__(self, context, n, config=None):
        self.n = n
        self.config = config
        self.context = context
        self.network = context.network
        self.market = self.network.market
        self.dtype = self.network.dtype

        self.max_lag = self.__get_max_lag()

        self.neurons = self.config['neurons']
        self.f_name = self.config['sigmoid']
        self.function = fn.sigmoids[self.f_name]
        self.sigmoid = self.function['sigmoid']
        self.prime = self.function['prime']
        self.zero_input = self.function['zero']

        self.odd_n_neurons = self.neurons % 2 == 1
        self.middle = self.__get_middle()

        self.has_targets = 'targets' in config or 'target' in config
        self.targets = self.__get_targets()
        self.external = self.__get_external()

        self.x = X(self)
        self.dx = DX(self)
        self.rx = RX(self)

        self.y = Y(self)
        self.dy = DY(self)
        self.ry = RY(self)

        if self.config.get('dropout'):
            self.dropout = DropOut(self, self.config['dropout'])
        else:
            self.dropout = None

    def loss(self, dt):
        return self.function['cost'](self.targets[dt], self.y[dt])

    def __get_middle(self):
        if self.network.__class__.__name__ == 'RNNAutoEncoder':
            return
        remove_middle = self.config.get('remove_middle') or self.odd_n_neurons

        if remove_middle:
            if remove_middle is True:
                remove_middle = 1 if self.odd_n_neurons else 2
            if remove_middle % 2 == 0 and self.odd_n_neurons:
                raise ValueError('Even remove middle [{0}] for odd n [{1}] of neurons'
                                 .format(remove_middle, self.neurons))
            elif remove_middle % 2 == 1 and not self.odd_n_neurons:
                raise ValueError('Odd remove middle [{0}] for even n [{1}] of neurons'
                                 .format(remove_middle, self.neurons))
            elif self.neurons - remove_middle < 2:
                raise ValueError('Cannot remove all neurons (middle value: {0}, total neurons: {1})'
                                 .format(remove_middle, self.neurons))
        if not remove_middle:
            return [float('inf'), float('-inf')]
        diff = (self.neurons - remove_middle) // 2
        return [diff - 1, self.neurons - diff]

    def __get_max_lag(self):
        if 'max_lag' in self.config:
            return self.config['max_lag']
        lags = []
        for i, layer in enumerate(self.network.config['layers']):
            layer_recurrent_layers_list = layer.get('inputs')
            if layer_recurrent_layers_list and layer_recurrent_layers_list[self.n] is not None:
                for j, recurrent_layer_dict in enumerate(layer_recurrent_layers_list[self.n]):
                    lags.append(recurrent_layer_dict['lag'])
        max_lag = max(lags) if lags else 0
        self.config.update({'max_lag': max_lag})
        # self.network.config.save()
        return max_lag

    def __get_external(self):
        if self.config.get('external'):
            return [
                External(self, js, i)
                for i, js in enumerate(self.config.get('external'))
            ]
        else:
            return []

    def __get_targets(self):
        if self.config.get('target'):       # one quote (neurons)
            return Target(self, self.config.get('target'))
        elif self.config.get('targets'):    # multiple quotes (binned)
            return Targets(self, self.config.get('targets'), 0)
        else:
            return None

    def __getitem__(self):
        error('Layer . __getitem__: Not Implemented')

# http://pytables.github.io/usersguide/libref/hierarchy_classes.html#tables.Group.__contains__


class LayerStage(object):
    def __init__(self, layer):
        self.layer = layer
        self.context = layer.context
        self.market_indexer = self.context.market_indexer
        self.learn_indexer = self.context.learn_indexer

        self.start = self.learn_indexer.shift(self.context.start, -self.layer.max_lag)
        self.learn_index = self.context.learn_indexer.between(self.start, self.context.end)

        self.data = self.empty()
        self.dict = self.get_dict()

    def empty(self):
        return pd.DataFrame(np.zeros((len(self.learn_index), self.layer.neurons), dtype=self.layer.dtype), index=self.learn_index)

    def get_dict(self):
        return {
            dt: self.data.ix[dt].values.reshape(-1, 1)
            for dt in self.learn_index
        }

    def list(self):
        return sorted(self.dict.keys())

    def shifted(self, dt, shift=0):
        return self[self.context.learn_indexer.shift(dt, shift)]

    def between(self, dt_start=None, dt_finish=None, shift=0):
        if dt_start:
            dt_start = self.market_indexer.shift(dt_start, shift)
        if dt_finish:
            dt_finish = self.market_indexer.shift(dt_finish, shift)
        return self[dt_start:dt_finish]

    def clear(self, dt=None):
        if dt is not None:
            self[dt] = np.zeros(self[dt].shape, dtype=self.layer.dtype)
        else:
            for dt, value in self.dict.iteritems():
                self[dt] = np.zeros(self[dt].shape, dtype=self.layer.dtype)

    def __getitem__(self, dt):
        return self.dict[dt]

    def __setitem__(self, key, value):
        self.dict[key] = value


class DropOut(LayerStage):
    """
        net
    """
    def __init__(self, layer, probability):
        self.probability = probability
        super(DropOut, self).__init__(layer)

    def empty(self):
        arr = np.random.choice([0, 1], len(self.learn_index) * self.layer.neurons, p=[self.probability, 1 - self.probability])
        return pd.DataFrame(arr.reshape(len(self.learn_index), self.layer.neurons), dtype=self.layer.dtype, index=self.learn_index)


# net input

class X(LayerStage):
    """
        net
    """
    def __init__(self, layer):
        super(X, self).__init__(layer)


# sigmoid(net)

class Y(LayerStage):
    """
        outputs
    """
    def __init__(self, layer):
        super(Y, self).__init__(layer)

    def prime(self, dt):
        return self.layer.prime(self[dt])

    def to_pct_change(self, dt):
        assert type(self.layer.targets) is Target
        return self.layer.targets.to_pct_change(dt, self[dt])


# dy/dx

class DX(LayerStage):
    """
        dOutputs/dNet
    """
    def __init__(self, layer):
        super(DX, self).__init__(layer)


# dt/dy

class DY(LayerStage):
    """
        dTargets/dOutputs
    """
    def __init__(self, layer):
        super(DY, self).__init__(layer)


# R{x}

class RX(LayerStage):
    """
        R{x} = w.T * R{y_prev} + v.T * y_prev
    """
    def __init__(self, layer):
        super(RX, self).__init__(layer)


# R{y}

class RY(LayerStage):
    """
        R{y} = sigmoid_prime(y_current) .* R{x_current}
    """
    def __init__(self, layer):
        super(RY, self).__init__(layer)


def get_key_and_column(q_string):
    source, indicator, exchange, name, column = q_string.split('|')
    return '/data/{source}/indicators/{indicator}/{exchange}/{q}' \
        .format(source=source, indicator=indicator, exchange=exchange, q=name), column.capitalize()


class External(object):
    def __init__(self, layer, js, n):
        self.layer = layer
        self.config = js

        self.context = layer.context
        self.market = layer.context.market
        self.network = layer.context.network
        self.market_indexer = layer.context.market_indexer
        self.learn_indexer = layer.context.learn_indexer

        self.transforms = self.config.get('transform', [])
        self.shift = self.config.get('shift', 0)

        self.quotes = self.config['quotes']
        self.dropout = self.config.get('dropout')

        self.start = self.learn_indexer.shift(self.context.start, self.shift)
        self.end = self.learn_indexer.shift(self.context.end, self.shift)

        self.raw = self.__get_raw_dataframe()

        self.transformed = self.__transform()
        self.market_index = self.market_indexer.between(self.start, self.end)
        self.dict = self.get_dict()

    def __get_raw_dataframe(self):
        df = pd.DataFrame({q: self.market[q].ix[self.start:self.end] for q in self.quotes}, dtype=self.network.dtype)
        if self.dropout:
            arr = np.random.choice([0, 1], df.shape[0] * df.shape[1], p=[self.dropout, 1 - self.dropout]) \
                .reshape(df.shape)
            df *= arr
        cols = pd.isnull(df).any(axis=0)
        cols = cols[cols].index
        if len(cols):
            raise error('[RAW] cols with NaN: {0}'.format(cols.tolist()))
        return df

    def __get_key(self, key):
        if not self.context.learn:
            with self.context.network.store('r') as store:
                try:
                    return store.get(key)
                except KeyError:
                    return None
        else:
            return None

    def __transform(self):
        data = self.raw
        for t in self.transforms:
            name = t['name']
            kwargs = t.get('arguments', {})
            func = transform.__dict__.get(name)
            if func is None:
                err = '[transform.py] function "{0}" not found!'.format(name)
                self.context.log.error(err)
                error(err)
            else:
                data = data.apply(func, args=(self,), **kwargs)
        cols = pd.isnull(data).any(axis=0)
        cols = cols[cols].index
        if len(cols):
            raise error('[TRANSFORM] cols with NaN: {0}'.format(cols.tolist()))
        return data.astype(self.layer.dtype)

    @property
    def columns(self):
        return self.raw.columns

    def get_dict(self):
        return {
            dt: self.transformed.ix[dt].values.reshape(-1, 1).astype(self.layer.dtype)
            for i, dt in enumerate(self.market_index)
        }

    def __getitem__(self, dt):
        return self.dict[self.learn_indexer.shift(dt, self.shift)]


class Target(object):
    def __init__(self, layer, js):
        self.layer = layer
        self.config = js

        self.context = layer.context
        self.market = layer.context.market
        self.network = layer.context.network
        self.learn_indexer = layer.context.learn_indexer

        self.transforms = self.config.get('transform', [])

        self.log = self.context.network.log

        self.quote = self.__quote()
        self.shift = self.config.get('shift', 0)
        self.start = self.learn_indexer.shift(self.context.start, self.shift)
        self.end = self.learn_indexer.shift(self.context.end, self.shift)

        self.neurons = js['classes']

        self.__raw = self.__get_raw_series()
        self.indicator = self.__get_indicator()
        self.transformed = self.__transform()
        self.classes, self.bins = self.__classes()
        self.processed = self.__neurons_from_classes()
        self.index = self.learn_indexer.between(self.start, self.end)
        self.dict = self.get_dict()
        self.use_index = self.get_skipped()

    def __quote(self):
        q = self.config['quote']
        sampler, indicator, quote, col = q.split('|')
        if '||' in q:
            raise ValueError('Roll is not supported in targets: {0}, roll: {1}'.format(q, q.split('||')[-1]))
        return {
            'item': q,
            'sampler': sampler,
            'indicator': indicator,
            'quote': quote,
            'column': col
        }

    def __get_indicator(self):
        config = self.market.config['data'][self.quote['sampler']]['indicators'][self.quote['indicator']]
        klass = Indicators.get_class(config['name'])
        return klass(config, name=self.quote['indicator'])

    def __get_raw_series(self):
        return self.market[self.quote['item']]

    def __get_pct_series(self, column=None):
        if column is None:
            column = self.quote['column']
        q = '|'.join([self.quote['sampler'], 'pct_change_1', self.quote['quote'], column])
        return self.market[q]

    def __transform(self):
        data = self.__raw.ix[self.start:self.end]
        for t in self.transforms:
            name = t['name']
            kwargs = t.get('arguments', {})
            func = transform.__dict__.get(name)
            if func is None:
                err = '[transform.py] function "{0}" not found!'.format(name)
                self.context.log.error(err)
                error(err)
            else:
                data = data.apply(func, args=(self,), **kwargs)
        return data

    def __classes(self):
        labels, bins = self.__quantile(self.transformed)
        return pd.Series(labels, index=self.transformed.index, dtype=self.layer.dtype), bins

    def __quantile(self, column):
        columns = column.to_frame()
        even_classes = self.neurons % 2 == 0
        if even_classes:
            neg = column[column < 0].to_frame()
            pos = column[column >= 0].to_frame()
            neg_q, neg_bins = pd.qcut(neg, int(self.neurons / 2), retbins=True)
            pos_q, pos_bins = pd.qcut(pos, int(self.neurons / 2), retbins=True)
            bins = {'pos': pos_bins, 'neg': neg_bins}

            neg_labels = pd.Series(neg_q.labels.flatten(), index=neg.index, dtype=self.layer.dtype)
            pos_labels = pd.Series(pos_q.labels.flatten() + int(self.neurons / 2), index=pos.index, dtype=self.layer.dtype)
            series = neg_labels.append(pos_labels).sort_index()
            return series, bins
        else:
            q, bins = pd.qcut(column, self.neurons, retbins=True)
            self.context.network.log.debug('[bins]: {0}'.format(bins))
            return q.labels, bins

    def __neurons_from_classes(self):
        return pd.DataFrame({
            cls: self.classes == cls
            for cls in xrange(self.neurons)
        }, index=self.classes.index, dtype=self.network.dtype)

    def pct(self, dt=None, column=None):
        if dt is None:
            return self.__get_pct_series(column)
        else:
            return self.__get_pct_series(column).ix[dt]

    def to_pct_change(self, dt, outputs=None, col=None):
        if col is None:
            col = self.quote['column']
        output = outputs.argmax()
        if type(self.bins) is dict:
            if output > (self.neurons - 1) / 2:
                bins = self.bins['pos']
                output -= int(self.neurons / 2)
            else:
                bins = self.bins['neg']
        else:
            bins = self.bins
        value = (bins[output] + bins[output + 1]) / 2
        return self.indicator.transform_back(dt, value, self.learn_indexer, self.__raw, self.__get_pct_series(col))

    def get_dict(self):
        return {
            dt: self.processed.ix[dt].values.reshape(-1, 1).astype(self.layer.dtype)
            for i, dt in enumerate(self.index)
        }

    def get_skipped(self):
        index = self.index
        use = self.config.get('use')
        skip = self.config.get('skip')
        if use:
            if 'probability' in use:
                probabilities = [1 - use['probability'], use['probability']]
                choice = np.random.choice([0, 1], len(self.index), p=[probabilities]).astype(np.bool)
                index = index[choice]
            elif 'every' in use:
                index = index[::-use['every']]
        elif skip:
            if 'probability' in skip:
                probabilities = [skip['probability'], 1 - skip['probability']]
                choice = np.random.choice([0, 1], len(self.index), p=[probabilities]).astype(np.bool)
                index = index[choice]
            elif 'every' in skip:
                index_skip = index[::-skip['every']]
                bools = [False if dt in index_skip else True for dt in index]
                index = index[bools]
        return index

    def __getitem__(self, dt):
        return self.dict[self.context.learn_indexer.shift(dt, self.shift)]

    def __iter__(self):
        return self.dict.iteritems()

    def __contains__(self, item):
        return item in self.use_index


class Targets(External):
    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.use_index = self.get_skipped()

    def get_skipped(self):
        index = self.market_index
        use = self.config.get('use')
        skip = self.config.get('skip')
        if use:
            if 'probability' in use:
                probabilities = [1 - use['probability'], use['probability']]
                choice = np.random.choice([0, 1], len(self.market_index), p=[probabilities]).astype(np.bool)
                index = index[choice]
            elif 'every' in use:
                index = index[::-use['every']]
        elif skip:
            if 'probability' in skip:
                probabilities = [skip['probability'], 1 - skip['probability']]
                choice = np.random.choice([0, 1], len(self.market_index), p=[probabilities]).astype(np.bool)
                index = index[choice]
            elif 'every' in skip:
                index_skip = index[::-skip['every']]
                bools = [False if dt in index_skip else True for dt in index]
                index = index[bools]
        return index

    def __contains__(self, item):
        # for x in self.use_index:
        #     print x
        return item in self.use_index


class Timestamp(pd.Timestamp):
    def __init__(self, ts, i=None):
        super(self.__class__, self).__init__(ts)
        self.i = i