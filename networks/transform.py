import numpy as np
from functions import logistic


# rename to binary
def binary(column, instance=None, **kwargs):
    return column > 0


def log_return(column, instance=None, **kwargs):
    column += 1
    column = column.apply(np.log)
    column = std_score(column)
    return logistic(column)
    # return instance.layer.sigmoid(column)

log = log_return


# rename to discrete
def discrete(column, instance=None, **kwargs):
    if instance.layer.has_targets:
        return (column > 0).astype(instance.layer.dtype)
    else:
        return (column > 0) * 2 - 1


# rename to discrete rounded
def central_rounded(column, instance=None, **kwargs):
    return central(column).round()


def unit(x, instance=None, **kwargs):
    x_min = x.min()
    return (x - x_min) / (x.max() - x_min)


def central(x, instance=None, **kwargs):
    x_max = x.max() / 2
    x_min = x.min() / 2
    return (x - x_max - x_min) / (x_max - x_min)


def std(column, instance=None, **kwargs):
    __std = column.std()
    if __std == 0:
        __std = 1
    return (column - column.mean()) / __std

std_score = std_mean = std


def sigmoid(column, instance=None, **kwargs):
    return instance.layer.sigmoid(column)