# -*- coding: utf-8 -*-
from __future__ import division

import numpy as np
from scipy.stats import logistic as scipy_logistic
from scipy.special import expit


# def logistic_prime(y):
#     y = y.flatten()
#     return np.diag(y * (1 - y))


def softmax(x):
    ex = np.exp(x)
    return ex / np.sum(ex)

dot = lambda x, y: x.dot(y)
mult = lambda x, y: x * y
square = lambda x: x ** 2
log = logistic = np.log


sigmoids = {
    "logistic": {
        "sigmoid": expit,
        "prime": lambda y: np.diag(scipy_logistic._pdf(y.ravel())),
        "zero": 0.5,
        "cost": lambda t, y: -np.sum(t * log(y) + (1 - t) * log(1 - y))
    },
    "softmax": {
        "sigmoid": softmax,
        "prime": lambda y: np.diag(y.ravel()) - y.dot(y.transpose()),
        "zero": 0,
        "cost": lambda t, y: -np.sum(t * log(y))
    },
    "tanh": {
        "sigmoid": np.tanh,
        "prime": lambda y: np.diag((1 - y ** 2).ravel()),
        "zero": 0,
        "cost": lambda t, y: square(t - y) / 2
    }
}
