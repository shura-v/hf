from __future__ import division
import numpy as np
from pandas import Timestamp, DataFrame, Series, qcut
from datetime import datetime
from pytz import utc
from dateutil.tz import tzlocal
import re
import os


class Collector(object):
    def __init__(self, network, overwrite=False, _id=None):
        self.network = network
        self.overwrite = overwrite

        self.context = network.context
        self.indexer = network.learn_indexer
        self.id = _id or self.network.id
        self.store = self.network.store
        self.log = self.network.log
        self.last_learn_accuracy = None
        # if os.path.exists(self.network.path.db()):
        #     os.unlink(self.network.path.db())

    @staticmethod
    def now():
        return Timestamp(datetime.utcnow())

    def append(self, **kwargs):
        layer = kwargs.pop('layer')
        index = self.indexer.between(kwargs['predict_start'], kwargs['predict_end'])

        self.last_learn_accuracy = kwargs['accuracy']

        now = self.now()
        df = DataFrame([self.__get_series(i, dt, appended=now, layer=layer, **kwargs)
                        for i, dt in enumerate(index)], index=[now] * len(index))
        outputs = df[range(layer.neurons)].ix[0]
        self.log.debug('OUTPUTS: {0}'.format(outputs.values.flatten()))
        outputs_sorted = sorted(outputs)
        diff = outputs_sorted[-1] - outputs_sorted[-2]
        self.log.debug('diff: {0:.3f}, max: {1:.3f}, min: {2:.3f}'.format(diff, outputs.max(), outputs.min()))
        self.__append(df, layer.n)
        return outputs, df.target_argmax

    def key(self, layer):
        return '/learning/layer_{0}'.format(layer)

    def __append(self, df, layer):
        key = self.key(layer)
        mode = 'r+'
        if self.overwrite:
            mode = 'w'
            self.overwrite = False
        with self.store(mode) as store:
            try:
                store.append(key, df)
            except ValueError:
                self.log.warning('[Structure changed] removing dataframe...')
                store.remove(key)
                store.append(key, df)

    def __get_series(self, i, dt, layer=None, **kwargs):
        keys = 'total_loss', 'accuracy', \
               'learn_start', 'learn_end', 'predict_start', 'predict_end', \
               'epoch'

        outputs = layer.y[dt]

        __dict = {
            'timestamp': dt,
            'id': self.id,
            'target_argmax': layer.targets[dt].argmax(),
            'output_argmax': outputs.argmax(),
            'output_pct': layer.y.to_pct_change(dt),
            'target_pct': layer.targets.pct(dt=dt),
            'predict_i': i,
            'layer': layer.n,
            'learn_accuracy': self.last_learn_accuracy
        }

        __dict.update({i: val for i, val in enumerate(outputs.ravel())})
        __dict.update({
            key: kwargs[key]
            for key in keys
        })
        return Series(__dict)

    def __get_learn_accuracy(self, layer, start, end):
        series = Series([
            layer.y[dt].argmax() == layer.targets[dt].argmax()
            for dt in self.indexer.between(start, end)
        ])
        return len(series[series]) / len(series)


class Predictor(object):
    def __init__(self, collector, layer=None, current_id=True, epoch=None, choose=None, force_keep_middle=None):
        """
            current_id: [Timestamp, True = collector.id, False (all)]
            choose: [None, int]
        """
        self.collector = collector
        self.network = collector.network
        self.log = collector.log
        self.layer = layer or self.network.weights[-1]
        self.neurons = self.layer.neurons

        self.id = self.collector.id if current_id is True else current_id
        self.choose = choose if choose is not None else self.network.config['optimizer'].get('choose', 1)

        if force_keep_middle:
            self.middle = [float('inf'), float('-inf')]
        else:
            self.middle = self.layer.middle

        self.epoch = 'all' if epoch is None else epoch

        # self.log.info('[middle] {0} [indexes] {1}'.format(self.middle, self.__array_from_middle()))
        self._dataframe = None

    def __array_from_middle(self):
        arr = np.zeros((self.layer.neurons, ), dtype=int)
        arr[np.arange(self.layer.neurons) <= self.middle[0]] = 1
        arr[np.arange(self.layer.neurons) >= self.middle[1]] = 1
        return arr

    @property
    def data(self):
        if self._dataframe is None:
            with self.collector.store('r') as store:
                df = store[self.collector.key(self.layer.n)]
            df = df[df.layer == self.layer.n]
            if self.id:
                df = df[df.id == self.id]
            if self.epoch != 'all':
                df = df[df.epoch == self.epoch]
            outputs_removed_middle = (df.output_argmax <= self.middle[0]) | (df.output_argmax >= self.middle[1])
            # __df = df
            # __df = __df.set_index('predict_end')
            # __df = __df[(__df.output_argmax == 3) | (__df.output_argmax == 0)]
            # # __df = __df[(__df.learn_accuracy > 0.7)]
            # __df = __df[(__df.epoch == 14)]
            # __df = __df[['output_argmax', 'target_argmax']]
            # __df['output_argmax_2'] = (__df['output_argmax'] / (self.layer.neurons - 1)).round()
            # __df['target_argmax_2'] = (__df['target_argmax'] / (self.layer.neurons - 1)).round()
            # __df = __df.at_time('10:00')
            # print __df[['output_argmax_2','target_argmax_2']]
            # print len(__df)
            #
            # eq = __df['output_argmax_2'] == __df['target_argmax_2']
            # from matplotlib.pyplot import show
            # print eq.sum() / len(eq)
            # show()
            self._dataframe = df[outputs_removed_middle]
        return self._dataframe

    @property
    def outputs(self):
        df = self.data.set_index('timestamp', append=True)
        return df[range(self.neurons)]

    @property
    def targets(self):
        return self.data.groupby('timestamp')['target_argmax'].mean()

    @staticmethod
    def _accuracy(series):
        return len(series[series]) / len(series) if len(series) else np.nan

    @property
    def accuracy(self):
        results, back_transformed_dataframe = self.results
        if results is None:
            return
        __dict = {
            'a_trans': self._accuracy(results['back_transformed']),
            # 'a__trans_005': self._accuracy(results['back_transformed'].ix[-5:]),
            # 'a__trans_010': self._accuracy(results['back_transformed'].ix[-10:]),
            # 'a__trans_025': self._accuracy(results['back_transformed'].ix[-25:]),
            # 'a__trans_050': self._accuracy(results['back_transformed'].ix[-50:]),
            # 'a__trans_100': self._accuracy(results['back_transformed'].ix[-100:]),
            'a_exact': self._accuracy(results['equal']),
            'binned': self._accuracy(results['binned_equal']),
            'binned_025': self._accuracy(results['binned_equal'].ix[-25:]),
            'binned_050': self._accuracy(results['binned_equal'].ix[-50:]),
            'binned_100': self._accuracy(results['binned_equal'].ix[-100:]),
            'targets': ','.join([repr(x) for x in results['targets'].tolist()[-10:]]),
            'outputs': ','.join([repr(x) for x in results['outputs_argmax'].tolist()[-10:]]),
            'ep': self.epoch
        }
        return __dict

    @property
    def results(self):
        data = self.data
        grouped = data.groupby('timestamp')
        outputs = grouped[range(self.neurons)].mean()
        outputs_argmax = outputs.idxmax(axis=1)
        outputs_max = outputs.max(axis=1)
        outputs_min = outputs.min(axis=1)
        outputs_median = outputs.median(axis=1)

        index = outputs_argmax.index
        if self.choose > 1:
            if len(outputs_max) < self.choose:
                return [None] * 2
            q = qcut(outputs_max, self.choose)
            index = index[q.labels >= self.choose - 1]
            outputs_argmax = outputs_argmax.ix[index]
        targets = self.targets.ix[index]
        middle = (self.neurons - 1) / 2
        # odd_n_neurons = self.neurons % 2 != 0
        equal = outputs_argmax == targets
        binned_outputs = outputs_argmax >= middle
        binned_targets = targets >= middle
        binned_equal = binned_outputs == binned_targets

        back_transformed = grouped[['output_pct', 'target_pct']].mean().reindex(index)
        df = DataFrame({
            'targets': targets,
            'equal': equal,
            'binned_outputs': binned_outputs,
            'binned_targets': binned_targets,
            'binned_equal': binned_equal,
            'outputs_argmax': outputs_argmax,
            'outputs_max': outputs_max.reindex(index),
            'outputs_min': outputs_min.reindex(index),
            'outputs_median': outputs_median.reindex(index),
            'back_transformed': np.sign(back_transformed).sum(axis=1) != 0
        }, index=index)
        return df, back_transformed

    def __len__(self):
        return len(self.data) if self.data is not None else 0


class MultiCollector(Collector):
    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.dt = None

    def append(self, **kwargs):
        layer = kwargs.pop('layer')
        self.dt = kwargs['predict_start']
        outputs = layer.y[self.dt]
        targets = layer.targets[self.dt]
        o_sum = outputs.sum()
        t_sum = targets.sum()
        n = outputs.size
        self.log.info('OUTPUTS [sum: {o}] [{o_pct:.3f}] [targets: {t}] [{t_pct:.3f}] [size: {neurons}]'
                      .format(o=o_sum, o_pct=o_sum / n, t=t_sum, t_pct=t_sum / n, neurons=n))
        if n > 1:
            self.log.info('MAX: {1}, MIN: {2}'
                          .format(np.std(outputs.flatten()), np.max(outputs), np.min(outputs)))
        equal = np.equal(outputs.round(), targets).transpose()
        df = DataFrame(equal, index=[kwargs['predict_start']], columns=layer.targets.columns)
        self.__append(df, layer.n, **kwargs)

    def key(self, layer, epoch=None):
        return '/encoder/id_{0}/layer_{1}/epoch_{2}'.format(self.id, layer, epoch)

    def __append(self, df, layer, **kwargs):
        key = self.key(layer, kwargs['epoch'])
        mode = 'r+'
        with self.store(mode) as store:
            if key in store:
                df = store[key].append(df)
                accuracies = store.get_storer(key).attrs.accuracies + [kwargs['accuracy']]
            else:
                accuracies = []
            store.put(key, df)
            store.get_storer(key).attrs.accuracies = accuracies + [kwargs['accuracy']]
            # self.log.info(np.array(store.get_storer(key).attrs.accuracies))


class MultiPredictor(Predictor):
    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)

    @property
    def data(self):
        if self._dataframe is None:
            with self.collector.store('r') as store:
                self._dataframe = store[self.collector.key(self.layer.n, self.epoch)]
        return self._dataframe

    def _accuracy(self, n=None):
        df = self.data
        if n > 0:
            df = df.ix[-n:]
        series = df.mean()
        series.sort(ascending=False)
        return Series(series)

    @property
    def accuracy(self):
        return self._accuracy()

    @property
    def results(self):
        accuracies = {
            '010_acc': self._accuracy(10).values,
            '025_acc': self._accuracy(25).values,
            '050_acc': self._accuracy(50).values,
            '100_acc': self._accuracy(100).values,
            'all_acc': self._accuracy().values
        }

        __dict = dict(accuracies)

        if self.layer.neurons > 1:
            __dict.update({
                '025__names': self._accuracy(25).index,
                '050__names': self._accuracy(50).index,
                '100__names': self._accuracy(100).index,
                'all__names': self._accuracy().index
            })

        df = DataFrame(__dict)
        df.sort(columns=['all_acc'], ascending=False)
        __mean = DataFrame(accuracies).mean().to_frame().transpose()
        return DataFrame(df), __mean

    def __len__(self):
        return len(self.data) if self.data is not None else 0


class Plotter(object):
    def __init__(self, network, collector=None):
        self.network = network
        self.collector = Collector()