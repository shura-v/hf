# TODO-done: SGD, HF (08-03-2014)
from __future__ import division

import numpy as np

from networks.weights import Weights


np.set_printoptions(linewidth=300, precision=20, suppress=True)


class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class Optimizer(object):
    def __init__(self, network):
        self.network = network
        self.dtype = network.dtype
        self.context = network.context
        self.weights = network.weights

        self.config = network.config['optimizer']
        self.log = self.network.log

        self.loss_old = None
        self.loss_new = None
        self.grad = None

        self.accuracies = self.config.get('accuracies')
        self.rates = self.config.get('rates')
        self.rate_boost = self.config.get('rate_boost')
        self.learn_iter = None
        self.epoch = None
        self.stop = self.config.get('stop')
        self.context = self.network.context

        self.__solution = None
        self.update_rate = self.rates
        self.__update_weights = None
        self.__rho = None

    @property
    def epochs(self):
        if self.accuracies:
            return len(self.accuracies)
        elif type(self.rates) is list:
            return len(self.rates)
        else:
            return self.config['epochs']

    def update(self):
        self.context = self.network.context
        self.weights = self.network.weights
        self.loss_old = self.context.forward(test_time=False).loss()
        return self

    @property
    def rate(self):
        if type(self.rates) is list:
            return self.rates[self.epoch] if len(self.rates) > self.epoch else self.rates[-1]
        else:
            if self.rate_boost:
                boost = self.rate_boost ** self.epoch
            else:
                boost = 1
            self.log.debug('rates: {0}, boost: {1}'.format(self.rates, boost))
            return self.rates * boost

    @property
    def solution(self):
        return self.__solution if self.__solution is not None else np.zeros(self.grad.shape)

    @solution.setter
    def solution(self, x):
        self.__solution = x

    def line_search(self):
        self.log.debug('Line search...')
        rate = self.rate
        c = 0.01
        j = 0
        total = 100

        prev_loss = self.loss_new
        while j < total:
            if prev_loss <= self.loss_old + c * rate * float(self.grad.T.dot(self.solution)):
                break
            rate *= 0.8
            j += 1
            prev_loss = self.context.forward(self.weights + self.solution * rate, test_time=False).loss()

        if j == total:
            rate = 0

        return rate


class HF(Optimizer):
    def __init__(self, network):
        super(HF, self).__init__(network)
        self.cg = CG(self)
        self.network.log.debug('======= HF =======')
        self.network.log.debug('Learning...')
        self.damping = self.__get_damping()

    @property
    def iter(self):
        return self.cg.iter

    def optimize(self, n, i):
        self.learn_iter = n
        self.epoch = i
        self.update()
        self.grad = self.context.backward()
        self.cg.update()
        self.solution = self.cg.solve()
        if not self.accuracies:
            self.solution = self.cg.backtrack()
        self.update_loss()
        self.damp()
        self.update_rate = self.rate
        if self.config.get('line_search'):
            self.update_rate = self.line_search()
        return self.weights + self.solution * self.update_rate

    def damp(self):
        if self.damping is None:
            return self.damping
        self.rho()
        if self.__rho > 3 / 4:
            self.damping.lmbda *= self.damping.drop
        elif self.__rho < 1 / 4:
            self.damping.lmbda *= self.damping.boost
        if self.damping.lmbda >= self.damping.max_lambda:
            self.damping.lmbda = self.damping.max_lambda
        self.log.debug('Damp... lambda: {0}, rho: {1}'.format(self.damping.lmbda, self.__rho))
        return self.damping

    def update_loss(self):
        self.loss_new = self.context.forward(self.weights + self.solution, test_time=False).loss()

    def rho(self):
        # rho numerator
        numerator = self.loss_new - self.loss_old

        # rho denominator
        # b'*H*b; b = cg solution (w_opt - w_current)
        gv = self.context.gv(self.solution)
        hess_term = self.solution.T.dot(gv)
        grad_term = self.solution.T.dot(self.grad)  # b'J
        denominator = float(0.5 * (hess_term + grad_term))
        self.__rho = numerator / denominator

        if numerator > 0:  # new loss is larger than previous
            self.__rho = float("-infinity")

    def log_iter(self, i, loss, accuracy=0.):
        self.network.log.info('[{iter:2}] CG iter: {cg_iter:3}/{maxiters}, loss: {loss:.3f}, rate: {rate:.3f}, '
                              'accuracy: {accuracy:.5f}'
                              .format(iter=i, cg_iter=self.cg.iter, maxiters=self.cg.maxiters,
                                      loss=loss, rate=self.update_rate, accuracy=accuracy))

    def __get_damping(self):
        return Damping(self, self.config.get('damping')) if self.config.get('damping') else None


class CG(object):
    def __init__(self, optimizer):
        self.optimizer = optimizer
        self.network = optimizer.network
        self.context = optimizer.context
        self.weights = optimizer.weights
        self.accuracies = optimizer.accuracies
        self.log = optimizer.network.log

        self.gv = optimizer.context.gv
        self.grad = None
        self.decay = optimizer.config.get('decay', 0)
        self.maxiters = optimizer.config['cg'].get('maxiters', 100)
        self.miniters = optimizer.config['cg'].get('miniters', 10)

        self.shape = (len(self.weights), self.maxiters + 1)

        self.x = X(self)
        self.r = R(self)
        self.d = D(self)

        self.values = None
        self.iter = None

    def update(self):
        self.grad = self.optimizer.grad
        self.context = self.optimizer.context
        self.weights = self.optimizer.weights
        self.values = CGValues(self, gap_ratio=0.1, min_gap=self.miniters, tolerance=1e-2)
        self.x = self.x.clear(self.decay * self.optimizer.solution)
        self.r = self.r.clear(self.gv(self.x[0]) - self.grad)
        self.d = self.d.clear(-self.r[0])
        return self

    def solve(self):
        """
        http://www.cs.cmu.edu/~quake-papers/painless-conjugate-gradient.pdf
        http://www.math.tau.ac.il/~turkel/notes/cg2.pdf

        @return: self (see self.solution)
        """
        val = (-self.grad + self.r[0]).T.dot(self.x[0])  # (b - (b - Gv)) = Gv.dot(w_opt - w)
        self.values += 0.5 * float(val)
        self.iter = 0
        for i in xrange(self.maxiters):
            x = self.x[i]
            r = self.r[i]
            d = self.d[i]

            ad = self.gv(d)
            dad = float(d.T.dot(ad))

            if dad <= 0:
                raise Exception('Negative Curvature')

            alpha = r.T.dot(r) / dad
            x_new = x + alpha * d
            r_new = r + alpha * ad

            beta = r_new.T.dot(r_new) / r.T.dot(r)
            d_new = -r_new + beta * d

            self.d += d_new
            self.r += r_new
            self.x += x_new

            # Verifying convergence criteria
            self.values += 0.5 * float((-self.grad + r_new).T.dot(x_new))

            # self.log.debug('CG iter: {cg_iter}/{cg_max_iter}, improvement: {improvement}, val: {current}, prev: {prev}'
            #                .format(cg_iter=i, cg_max_iter=self.maxiters,
            #                        improvement=self.values.improvement(),
            #                        current=self.values.current(),
            #                        prev=self.values.prev()))

            if self.values.condition(self.x[-1]):
                return self.x[-1]
            self.iter += 1
        return self.x[-1]

    # In Martens' paper, he allows conjugate-gradient to terminate then (if necessary) backtracks until he finds
    # a weight update that performs the 'best', starting from the end and working backward.
    # His criteria for 'best' in this case is pretty straight forward: check whether the previous step did better
    # than the current step, and if so take a step back -- stopping when the previous step didn't do better.
    def backtrack(self):
        self.log.debug('Backtrack...')
        current_x = self.x[-1]
        current_loss = self.context.forward(self.weights - current_x, test_time=False).loss()

        for i in range(len(self.x))[::-1]:
            prev_x = self.x[i]
            prev_loss = self.context.forward(self.weights - prev_x, test_time=False).loss()
            if prev_loss > current_loss:
                return current_x
            else:
                current_x = prev_x
                current_loss = prev_loss
        return self.x[0]


class CGValues(object):
    def __init__(self, cg, gap_ratio=0., min_gap=10, tolerance=1e-3):
        self.cg = cg
        self.optimizer = cg.optimizer
        self.network = cg.network
        self.context = cg.context
        self.accuracies = cg.accuracies
        self.rate = self.optimizer.rate
        self.epoch = self.optimizer.epoch
        self.accuracy_tolerance = self.optimizer.config.get('accuracy_tolerance', 0.001)

        self.gap_ratio = gap_ratio
        self.min_gap = min_gap
        self.tolerance = tolerance
        self.max_test_gap = int(max(cg.maxiters * self.gap_ratio, self.min_gap))
        self.__values = [None] * self.max_test_gap
        self.__current = None
        self.__iter = 0

    def __iadd__(self, value):
        return self.append(value)

    def append(self, value):
        if self.__current is None:
            self.__current = 0
        else:
            self.__current = (self.__current + 1) % self.max_test_gap
        self.__values[self.__current] = value
        self.__iter += 1
        return self

    def current(self):
        return self.__values[self.__current % self.max_test_gap] if self.__current is not None else None

    def prev(self):
        return self.__values[(self.__current - 1) % self.max_test_gap] if self.__current is not None else None

    def improvement(self):
        return (self.current() - self.prev()) / self.current()

    def gap(self):
        return max(self.__current * self.gap_ratio, self.min_gap)

    def condition(self, x):
        if self.accuracies:
            weights = Weights(self.network, self.cg.weights.pack() + x * self.rate)
            accuracy = self.network.accuracy(weights)
            target = self.accuracies[self.epoch]
            self.optimizer.log.debug('[{learn_i:3}] [{epoch:2}] acc/target: {0:.5f}/{1:.3f}'
                                    .format(accuracy, self.accuracies[self.epoch], epoch=self.epoch,
                                            learn_i=self.optimizer.learn_iter))
            if accuracy >= target or (self.accuracies[self.epoch] - accuracy < self.accuracy_tolerance):
                return True
        else:
            return (self.improvement() < self.tolerance * self.gap() and
                    self.gap() < self.__iter - 1 and
                    self.prev() < 0 and
                    self.cg.miniters < self.__iter - 1)


class Iters(object):
    def __init__(self, method):
        self.method = method
        self.optimizer = method.optimizer
        self.shape = method.shape
        self.__data = np.zeros(self.shape, dtype=self.optimizer.dtype)
        self.__len = 0

    def __getitem__(self, item):
        if type(item) is int:
            if item == -1:
                return self.__data[:, self.__len - 1:self.__len]
            return self.__data[:, item:item + 1]
        else:
            return self.__data[:, item]

    def __setitem__(self, key, value):
        if type(key) is int:
            self.__data[:, key:key + 1] = value
            self.__len = max(key + 1, self.__len)
        else:
            self.__data[:, key.start:key.stop] = value
            self.__len = max(key.stop, self.__len)

    def __iadd__(self, other):
        self.append(other)
        return self

    def __len__(self):
        return self.__len

    def append(self, other):
        self[self.__len] = other

    def clear(self, first=None):
        self.__data *= 0
        self.__len = 0
        if first is not None:
            self[0] = first
        return self

    @property
    def len(self):
        return self.__len

    def last(self):
        return self[self.__len - 1]


class X(Iters):
    def __init__(self, method):
        super(X, self).__init__(method)


class R(Iters):
    def __init__(self, method):
        super(R, self).__init__(method)


class D(Iters):
    def __init__(self, method):
        super(D, self).__init__(method)


class Damping(object):
    def __init__(self, optimizer, config):
        self.optimizer = optimizer
        self.__mu = [config.get('mu')]
        self.__drop = [config.get('drop')]
        self.__boost = [config.get('boost')]
        self.__lambda = [config.get('lambda')]
        self.max_lambda = config.get('max_lambda', 3)

    @property
    def lmbda(self):
        return self.__lambda[-1]

    @lmbda.setter
    def lmbda(self, x):
        self.__lambda.append(x)

    @property
    def drop(self):
        return self.__drop[-1]

    @drop.setter
    def drop(self, x):
        self.__drop.append(x)

    @property
    def boost(self):
        return self.__boost[-1]

    @boost.setter
    def boost(self, x):
        self.__boost.append(x)

    @property
    def mu(self):
        return self.__mu[-1]

    @mu.setter
    def mu(self, x):
        self.__mu.append(x)


class SGD(Optimizer):
    def __init__(self, network):
        Optimizer.__init__(self, network)
        self.momentum = self.config.get("momentum", False)
        self.nesterov = self.config.get("nesterov", False)
        self.regularization = self.config.get("regularization", 1.)
        self.v_prev = None
        self.network.log.debug('======= SGD =======')
        self.network.log.debug('Learning...')

    def optimize(self, n, i):
        self.update()

        rate = self.rate / 1000

        if self.nesterov:
            next_weights = self.weights
            if self.v_prev is not None:
                next_weights += self.momentum * self.v_prev
            grad = self.context.forward(next_weights, test_time=False).backward()
            v_next = rate * grad
        else:
            grad = self.context.forward(test_time=False).backward()
            v_next = rate * grad
            if self.v_prev is not None:
                v_next += self.momentum * self.v_prev

        self.v_prev = v_next
        return self.weights + v_next - self.weights.pack() * rate * self.regularization

    def log_iter(self, i, loss, accuracy=0.):
        self.network.log.info('[{iter:2}] loss: {loss:.3f}, rate: {rate:.5f}, accuracy: {accuracy:.5f}'
                               .format(iter=i, loss=loss, rate=self.rate / 1000, accuracy=accuracy))