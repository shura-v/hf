from networks.network import RBF
from tools.tools import decode_json
import json
import numpy as np


class Template(object):
    """
        'binary' (0, 1)
        'unit' (0, ..., 1)
        'discrete' (-1, 1)
        'central_rounded' (-1, 0, 1)
        'central' (-1, ... , -1)
        'sigmoid'
        'std_mean' = 'std_score'
    """
    __template__ = '''{
        "market": "fx.40T",
        "log": {
            "file": "INFO",
            "console": "INFO"
        },
        "load": {
            "weights": false
        },
        "layers": [
            {
                "dropout": null,
                "neurons": 100,
                "bias": {"weights": {"eps": 0}},
                "external": [
                    {
                        "dropout": null,
                        "shift": -1,
                        "transform": [
                            {"name": "discrete"}
                        ],
                        "weights": {},
                        "quotes": [
                            "mean|pct_change_1|EURUSD.COMP|C",
                            "mean|pct_change_2|EURUSD.COMP|C",
                            "mean|pct_change_3|EURUSD.COMP|C",
                            "mean|pct_change_4|EURUSD.COMP|C",
                            "mean|pct_change_5|EURUSD.COMP|C",
                            "mean|pct_change_8|EURUSD.COMP|C",
                            "mean|pct_change_10|EURUSD.COMP|C",
                            "mean|pct_change_12|EURUSD.COMP|C",
                            "mean|pct_change_15|EURUSD.COMP|C",
                            "mean|pct_change_20|EURUSD.COMP|C",
                            "mean|pct_change_24|EURUSD.COMP|C",
                            "mean|pct_change_48|EURUSD.COMP|C",
                            "mean|pct_change_72|EURUSD.COMP|C",
                            "mean|pct_change_36|EURUSD.COMP|C",
                            "mean|pct_change_36|EURUSD.COMP|C||-1",
                            "mean|pct_change_36|EURUSD.COMP|C||-2",
                            "mean|pct_change_36|EURUSD.COMP|C||-3",
                            "mean|pct_change_36|EURUSD.COMP|C||-4",
                            "mean|pct_change_36|EURUSD.COMP|C||-5",
                            "mean|pct_change_36|EURUSD.COMP|C||-6",
                            "mean|pct_change_36|EURUSD.COMP|C||-7"
                        ]
                    }
                ],
                "sigmoid": "tanh"
            },
            {
                "remove_middle": false,
                "target": {
                    "quote": "mean|pct_change_36|EURUSD.COMP|C",
                    "shift": 0,
                    "classes": 4
                },
                "inputs": [
                    [
                        {"lag": 0, "weights": {}}
                    ],
                    null
                ],
                "bias": {},
                "sigmoid": "softmax"
            }
        ],
        "optimizer": {
            "rates": -0.2,
            "rate_boost": 1.5,
            "name": "HF",
            "regularization": 0.8,
            "epochs": 5,
            "momentum": 0.99,
            "cg": {
                "decay": 0,
                "maxiters": 100,
                "miniters": 10
            },
            "damping": {
                "mu": 0.1,
                "drop": 0.666,
                "boost": 1.5,
                "lambda": 0,
                "max_lambda": 0.01
            },
            "stop": {},
            "line_search": true,
            "choose": 3
        },
        "learner": {
            "times": ["17:20"],
            "learn": 250,
            "start": "2013-05-06",
            "end": "2013-06-01"
        }
    }'''

    @staticmethod
    def keywords(keywords):
        kw = dict(keywords)
        for key, value in kw.iteritems():
            kw[key] = json.dumps(value)
        return kw

    def config(self, keywords):
        config = str(self.__template__)
        for key, value in keywords.iteritems():
            config = config.replace('%{0}%'.format(key), value)
        return json.loads(config, object_hook=decode_json)

    def dict(self, **kwargs):
        keywords = self.keywords(kwargs)
        return self.config(keywords)


def lin_reg(a, b):
    inv = np.linalg.pinv
    return inv(a.T.dot(a)).dot(a.T.dot(b))


def old_(x, mu, y, w, centers, examples):
    y_mu = np.zeros((centers, examples))
    for center in range(centers):
        diffs = x - mu[center, :]
        for example in range(examples):
            diff = diffs[example, :]
            y_mu[center, example] = diff.dot(diff)

    A = y_mu.transpose()
    Y = y.T.copy()
    inv = np.linalg.pinv
    w1 = inv(A.T.dot(A)).dot(A.T.dot(Y))
    w2 = lin_reg(y_mu.transpose().copy(), y.transpose().copy())
    w3 = np.linalg.lstsq(y_mu.transpose().copy(), y.transpose().copy())[0]
    return y_mu, w3


def new_(x, mu, y, centers, examples):
    y_mu = np.zeros((centers, examples))
    for center in range(centers):
        diffs = x - mu[center:center + 1, :]
        y_mu[center, :] = (diffs ** 2).sum(axis=1)
    return y_mu, lin_reg(y_mu.transpose(), y.transpose())


def vec_(x, mu, y):
    mu = mu.reshape(mu.shape[0], 1, mu.shape[1])
    y_mu = np.sum((x - mu) ** 2, axis=-1)
    return y_mu, lin_reg(y_mu.transpose(), y.transpose())


def main():
    features = 20  # features
    examples = 300  # examples
    centers = 300  # centers
    outputs = 100  # outputs for each example

    x = np.random.random((examples, features))
    mu = np.random.random((centers, features))
    y = np.random.random((outputs, examples))
    w = np.random.random((centers, outputs))

    # old_y_mu, old_w = old_(x, mu, y, w, centers, examples)
    new_y_mu, new_w = new_(x, mu, y, centers, examples)
    vec_y_mu, vec_w = vec_(x, mu, y)
    # print old_y_mu.shape
    # print new_y_mu.shape
    # print vec_y_mu.shape
    # print new_w.T.dot(x)
    print new_w.shape
    print w.shape
    # print new_w.T.dot(new_y_mu)
    print new_w.T.dot(new_y_mu)
    print vec_w.T.dot(vec_y_mu)


if __name__ == '__main__':
    main()


    # print y


    # rbf = RBF(name="rbf", config=Template().dict())
    # print rbf