from networks.network import RNN, RNNAutoEncoder
import json
from tools.tools import decode_json


class Template(object):
    """
        'binary',
        'central',
        'central_rounded',
        'discrete',
        'sigmoid',
        'std_mean', 'std_score',
        'unit'
    """
    __template__ = '''{
        "market": "fx.60T",
        "log": {
            "file": "INFO",
            "console": "INFO"
        },
        "dates": {},
        "load": {
            "weights": false
        },
        "layers": [
            {
                "dropout": 0,
                "neurons": 100,
                "inputs": [
                    [
                        {"lag": 1, "weights": {}}
                    ],
                    [
                        {"lag": 1, "weights": {}}
                    ]
                ],
                "bias": {},
                "external": [
                    {
                        "dropout": 0,
                        "shift": -1,
                        "transform": [
                            {"name": "std_score"}
                        ],
                        "weights": {},
                        "quotes": [
                            "mean|pct_change_24|EURUSD.COMP|C",
                            "mean|pct_change_24|EURUSD.COMP|C||-1",
                            "mean|pct_change_24|EURUSD.COMP|C||-2",
                            "mean|pct_change_24|EURUSD.COMP|C||-3",
                            "mean|pct_change_24|EURUSD.COMP|C||-4",
                            "mean|pct_change_24|EURUSD.COMP|C||-5",
                            "mean|pct_change_24|EURUSD.COMP|C||-6",
                            "mean|pct_change_24|EURUSD.COMP|C||-7",
                            "mean|pct_change_24|EURUSD.COMP|C||-8",
                            "mean|pct_change_24|EURUSD.COMP|C||-9",
                            "mean|pct_change_24|EURUSD.COMP|C||-10",
                            "mean|pct_change_24|EURUSD.COMP|C||-11",
                            "mean|pct_change_24|EURUSD.COMP|C||-12",
                            "mean|pct_change_24|EURUSD.COMP|C||-13",
                            "mean|ma_5|EURUSD.COMP|C",
                            "mean|macd|EURUSD.COMP|C",
                            "mean|mad_5_10|EURUSD.COMP|C",
                            "mean|rsi|EURUSD.COMP|C"
                        ]
                    }
                ],
                "sigmoid": "tanh"
            },
            {
                "remove_middle": %remove_middle%,
                "target": {
                    "quote": "mean|pct_change_24|EURUSD.COMP|C",
                    "shift": 0,
                    "classes": %classes%
                },
                "inputs": [
                    [
                        {"lag": 0, "weights": {}}
                    ],
                    null
                ],
                "bias": {},
                "sigmoid": "softmax"
            }
        ],
        "optimizer": {
            "rates": -0.2,
            "name": "HF",
            "regularization": 0.8,
            "epochs": %epochs%,
            "momentum": 0.99,
            "cg": {
                "decay": 0,
                "maxiters": 100,
                "miniters": 10
            },
            "damping": {
                "mu": 0.1,
                "drop": 0.6666666666666666666666666666666666666666,
                "boost": 1.5,
                "lambda": 0,
                "max_lambda": 2
            },
            "stop": {},
            "line_search": true
        },
        "predictor": [
            {"choose": %choose%}
        ],
        "learner": {
            "times": %times%,
            "learn": %learn%,
            "start": %start%,
            "end": %end%
        }
    }'''

    @staticmethod
    def keywords(keywords):
        kw = dict(keywords)
        for key, value in kw.iteritems():
            kw[key] = json.dumps(value)
        return kw

    def config(self, keywords):
        config = str(self.__template__)
        for key, value in keywords.iteritems():
            config = config.replace('%{0}%'.format(key), value)
        return json.loads(config, object_hook=decode_json)

    def dict(self, **kwargs):
        keywords = self.keywords(kwargs)
        return self.config(keywords)


def run():
    times = [['00:00'], ['01:00'], ['02:00'], ['03:00'], ['04:00'], ['05:00'], ['06:00'], ['07:00'], ['08:00'], ['09:00'],
             ['10:00'], ['11:00'], ['12:00'], ['13:00'], ['14:00'], ['15:00'], ['16:00'], ['17:00'], ['18:00'], ['19:00'],
             ['20:00'], ['21:00'], ['22:00'], ['23:00']]
    times = [['17:00']]

    for i, t in enumerate(times):
        config = Template().dict(times=t, remove_middle=False, choose=1, classes=4,
                                 start="2013-01-01", end="2014-01-01", learn=150, epochs=10)
        n = RNN(name='2013_03_{0}'.format(i), config=config)
        n.learn()


if __name__ == '__main__':
    run()