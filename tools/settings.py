import os

folder = '/Volumes/E (HFS+)/Z/'


def get_folder(name, sub_folder=folder):
    if os.path.isdir(name):
        return os.path.join(sub_folder, name.lower())


def get_path(key):
    return os.getenv('NETWORK_CONFIG_' + key)


PATHS = {
    'db': get_folder('db'),
    'csv': get_folder('csv'),
    'json': get_folder('json'),
    'pickle': get_folder('pickle'),
    'yaml': get_folder('yaml'),
    'log': get_folder('log'),
    'code': get_folder('code'),
    'python': '/usr/local/bin/python'
}
