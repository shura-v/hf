import json
import os
import logging
import sys
import yaml

from time import sleep
from datetime import datetime, time

import settings
from mypprint import pformat


class Access(object):
    def __init__(self, instance, mode='r', store=False):
        self.mode = mode
        self.store = store
        self.instance = instance
        self.path = instance.path.busy()

    def __enter__(self):
        if self.busy:
            if self.mode == 'r' and self.busy == 'r' * len(self.busy):
                pass
            elif not self.get(self.mode):
                error('[ERROR] Maximum tries exceeded (busy)', self.instance.log)
        if self.mode != 'r' and not os.path.isfile(self.instance.path.db()):
            self.mode = 'w'
        self.busy = self.mode
        return self.instance

    # noinspection PyUnusedLocal
    def __exit__(self, exc_type, exc_value, traceback):
        self.busy = False

    @property
    def busy(self):
        if not os.path.exists(self.path):
            return False
        try:
            with open(self.path, 'r') as f:
                return f.read()
        except IOError:
            sleep(1)
            return self.busy

    @busy.setter
    def busy(self, value):
        try:
            if self.mode == 'r':
                if os.path.isfile(self.path):
                    with open(self.path, 'r') as f:
                        values = f.read()
                    if value is False:
                        values = values[:-1]
                    else:
                        values += 'r'
                    os.unlink(self.path)
                    if values:
                        with open(self.path, 'w') as f:
                            f.write(values)
                else:
                    with open(self.path, 'w') as f:
                        f.write(value)
            else:  # 'w', 'r+'
                if os.path.isfile(self.path):
                    os.unlink(self.path)
                elif value:
                    with open(self.path, 'w') as f:
                        f.write(value)
        except (IOError, TypeError, WindowsError), e:
            self.instance.log.exception(e)
            sleep(1)
            self.busy = value

    def get(self, mode='r', attempts=0, timeout=5000, sleep_secs=1):
        if attempts < timeout:
            if not self.busy:
                return True
            else:
                self.instance.log.warning('Busy... ({0}) [{1}/{2}]'.format(self.instance.name,
                                                                           attempts + 1,
                                                                           timeout))
                sleep(sleep_secs)
                return self.get(mode, attempts + 1, timeout, sleep_secs)


class BlankStore(object):
    def __exit__(self, exc_type, exc_value, traceback):
        pass

    def __enter__(self):
        return None


class Compression(object):
    def __init__(self, instance):
        self.level = 0
        self.lib = None

        config = instance.config.get('compression')

        if config is None:
            return
        self.lib = config.get('lib')
        self.level = config.get('level')
        if self.level is None and self.lib is not None:
            self.level = 0


class Config(object):
    def __init__(self, instance, config=None, create=False):
        self.instance = instance
        self.name = instance.name
        self.create = create
        self.path = Path(self, sub_folder=instance.sub_folder)
        self.yaml = self.path.config()
        if config:
            self.dict = config
        else:
            self.source = Path.get_source(self.name, sub_folder=instance.sub_folder)
            self.dict = self.__load()
        if self.dict is None:
            raise error('No config (source="{0}", yaml="{1}", [create={2}]'.format(self.source, self.yaml, self.create))

    @property
    def sub_folder(self):
        return self.instance.sub_folder

    def __load(self):
        if self.create or not os.path.exists(self.yaml):
            with open(self.source, mode='r') as f:
                return json.load(f, object_hook=decode_json)
        else:
            with open(self.yaml, mode='r') as f:
                return yaml.load(f)

    def save(self):
        with open(self.yaml, 'w') as f:
            yaml.dump(self.dict, f, indent=4, explicit_start=True)

    def get(self, key, __default=None):
        return self.dict.get(key, __default)

    def __getitem__(self, item):
        return self.get(item)

    def __setitem__(self, key, value):
        self.dict[key] = value

    def __repr__(self):
        if hasattr(self, 'dict'):
            return '{0}.{1}:\n{2}\n.'.format(__name__, self.__class__.__name__, pformat(self.dict, indent=4))
        else:
            return self.__class__.__name__


class Logger(object):
    def __init__(self, instance, console=None, f=None, level=None):
        self.name = instance.name
        self.folder = instance.path.log()

        self.levels = self.Levels(instance, level=level, console=console, f=f)
        self.start = datetime.now()
        self.filename = '{0} {1}'.format(self.start.__str__().replace(':', '-'), self.name)

        self.logger = logging.getLogger(self.filename)
        self.logger.setLevel('DEBUG')

        self.file_formatter = LogFormatter("[{0}] %(asctime)s [%(elapsed)s] [%(elapsed_prev)s] "
                                           "[%(levelname)-5.5s] %(message)s".format(self.name))
        self.console_formatter = LogFormatter("[{0}] %(aasctime)s [%(elapsed)s] [%(elapsed_prev)s] "
                                              "[%(levelname)-5.5s] %(message)s".format(self.name))

        if self.levels.file is not False:
            self.file_handler = logging.FileHandler("{0}/{1}.log".format(self.folder, self.filename), delay=True)
            self.file_handler.setFormatter(self.file_formatter)
            self.logger.addHandler(self.file_handler)
            self.file_handler.setLevel(self.levels.file)

        if self.levels.console is not False:
            self.console_handler = logging.StreamHandler(sys.stdout)
            self.console_handler.setFormatter(self.console_formatter)
            self.console_handler.setLevel(self.levels.console)
            self.logger.addHandler(self.console_handler)

    class Levels(object):
        def __init__(self, instance, level=None, console=None, f=None):
            self.levels = instance.config.get('log', {})
            self.levels.setdefault('console', 'INFO')
            self.levels.setdefault('file', 'DEBUG')

            if level is False:
                self.levels.update({'console': False, 'file': False})
            elif type(level) is str:
                if level == 'console':
                    self.levels.update({'console': 'DEBUG', 'file': False})
                elif level == 'file':
                    self.levels.update({'console': False, 'file': 'DEBUG'})
                else:
                    self.levels.update({'console': level, 'file': level})
            elif console is not None:
                self.levels['console'] = console
            elif f is not None:
                self.levels['file'] = f
            self.file = self.levels['file']
            self.console = self.levels['console']

        def __repr__(self):
            return 'console: {0}, f: {1}'.format(self.console, self.file)

    def debug(self, message):
        return self.logger.debug(message)

    def info(self, message):
        return self.logger.info(message)

    def warn(self, message):
        return self.warning(message)

    def warning(self, message):
        return self.logger.warning(message)

    def exception(self, e):
        return self.logger.exception(e)

    def error(self, message):
        return self.logger.error(message)

    def critical(self, message):
        return self.logger.critical(message)


class LogFormatter(logging.Formatter):
    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.start = datetime.now()
        self.prev = datetime.now()

    def format(self, record):
        now = datetime.now()
        record.aasctime = datetime.now().strftime('%H:%M:%S')
        record.elapsed = (datetime.combine(now, time()) + (now - self.start)).strftime('%H:%M:%S')
        record.elapsed_prev = (datetime.combine(now, time()) + (now - self.prev)).strftime('%H:%M:%S')
        self.prev = now
        return super(LogFormatter, self).format(record)


class Path(object):
    def __init__(self, instance, sub_folder=''):
        self.instance = instance
        self.name = self.instance.name
        self.sub_folder = sub_folder or instance.sub_folder

    def db(self, name=None):
        if name is None:
            name = self.name
        folder = os.path.join(settings.get_path('DB'), self.sub_folder)
        return os.path.join(folder, name + '.h5')

    def busy(self):
        folder = os.path.join(settings.get_path('DB'), self.sub_folder)
        if not os.path.exists(folder):
            os.mkdir(folder)
        return os.path.join(folder, self.name + '.busy')

    def config(self):
        return Path._config(self.name, self.sub_folder)

    @classmethod
    def _config(cls, filename, sub_folder=''):
        folder = os.path.join(settings.get_path('YAML'), sub_folder)
        if not os.path.exists(folder):
            os.mkdir(folder)
        return os.path.join(folder, filename + '.yaml')

    def source(self, yml=False):
        return Path.get_source(self.name, yml, self.sub_folder)

    @classmethod
    def get_source(cls, filename, yml=False, sub_folder=None):
        if yml:
            return os.path.join(settings.get_path('YAML'), sub_folder or '', filename + '.yaml')
        else:
            return os.path.join(settings.get_path('JSON'), sub_folder or '', filename + '.json')

    def log(self):
        return Path._log(self.sub_folder)

    @classmethod
    def _log(cls, sub_folder=''):
        folder = os.path.join(settings.get_path('LOG'), sub_folder)
        if not os.path.exists(folder):
            os.mkdir(folder)
        return folder

    def csv(self, use_name=True):
        if use_name:
            return os.path.join(settings.get_path('CSV'), self.name)
        else:
            return settings.get_path('CSV')


def yield_chunks(arr, n):
    for i in range(0, len(arr), n):
        yield arr[i:i + n]


class Error(Exception):
    pass


def error(msg, log=None):
    try:
        raise Error(msg)
    except Error, e:
        if log is not None:
            log.error(e.message)
        raise Error(msg)


def decode_json(data):
    return _decode_dict(data)


def _decode_list(data):
    rv = []
    for item in data:
        if isinstance(item, unicode):
            item = item.encode('utf-8')
        elif isinstance(item, list):
            item = _decode_list(item)
        elif isinstance(item, dict):
            item = _decode_dict(item)
        rv.append(item)
    return rv


def _decode_dict(data):
    rv = {}
    for key, value in data.iteritems():
        if isinstance(key, unicode):
            key = key.encode('utf-8')
        if isinstance(value, unicode):
            value = value.encode('utf-8')
        elif isinstance(value, list):
            value = _decode_list(value)
        elif isinstance(value, dict):
            value = _decode_dict(value)
        rv[key] = value
    return rv