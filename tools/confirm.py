import sys


def confirm(message):
    yes = {'yes', 'y', 'ye', ''}
    no = {'no', 'n'}

    sys.stdout.write(message + ' [y/n] ')

    choice = raw_input().lower()
    if choice in yes:
        return True
    elif choice in no:
        return False
    else:
        sys.stdout.write("Please respond with '(y)es)' or '(n)o'")
