from pandas import *
import numpy as np
from datetime import datetime
import pandas as pd

# df = DataFrame(range(10), range(0, 10))
# df.index = range(10, 20)
# print df

index_from = pd.date_range('2009-01-01', '2016-01-01', freq='T')
s = pd.Series(np.arange(len(index_from)), index_from)

index_to = pd.date_range('2010-01-01', '2015-05-30', freq='H')
bins = index_to.insert(0, datetime.fromtimestamp(0)).astype(np.long)
labels = pd.cut(s.index.astype(np.long), bins=bins).labels
print len(labels)

agg = s.groupby(labels).last().ix[0:]
print pd.Series(agg.values, index_to)
