import sys

from markets.market import Market


def main():
    market = Market(name='FX.60T', create=True)
    market.config.save()


if __name__ == '__main__':
    main()