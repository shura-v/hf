from __future__ import division

from matplotlib.pyplot import plot, show
from pandas import *

from market.market import Market
from markets.dataprovider import DataProvider


def main():
    market1 = Market(argv=sys.argv, filename='all.240T.30T')
    market2 = Market(argv=sys.argv, filename='all.240T.10T')
    p = market1['mean|pct_change|USDCAD.COMP|C']
    p.plot()
    r = market1.provider['USDCAD.COMP']['C'].reindex(p.index, method='ffill').ffill().pct_change()
    r.plot('r')

    p = p > 0
    r = r > 0

    e = p == r
    print len(e[e]) / len(p)

    show()


def ccut(s):
    perc_min = np.percentile(s, 40)
    perc_max = np.percentile(s, 60)
    return s[(s < perc_min) | (s > perc_max)]


def perc(s1, s2):
    s1 = s1 > 0
    s2 = s2 > 0
    return s1[s1 == s2]


def cols():
    m = Market(argv=sys.argv, filename='all.30T.5T')
    p1 = m['most|pct_change|USDCAD.COMP|HLC'].fillna(0)
    p2 = m['most|pct_change|USDCAD.COMP|C'].fillna(0)

    # p1 = ccut(p1)
    # p2 = ccut(p2)

    print perc(p1, p2).size / p1.size


def corrs():
    m = Market(argv=sys.argv, filename='all.60T.5T')
    p1 = m['most|pct_change|RUT.X|HL'].shift(1)
    p2 = m['most|pct_change|RUT.X|HL']
    a = rolling_corr(p1, p2, 3)
    print a
    print a[a < 0].size / a.size
    show()



def raw():
    r = DataProvider(name='all_150.5T')
    r['EUR.X']['HL'].dropna().plot()
    show()

def compare_with_raw():
    # m = Market(argv=sys.argv, filename='all.60T.5T')
    r = DataProvider(name='all_800.D')
    m = Market(filename='all.D.60T')
    mdf = m['most|RUA.X|C'].ix['2013':]
    rdf = r['RUA.X']['C'].dropna().reindex(mdf.index, method='ffill').interpolate().ix['2013':]
    eq = (mdf.pct_change() >= 0) == (rdf.pct_change() >= 0)
    print len(mdf[eq]) / len(mdf)
    mdf.plot(label='mdf')
    rdf.plot(label='rdf')
    print mdf.ix[-2]
    print rdf.ix[-2]
    show()


def compare_sampled():
    m = Market(filename='all.D.60T')
    i1 = m['most|RUA.X|C']
    i2 = m['mean|RUA.X|HLC']
    i1.plot()
    i2.plot()
    eq = (i1.pct_change() >= 0) == (i2.pct_change() >= 0)
    print sum(eq) / len(i1)
    show()

def interpol():
    df = Series([np.nan] * 20)
    df.ix[5:15] = range(11)
    df.ix[7:9] = np.nan
    print df.interpolate()


def plot_raw():
    # p = DataProvider(filename='all_5000.D')
    # print p
    p = Market(filename='all.D.D')
    with p.db.get('r') as db:
        print db['/data/most/source'].items
    a = p['most|pct_change|EURUSD.COMP|H'].ix['2014':]
    b = p['most|ewma_5|EURUSD.COMP|H'].ix['2014':]
    a = (a - a.mean()) / a.std()
    b = (b - b.mean()) / b.std()
    a.plot()
    b.plot()
    show()


def plot_transforms():
    p = Market(filename='test')
    data = p['most|pct_change|EURUSD.COMP|C']
    plot(p.t['most|zigzag_c|EURUSD.COMP'])
    # p.t['most|chaikin_osc|EURUSD.COMP'].plot(label='Chaikin oscillator')
    show()


def plot_volumes(f, q):
    p = DataProvider(name=f)
    data = p[q]['PV']
    data = data.groupby(lambda x: x.time()).sum()
    data = (data - data.mean()) / (data.std())
    data.plot()

def plot_quote(f, q, col=None):
    p = DataProvider(name=f)[q]
    if col:
        p = p[col]
    p.plot()

if __name__ == '__main__':
    # main()
    # raw()
    # cols()
    # corrs()
    # compare_with_raw()
    # interpol()
    # compare_sampled()
    # plot_raw()
    # plot_transforms()
    # plot_volumes('all_900.45T', 'USDCHF.COMP')
    # plot_volumes('all_900.45T', 'GBPUSD.COMP')
    # plot_volumes('all_900.45T', 'USDJPY.COMP')
    plot_quote('30T.1500D', 'USDCHF.COMP', 'C')
    # plot_quote('all_900.45T', 'GBPUSD.COMP')
    # plot_quote('all_900.45T', 'USDJPY.COMP')
    # plot_volumes('all_400.30T')
    show()