import sys
sys.path.append('z:\\code')
from networks import network


def main():
    net = network.RNNAutoEncoder(argv=sys.argv, create=True)
    try:
        net.learn()
    except Exception, e:
        net.log.exception(e)
    # collector = Collector(net)


if __name__ == '__main__':
    main()
