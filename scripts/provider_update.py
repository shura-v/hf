import sys

from markets.dataprovider import DataProvider


if __name__ == '__main__':
    # with DataProvider(argv=sys.argv, name='all.D').access('r+') as provider:
    #     provider.update()
    # with DataProvider(argv=sys.argv, name='fx.1T').access('r+') as provider:
    #     provider.update()
    with DataProvider(argv=sys.argv, name='nyse.5T.removed').access('r+') as provider:
        provider.update()
    # with DataProvider(argv=sys.argv, name='nyse.5T').access('r+') as provider:
    #     provider.update()