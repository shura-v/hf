import sys
import optparse

from networks import network


def learn(options):
    net = network.RNN(options, name=options.name, source=options.source)
    net.check_losses()


def main():
    argv = sys.argv
    if "--" not in argv:
        argv = []
    else:
        argv = argv[argv.index("--") + 1:]

    parser = optparse.OptionParser()
    parser.add_option("-s", "--source", dest="source", help="Path to source JSON")
    parser.add_option("-n", "--name", dest="name", help="Network name")

    options, args = parser.parse_args(argv)

    if not argv:
        parser.print_help()
        return

    learn(options)


if __name__ == '__main__':
    main()
