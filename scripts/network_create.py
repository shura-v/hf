import sys

from networks.network import RNN


def main():
    network = RNN(argv=sys.argv, create=True)
    network.config.save()
    print "Done! New network:", network.name


if __name__ == '__main__':
    main()
