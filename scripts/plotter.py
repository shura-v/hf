from networks.network import RNN
from networks.statistics import Predictor


def main():
    net = RNN('fx', no_print=True, collector='20140520005541638000')
    # data = net.market.provider['EURUSD.COMP']['PV']
    # data.plot()
    # show()
    predictor = Predictor(net.collector, current_id='20140520005541638000', force_keep_middle=True)
    print predictor.results


if __name__ == '__main__':
    main()