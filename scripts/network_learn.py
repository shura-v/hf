import sys
sys.path.append('z:\\code')
from networks import network


def main():
    net = network.RNN(argv=sys.argv, create=True)
    # print net.learn_indexer.index[87]
    # print net.market_indexer.shift(net.learn_indexer.index[87], -36)
    # print net.learn_indexer.shift(net.learn_indexer.index[87], -1)
    try:
        net.learn()
    except Exception, e:
        net.log.exception(e)


if __name__ == '__main__':
    main()
