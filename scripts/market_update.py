from sys import argv

from markets.market import Market


def main():
    with Market(name='fx.60T', argv=argv, create=True).access('r+') as market:
        print market['mean|mad_3_7']
        # with market.provider.access('r'):
        #     market.update()


if __name__ == '__main__':
    main()