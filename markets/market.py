# -*- coding: utf-8 -*-

# TODO-maybe: Panel (?)
# TODO-done-wrong: 01.04.2014 raw_last_valid in place of raw_updated
# TODO-done: 01.04.2014 added exchanges list to market or 'all'
# TODO-done: 03.04.2014 fixed all update issues
# TODO-done: 18.04.2014 new version!
# TODO-done: 03.05.2014 make DB object (hdfstore or mysql)
# TODO-done: 03.05.2014 Log levels (process, index, update)
# TODO: MySQL ?
# TODO: print errors in parse_source (try/except)

from __future__ import division

import optparse
from datetime import datetime

from pandas import cut, Panel
import numpy as np
from pandas.stats import moments as moments

from markets.dataprovider import DataProvider
from markets.indicators import Transforms, Indicators
from tools.tools import Access, Config, Logger, Path, error
from markets.indexer import TimeIndex
from database.db import Store


class DB(Store):
    def __init__(self, *args, **kwargs):
        super(DB, self).__init__(*args, **kwargs)
        self.log = self.instance.log

    def update(self, panel, key=None):
        self.log.info('Updating DB...')
        if panel.shape[1] == 0:
            self.log.info('Empty Panel')
            return
        if key is None:
            key = self.__keys['data']
        with self.get('r+') as db:
            db.put(key, panel)


class Market(object):
    dtype = np.float32

    def __init__(self, name=None, config=None, argv=None, create=False, log=None):
        self.options = self.__parse_options(argv)
        self.sub_folder = 'markets'
        self.name = self.options.name or name
        self.config = self.Config(self, config=config, create=create)
        self.freq = self.config['freq']

        self.path = Path(self)
        self.access = lambda *args, **kwargs: Access(self, *args, **kwargs)
        self.log = Logger(self, level=log)
        self.indexer = TimeIndex(**self.config['index'])
        self.db = DB(self)
        self.provider = DataProvider(name=self.config['provider'], log='console')
        self.data = MarketPanel(self)
        self.t = self.Transforms(self)

    @staticmethod
    def __parse_options(argv=None):
        if argv is None:
            argv = []
        parser = optparse.OptionParser()
        parser.add_option("-n", "--name", dest="name")
        parser.add_option("-m", "--no_multiprocessing", action="store_true", dest="no_multiprocessing", default=False)
        options, args = parser.parse_args(argv)
        return options

    def error(self, msg):
        try:
            error(msg)
        except Exception, e:
            self.log.error(e.message)

    @staticmethod
    def get_source_column_class(col_name):
        return {
            'O': Open,
            'H': High,
            'L': Low,
            'C': Close,
            'OHLC': OHLC,
            'HLC': HLC,
            'HL': HL,
            'PV': PV,
            'TV': TV,
            'OI': OI
        }[col_name]

    def update(self):
        columns = self.config['columns']
        for s_name, d_config in self.config['data'].iteritems():

            s_config = d_config.get('source')
            __source = SourcePanel(self, s_name, s_config, columns)
            __source.create()
            __source.save()

            i_config = d_config.get('indicators')
            if i_config:
                self.log.info('[{0}] Indicators...'.format(s_name))
                __indicators = Indicators(self, __source, i_config)
                __indicators.create()
                __indicators.save()

                t_config = d_config.get('transforms')
                if t_config:
                    self.log.info('[{0}] Transforms...'.format(s_name))
                    __transforms = Transforms(self, __source, __indicators, t_config)
                    __transforms.create()
                    __transforms.save()

        self.log.info('Done!')

    class Config(Config):
        def __init__(self, *args, **kwargs):
            super(self.__class__, self).__init__(*args, **kwargs)

        def add_network(self, name):
            networks = set(self.instance.config['networks'])
            networks.add(name)
            self.instance.config['networks'] = list(networks)
            self.instance.config.save()

    class Transforms(object):
        def __init__(self, market):
            self.market = market

        def __getitem__(self, item):
            args = item.split('|')
            return self.market.get_transform(*args)

    def get_source(self, s_name, key, column, roll=0):
        return self.data.source(s_name, key, column, roll=roll)

    def get_indicator(self, s_name, i_name, key, column, roll=0):
        return self.data.indicator(s_name, i_name, key, column, roll=roll)

    def get_transform(self, s_name, i_name, key, roll=0):
        return self.data.transform(s_name, i_name, key, roll=roll)

    def __getitem__(self, item):
        roll = 0
        if '||' in item:
            item, roll = item.split('||')
            roll = -int(roll)
        args = item.split('|')
        if item.startswith('T|'):
            return self.get_transform(*args[1:], roll=roll)
        elif len(args) == 3:
            return self.get_source(*args, roll=roll)
        elif len(args) == 4:
            return self.get_indicator(*args, roll=roll)
        raise NotImplementedError('Wrong mapping {0}'.format(item))


class MarketPanel(object):
    def __init__(self, market):
        self.market = market
        self.data = self.__struct()

    def __struct(self):
        return {
            source_name: {
                'source': None,
                'indicators': {
                    indicator_name: None
                    for indicator_name in config.get('indicators', {}).keys()
                },
                'transforms': {
                    transform_name: None
                    for transform_name in config.get('transforms', {}).keys()
                }
            }
            for source_name, config in self.market.config['data'].iteritems()
        }

    def source(self, s_name, key, column, roll=0):
        if self.data[s_name]['source'] is None:
            with self.market.db.get('r') as db:
                db_key = 'data/{0}/source'.format(s_name)
                self.data[s_name]['source'] = db[db_key]
        data = self.data[s_name]['source'][key][column]
        return data.shift(roll)

    def indicator(self, s_name, i_name, key, column, roll=0):
        if self.data[s_name]['indicators'][i_name] is None:
            with self.market.db.get('r') as db:
                db_key = 'data/{0}/indicators/{1}'.format(s_name, i_name)
                self.data[s_name]['indicators'][i_name] = db[db_key]
        data = self.data[s_name]['indicators'][i_name][key][column]
        return data.shift(roll)

    def transform(self, s_name, i_name, key, roll=0):
        if self.data[s_name]['transforms'][i_name] is None:
            with self.market.db.get('r') as db:
                db_key = 'data/{0}/transforms/{1}'.format(s_name, i_name)
                self.data[s_name]['transforms'][i_name] = db[db_key]
        data = self.data[s_name]['transforms'][i_name].ix[key]
        return data.shift(roll)


class SourcePanel(object):
    def __init__(self, market, name, config, columns):
        self.market = market
        self.name = name
        self.config = config
        self.columns = columns
        self.log = market.log
        self.key = '/data/{0}/source'.format(self.name)
        self.__panel = None

    @property
    def panel(self):
        if self.__panel is None:
            with self.market.db.get('r') as db:
                return db[self.key]
        else:
            return self.__panel

    def create(self):
        cols = []
        for col_name in self.columns:
            cols.append(parse_column(self.market.name, col_name, self.config))
        self.__panel = Panel({
            col['name']: col['dataframe']
            for col in cols
        }, dtype=self.market.dtype).transpose(2, 1, 0)

    def save(self):
        if self.__panel is None:
            return self
        with self.market.db.get('r+') as db:
            db.put(self.key, self.__panel)


def parse_column(market_name, col, config):
    market = Market(market_name, log='console')
    market.log.info('Column start: {0}'.format(col))
    klass = market.get_source_column_class(col)
    source_column = klass(market, config)
    return source_column.dict()


class SourceColumn(object):
    def __init__(self, market, col_name, config):
        self.market = market
        self.log = market.log
        self.config = config

        self.provider = market.provider
        self.indexer = market.indexer
        self.freq = market.indexer.freq

        self.col_name = col_name

        self.raw = None
        self.index = None
        self.__initialize()

        self.dataframe = self.__get()

    def dict(self):
        return {
            'name': self.col_name,
            'dataframe': self.dataframe
        }

    def __initialize(self):
        with self.provider.db.get(mode='r') as db:
            self.raw = db[self.provider.db['data']].ix[:, :self.provider.updated, self.col_name]
        self.index = self.indexer.between(self.raw.index[0], self.raw.index[-1])

    def __get(self):
        return self.__aggregate().interpolate().bfill()

    def __aggregate(self):
        bins = self.index.insert(0, datetime.fromtimestamp(0)).astype(np.long)
        labels = cut(self.raw.index.astype(np.long), bins=bins)
        resampler = self.Resampler(self.config)
        aggregated = self.raw.groupby(labels).aggregate(*resampler.aggregate).ix[0:]
        aggregated.index = self.index
        return aggregated

    class Resampler(object):
        def __init__(self, config):
            self.name = config.get('name', 'mean')
            self.kwargs = config.get('kwargs', {})
            self.func = {
                'ewma': self.ewma,
                'most': self.most,
                'mean': self.mean,
                'median': self.median
            }[self.name]
            self.aggregate = (self.func, self.kwargs)

        @staticmethod
        def ewma(x, kwargs):
            kwargs = dict(kwargs)
            kwargs.setdefault('adjust', False)
            x = x.dropna().values
            if len(x) == 0:
                return np.nan
            elif len(x) == 1:
                return x[0]
            elif kwargs.get('span') is None:
                kwargs['span'] = len(x)
            return moments.ewma(x, **kwargs).values[-1]

        @staticmethod
        def most(x, kwargs):
            return x.ffill().values[-1] if len(x) else np.nan

        @staticmethod
        def mean(x, kwargs):
            return x.mean()

        @staticmethod
        def median(x, kwargs):
            return x.median()


class Open(SourceColumn):
    def __init__(self, market, config):
        super(Open, self).__init__(market, 'O', config)

    class Resampler(SourceColumn.Resampler):
        def __init__(self, config):
            super(self.__class__, self).__init__(config)

        @staticmethod
        def ewma(x, kwargs):
            kwargs = dict(kwargs)
            kwargs.setdefault('adjust', False)
            x = x.dropna().values
            if len(x) == 0:
                return np.nan
            elif len(x) == 1:
                return x[0]
            elif kwargs.get('span') is None:
                kwargs['span'] = len(x)
            x = moments.ewma(x[::-1], **kwargs)
            return x[-1]

        @staticmethod
        def most(x, kwargs):
            return x.bfill().values[0] if len(x) else np.nan


class High(SourceColumn):
    def __init__(self, market, config):
        super(High, self).__init__(market, 'H', config)

    class Resampler(SourceColumn.Resampler):
        def __init__(self, config):
            super(self.__class__, self).__init__(config)

        @staticmethod
        def ewma(x, kwargs):
            kwargs = dict(kwargs)
            kwargs.setdefault('adjust', False)
            x = x.dropna().values
            if len(x) == 0:
                return np.nan
            elif len(x) == 1:
                return x[0]
            elif kwargs.get('span') is None:
                kwargs['span'] = len(x)
            x = np.sort(x, axis=0)
            x = moments.ewma(x, **kwargs)
            return x[-1] if len(x) > 0 else np.nan

        @staticmethod
        def most(x, kwargs):
            return x.max()


class Low(SourceColumn):
    def __init__(self, market, config):
        super(Low, self).__init__(market, 'L', config)

    class Resampler(SourceColumn.Resampler):
        def __init__(self, config):
            super(self.__class__, self).__init__(config)

        @staticmethod
        def ewma(x, kwargs):
            kwargs = dict(kwargs)
            kwargs.setdefault('adjust', False)
            x = x.dropna().values
            if len(x) == 0:
                return np.nan
            elif len(x) == 1:
                return x[0]
            elif kwargs.get('span') is None:
                kwargs['span'] = len(x)
            x = np.sort(x, axis=0)
            x = moments.ewma(x[::-1], **kwargs)
            return x[-1]

        @staticmethod
        def most(x, kwargs):
            return x.min()


class Close(SourceColumn):
    def __init__(self, market, config):
        super(Close, self).__init__(market, 'C', config)


class OHLC(SourceColumn):
    def __init__(self, market, config):
        super(OHLC, self).__init__(market, 'OHLC', config)


class HLC(SourceColumn):
    def __init__(self, market, config):
        super(HLC, self).__init__(market, 'HLC', config)

    class Resampler(SourceColumn.Resampler):
        def __init__(self, config):
            super(self.__class__, self).__init__(config)

        @staticmethod
        def most(x, kwargs):
            if len(x) == 0:
                return np.nan
            else:
                return (x.max() + x.min() + x.ffill().values[-1]) / 3


class HL(SourceColumn):
    def __init__(self, market, config):
        super(HL, self).__init__(market, 'HL', config)

    class Resampler(SourceColumn.Resampler):
        def __init__(self, config):
            super(self.__class__, self).__init__(config)

        @staticmethod
        def most(x, kwargs):
            if len(x) == 0:
                return np.nan
            else:
                return (x.max() + x.min()) / 2


class PV(SourceColumn):
    def __init__(self, market, config):
        super(PV, self).__init__(market, 'PV', config)


class TV(SourceColumn):
    def __init__(self, market, config):
        super(TV, self).__init__(market, 'TV', config)


class OI(SourceColumn):
    def __init__(self, market, config):
        super(self.__class__, self).__init__(market, 'OI', config)


def initializer(terminating_=None):
    # This places terminating in the global namespace of the worker subprocesses.
    # This allows the worker function to access `terminating` even though it is
    # not passed as an argument to the function.
    global terminating
    terminating = terminating_