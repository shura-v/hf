from __future__ import division
import numpy as np
from pandas.stats import moments
from pandas import Series, Panel, Panel4D


class Indicators(object):
    def __init__(self, market, source, config):
        self.market = market
        self.db = market.db
        self.log = self.market.log
        self.source = source
        self.name = source.name
        self.config = config
        self.array = self.__get_array()
        self.panel = None

    def key(self, label=None):
        if label:
            return '/data/{0}/indicators/{1}'.format(self.name, label)
        else:
            return '/data/{0}/indicators'.format(self.name)

    def __get_array(self):
        __array = []
        for name, __config in self.config.iteritems():
            klass = self.get_class(__config['name'])
            instance = klass(__config, name=name, source=self.source)
            __array.append(instance)
        return __array

    def create(self):
        self.panel = Panel4D({
            indicator.name: indicator.get()
            for indicator in self.array
        }, dtype=self.market.dtype)

    def save(self):
        with self.db.get('r+') as db:
            for label in self.panel.axes[0]:
                db.put(self.key(label), self.panel[label])

    @staticmethod
    def get_class(name):
        return {
            'pct_change': PctChange,
            'rolling_mean': MovingAverage,
            'mad': MovingAverageDifference,
            'ewma': ExpMovingAverage,
            'macd': MACD,
            'macd_histogram': MACDHistogram,
            'macd_signal': MACDSignal,
            'rsi': RSI,
            'stok': StochasticK,
            'sto': StochasticD
        }[name]


class Indicator(object):
    def __init__(self, config, name=None, source=None, panel=None):
        self.source_panel = self.__set_panel(source, panel)
        self.market = source.market if source is not None else None
        self.log = self.market.log if self.market is not None else None
        self.panel = None
        self.config = config
        self.name = name
        self.kwargs = self.config.get('kwargs') or {}
        self.dtype = np.float32

    def get_panel(self):
        raise Exception('Indicator Class not found: {0}'.format(self.__class__.__name__))

    def get(self):
        self.log.info('[Indicator] {0} {1}'.format(self.__class__.__name__, self.config))
        return self.get_panel()

    def transform_back(self, dt, value, index, transformed, pct, *args, **kwargs):
        return value

    def __set_panel(self, source, panel):
        if panel is not None:
            return panel
        elif source is not None:
            return source.panel


class PctChange(Indicator):
    def __init__(self, *args, **kwargs):
        super(PctChange, self).__init__(*args, **kwargs)

    def get_panel(self):
        return self.source_panel.apply(self.__apply)

    def __apply(self, series):
        return series.pct_change(**self.kwargs)


class MovingAverage(Indicator):
    def __init__(self, *args, **kwargs):
        super(MovingAverage, self).__init__(*args, **kwargs)

    def get_panel(self):
        return self.source_panel \
            .apply(self.__apply) \
            .apply(self.__pct_change)

    def __apply(self, series):
        return moments.rolling_mean(series, **self.kwargs)

    @staticmethod
    def __pct_change(series):
        return series.pct_change()

    # def transform_back(self, dt, value, indexer, transformed, pct, *args, **kwargs):
    #     start = indexer.shift(dt, -self.kwargs['window'] + 1)
    #     end = indexer.shift(dt, -1)
    #     return self.kwargs['window'] * value - pct.ix[start:end].sum()


class MovingAverageDifference(Indicator):
    def __init__(self, *args, **kwargs):
        super(MovingAverageDifference, self).__init__(*args, **kwargs)
        self.windows = self.kwargs['windows']

    def get_panel(self):
        return self.source_panel \
            .apply(self.__apply) \
            .apply(self.__pct_change)

    def __apply(self, series):
        return moments.rolling_mean(series, self.windows[0]) - moments.rolling_mean(series, self.windows[1])

    @staticmethod
    def __pct_change(series):
        return series.pct_change()

    # def transform_back(self, dt, value, indexer, transformed, pct, *args, **kwargs):
    #     start = indexer.shift(dt, -self.kwargs['window'] + 1)
    #     end = indexer.shift(dt, -1)
    #     return self.kwargs['window'] * value - pct.ix[start:end].sum()


class ExpMovingAverage(Indicator):
    def __init__(self, *args, **kwargs):
        super(ExpMovingAverage, self).__init__(*args, **kwargs)
        self.kwargs.setdefault('adjust', False)
        if self.kwargs.get('com') is None:
            self.kwargs.setdefault('span', 12)

    # def transform_back(self, dt, value, indexer, transformed, pct, *args, **kwargs):
    #     dt_prev = indexer.shift(dt, -1)
    #     prev_value = transformed.ix[dt_prev]
    #     if self.kwargs.get('com') is not None:
    #         alpha = 1 / float(1 + self.kwargs['com'])
    #     else:
    #         alpha = 2 / float(1 + self.kwargs['span'])
    #     print alpha, value, prev_value, (value + prev_value * (alpha - 1)) / alpha
    #     return (value + prev_value * (alpha - 1)) / alpha

    def get_panel(self):
        return self.source_panel \
            .apply(self.__apply) \
            .apply(self.__pct_change)

    def __apply(self, series):
        return moments.ewma(series, **self.kwargs)

    @staticmethod
    def __pct_change(series):
        return series.pct_change()


class MACD(Indicator):
    def __init__(self, *args, **kwargs):
        super(MACD, self).__init__(*args, **kwargs)
        self.kwargs.setdefault('fast', 12)
        self.kwargs.setdefault('slow', 26)
        self.fast = None
        self.slow = None

    def get_panel(self):
        self.fast = self.source_panel.apply(lambda x: moments.ewma(x, adjust=False, span=self.kwargs['fast']))
        self.slow = self.source_panel.apply(lambda x: moments.ewma(x, adjust=False, span=self.kwargs['slow']))
        return self.fast.apply(self.__apply)

    def __apply(self, fast):
        item, column = fast.name
        return fast - self.slow[item][column]


class MACDSignal(Indicator):
    def __init__(self, *args, **kwargs):
        super(MACDSignal, self).__init__(*args, **kwargs)
        self.kwargs.setdefault('fast', 12)
        self.kwargs.setdefault('slow', 26)
        self.kwargs.setdefault('span', 9)
        self.fast = None
        self.slow = None
        self.macd = None

    def get_panel(self):
        self.fast = self.source_panel.apply(lambda x: moments.ewma(x, adjust=False, span=self.kwargs['fast']))
        self.slow = self.source_panel.apply(lambda x: moments.ewma(x, adjust=False, span=self.kwargs['slow']))
        self.macd = self.fast.apply(self.__macd_apply)
        return self.macd.apply(self.__signal_apply)

    def __macd_apply(self, fast):
        item, column = fast.name
        return fast - self.slow[item][column]

    def __signal_apply(self, macd):
        return moments.ewma(macd, adjust=False, span=self.kwargs['span'])


class MACDHistogram(Indicator):
    def __init__(self, *args, **kwargs):
        super(MACDHistogram, self).__init__(*args, **kwargs)
        self.kwargs.setdefault('fast', 12)
        self.kwargs.setdefault('slow', 26)
        self.kwargs.setdefault('span', 9)
        self.fast = None
        self.slow = None
        self.macd = None
        self.signal = None

    def get_panel(self):
        self.fast = self.source_panel.apply(lambda x: moments.ewma(x, adjust=False, span=self.kwargs['fast']))
        self.slow = self.source_panel.apply(lambda x: moments.ewma(x, adjust=False, span=self.kwargs['slow']))
        self.macd = self.fast.apply(self.__macd_apply)
        self.signal = self.macd.apply(self.__signal_apply)
        return self.macd.apply(self.__final_apply)

    def __macd_apply(self, fast):
        item, column = fast.name
        return fast - self.slow[item][column]

    def __signal_apply(self, macd):
        return moments.ewma(macd, adjust=False, span=self.kwargs['span'])

    def __final_apply(self, macd):
        item, column = macd.name
        return macd - self.signal[item][column]


class RSI(Indicator):
    def __init__(self, *args, **kwargs):
        super(RSI, self).__init__(*args, **kwargs)
        if self.kwargs.get('com') is None:
            self.kwargs.setdefault('span', 27)

    def get_panel(self):
        return self.source_panel.apply(self.__apply)

    def __ewma(self, series):
        return moments.ewma(series, com=self.kwargs.get('com'), span=self.kwargs.get('span'))

    def __apply(self, series):
        delta = series.diff()
        u = delta.copy()
        d = delta.copy()
        u[delta < 0] = 0
        d[delta > 0] = 0
        rs = self.__ewma(u) / self.__ewma(d)
        return 1 - 2 / (1 + rs)


class StochasticK(Indicator):
    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.kwargs.setdefault('window', 5)

    def get_panel(self):
        return self.source_panel.apply(self.__apply)

    def __apply(self, series):
        high = moments.rolling_max(series, self.kwargs['window'])
        low = moments.rolling_min(series, self.kwargs['window'])
        stochastic_k = ((series - low) / (high - low)) * 2 - 1
        return stochastic_k.fillna(0)


class StochasticD(Indicator):
    def __init__(self, *args, **kwargs):
        super(StochasticD, self).__init__(*args, **kwargs)
        self.kwargs.setdefault('window', 5)
        self.kwargs.setdefault('ma', 3)

    def get_panel(self):
        return self.source_panel.apply(self.__apply)

    def __apply(self, series):
        high = moments.rolling_max(series, self.kwargs['window'])
        low = moments.rolling_min(series, self.kwargs['window'])
        stochastic_k = ((series - low) / (high - low)) * 2 - 1
        return moments.rolling_mean(stochastic_k.fillna(0), self.kwargs['ma'])


class Transforms(object):
    def __init__(self, market, source, __indicators, config):
        self.market = market
        self.db = market.db
        self.source = source
        self.name = source.name
        self.indicators = __indicators
        self.config = config
        self.array = self.__get_array()
        self.panel = None

    def __get_array(self):
        __array = []
        for name, __config in self.config.iteritems():
            klass = self.get_class(__config['name'])
            if __config.get('indicator'):
                i_name = __config['indicator']
                instance = klass(__config, name=name, source=self.source, panel=self.indicators.panel[i_name])
            else:
                instance = klass(__config, name=name, source=self.source)
            __array.append(instance)
        return __array

    @staticmethod
    def get_class(name):
        return {
            'chaikin': Chaikin,
            'chaikin_index': ChaikinIndex,
            'chaikin_osc': ChaikinOscillator,
            'cmi': ChoppyMarketIndex,
            'zigzag': ZigZag
        }[name]

    def create(self):
        self.panel = Panel({
            transform.name: transform.get()
            for transform in self.array
        }, dtype=self.market.dtype)

    def key(self, label=None):
        if label:
            return '/data/{0}/transforms/{1}'.format(self.name, label)
        else:
            return '/data/{0}/transforms'.format(self.name)

    def save(self):
        with self.db.get('r+') as db:
            for label in self.panel.axes[0]:
                db.put(self.key(label), self.panel[label])


class Transform(Indicator):
    def __init__(self, *args, **kwargs):
        super(Transform, self).__init__(*args, **kwargs)

    def get(self):
        self.log.info('[Transform] {0} {1}'.format(self.__class__.__name__, self.config))
        return self.get_dataframe()

    def get_dataframe(self):
        raise Exception('Transform Class not found: {0}'.format(self.__class__.__name__))


class Chaikin(Transform):
    def __init__(self, *args, **kwargs):
        super(Chaikin, self).__init__(*args, **kwargs)
        self.ma = self.kwargs.get('ma')

    def get_dataframe(self):
        return self.source_panel.groupby(lambda x: x, axis='items').apply(self.__apply)

    def __apply(self, panel):
        df = panel[panel.items[0]]
        numerator = (df['C'] - df['L']) - (df['H'] - df['C'])
        denom = (df['H'] - df['L'])
        chaikin_indicator = (numerator / denom) * df['PV']
        if self.ma:
            chaikin_indicator = moments.rolling_mean(chaikin_indicator, self.ma)
        return chaikin_indicator


class ChaikinIndex(Transform):
    def __init__(self, *args, **kwargs):
        super(ChaikinIndex, self).__init__(*args, **kwargs)
        self.sum = self.kwargs['sum']

    def get_dataframe(self):
        return self.source_panel.groupby(lambda x: x, axis='items').apply(self.__apply)

    def __apply(self, panel):
        df = panel[panel.items[0]]
        numerator = (df['C'] - df['L']) - (df['H'] - df['C'])
        denom = (df['H'] - df['L'])
        chaikin_indicator = (numerator / denom) * df['PV']
        return moments.rolling_sum(chaikin_indicator, self.sum) / moments.rolling_sum(df['PV'], self.sum)


class ChaikinOscillator(Transform):
    def __init__(self, *args, **kwargs):
        super(ChaikinOscillator, self).__init__(*args, **kwargs)
        self.long = self.kwargs['long']
        self.short = self.kwargs['short']

    def get_dataframe(self):
        return self.source_panel.groupby(lambda x: x, axis='items').apply(self.__apply)

    def __apply(self, panel):
        df = panel[panel.items[0]]
        numerator = (df['C'] - df['L']) - (df['H'] - df['C'])
        denom = (df['H'] - df['L'])
        chaikin_indicator = (numerator / denom) * df['PV']
        return moments.rolling_mean(chaikin_indicator, self.long) - moments.rolling_mean(chaikin_indicator, self.short)


class ChoppyMarketIndex(Transform):
    def __init__(self, *args, **kwargs):
        super(ChoppyMarketIndex, self).__init__(*args, **kwargs)
        self.periods = self.kwargs['periods']

    def get_dataframe(self):
        return self.source_panel.groupby(lambda x: x, axis='items').apply(self.__apply)

    def __apply(self, panel):
        df = panel[panel.items[0]]
        numerator = df['C'] - df['C'].shift(self.periods)
        denom = moments.rolling_max(df['H'], self.periods) - moments.rolling_max(df['L'], self.periods)
        return (numerator / denom)[self.periods:]


class ZigZag(Transform):
    def __init__(self, *args, **kwargs):
        super(ZigZag, self).__init__(*args, **kwargs)
        self.column = self.kwargs.get('column')

    def get_dataframe(self):
        if self.column:
            return self.source_panel.groupby(lambda x: x, axis='items').apply(self.__apply_column)
        else:
            return self.source_panel.groupby(lambda x: x, axis='items').apply(self.__apply)

    def __apply(self, panel):
        df = panel[panel.items[0]]
        last_col = 'H'
        last_col_series = df[last_col]
        ix_not_null = last_col_series[last_col_series.notnull()]
        if len(ix_not_null.index) == 0:
            self.log.error('All NaNs: {0}'.format(panel.items[0]))
            return
        df = df.ix[ix_not_null.index[0]:]
        df_mean = df.mean().to_dict()
        df_std = df.std().to_dict()

        index = [df.index[0]]
        values = [df[last_col][0]]

        for i, x in df.ix[1:].iterrows():
            use_col = 'L' if last_col == 'H' else 'H'
            value = x[use_col]
            diff = (value - df_mean[use_col]) / df_std[use_col] - (values[-1] - df_mean[last_col]) / df_std[last_col]
            sign_change = diff < 0 and last_col == 'H' or diff > 0 and last_col == 'L'
            if not sign_change and len(values) > 1:
                index[-1] = i
                values[-1] = x[last_col]
            elif abs(diff) > self.kwargs['sigma']:
                index.append(i)
                values.append(value)
                last_col = use_col

        if index[-1] != df.index[-1]:
            index.append(df.index[-1])
            values.append(df.ix[-1]['C'])
        # s = Series(values, index).reindex(df.index).interpolate()
        # plot(df['C'])
        # plot(s)
        # show()
        return Series(values, index).reindex(df.index).interpolate()

    def __apply_column(self, panel):
        series = panel[panel.items[0]][self.column]
        series = series.ix[series[series.notnull()].index]

        if len(series.index) == 0:
            self.log.error('All NaNs: {0}'.format(panel.items[0]))
            return

        s_mean = series.mean()
        s_std = series.std()

        index = [series.index[0]]
        values = [series[0]]

        for i, x in series.ix[1:].iteritems():
            last = series[index[-1]]
            sign_change = True if len(index) == 1 or (x - last) * (last - series[index[-2]]) < 0 else False
            diff = (x - s_mean) / s_std - (values[-1] - s_mean) / s_std

            if not sign_change and len(values) > 1:
                index[-1] = i
                values[-1] = x
            elif abs(diff) > self.kwargs['sigma']:
                index.append(i)
                values.append(x)

        if index[-1] != series.index[-1]:
            index.append(series.index[-1])
            values.append(series[-1])
        return Series(values, index).reindex(series.index).interpolate()