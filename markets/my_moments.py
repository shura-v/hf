from pandas.stats import moments
import numpy as np


def ewma(x, **kwargs):
    """
        @param x: Series, DataFrame
        @param kwargs: (com=None, span=None, halflife=None, min_periods=0, freq=None, time_rule=None, adjust=True)
        @return: Series, DataFrame
    """
    if kwargs.get('span') is None:
        kwargs['span'] = len(x) - 1
    values = moments.ewma(x, **kwargs)
    if len(values):
        return values[-1]
    return np.nan


def last(x, **kwargs):
    if len(x):
        return x[-1]
    else:
        return np.nan


def first(x, **kwargs):
    if len(x):
        return x[0]
    else:
        return np.nan


def mean(x, **kwargs):
    if len(x):
        return np.mean(x)
    else:
        return np.nan