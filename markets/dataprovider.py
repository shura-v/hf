from __future__ import division
from datetime import datetime, timedelta
import optparse
import socket
import multiprocessing as mp
from math import ceil

import numpy as np
from pandas import DataFrame, Series, Panel, Timestamp, DatetimeIndex
from pytz import timezone
import pandas as pd

from indexer import TimeIndex

from tools.tools import Access, Config, Logger, Path, yield_chunks
from database.db import Store


class DataProvider(object):
    def __init__(self, name=None, config=None, argv=(), create=False, log=None):
        self.options = self.__parse_options(argv)
        self.sub_folder = 'providers'
        self.name = self.options.name or name
        self.config = Config(self, config=config, create=create)
        self.sockets = self.config.get('sockets', 11)
        self.symbols = self.config['symbols']
        self.path = Path(self, self.sub_folder)
        self.timezone = timezone('EST5EDT')
        self.access = lambda *args, **kwargs: Access(self, *args, **kwargs)
        self.log = Logger(self, level=log)

        self.db = DB(self)
        self.dtype = np.float32
        self.days = self.config['days']
        self.socket = self.__get_socket()
        self.__now = datetime.now(tz=self.timezone)
        self.__end_dates = None
        self.__panel = None
        self.as_indexer = TimeIndex(self.config['as_indexer'], init=False) if self.config['as_indexer'] else False
        self.log_info()

    def __getitem__(self, __tuple):
        if type(__tuple) is slice:
            key, col = [None] * 2
        elif len(__tuple) == 2:
            key, col = __tuple
        else:
            key = __tuple
            col = None
        if self.__panel is None:
            with self.db.get('r') as db:
                self.__panel = db[self.db.key('data')]
        if key is None:
            return self.__panel
        elif col is None:
            return self.__panel[key]
        else:
            return self.__panel[key][col]

    def __get_socket(self):
        sockets = {
            'HIT': HIT,
            'HDT': HDT
        }
        return sockets[self.config['socket']](self)

    @staticmethod
    def __parse_options(argv=()):
        argv = list(argv)
        parser = optparse.OptionParser()
        parser.add_option("-n", "--name", dest="name")
        parser.add_option("-m", "--no_multiprocessing", action="store_true", dest="no_multiprocessing", default=False)
        opts, args = parser.parse_args(argv)
        return opts

    def drop_key(self, key):
        if key in self.end_dates:
            end_dates = self.end_dates.drop(key)
            self.__save_end_dates(end_dates, combine=False)
        with self.db.get('r+') as db:
            __key = self.db.key('data')
            panel = db[__key]
            if key in panel.axes[0]:
                db.put(__key, panel.drop(key, axis='items'))

    @property
    def between_time(self):
        cfg = self.config.get('between_time')
        return cfg if cfg is not None else [None] * 2

    def end_date(self, q):
        if self.end_dates is None:
            return self.first_date
        else:
            return self.end_dates[q] if q in self.end_dates else self.first_date

    @property
    def end_dates(self):
        key = self.db.key('end_dates')
        if self.__end_dates is None:
            with self.db.get('r+') as db:
                if key in db:
                    self.__end_dates = db[key]
                else:
                    self.__end_dates = None
        return self.__end_dates

    @property
    def first_date(self):
        return Timestamp(self.__now - timedelta(days=self.days))

    @property
    def updated(self):
        if self.config.get('updated') is None:
            raise Exception('No updated')
        return Timestamp(self.config.get('updated'))

    @updated.setter
    def updated(self, ts):
        self.config['updated'] = str(ts.replace(tzinfo=None))
        self.config.save()

    def log_info(self):
        self.log.info('Symbols: {0}'.format(len(self.symbols)))

    def save_end_dates(self, __dict):
        self.log.info('Saving dates...')
        ___dict = {}
        for symbol, df in __dict.iteritems():
            if len(df.index):
                ___dict.update({symbol: df.index[-1]})
            else:
                self.log.debug('Empty dataframe: {0}'.format(symbol))
        end_dates = Series(___dict)
        self.__save_end_dates(end_dates, combine=True)

    def __save_end_dates(self, series, combine=True):
        key = self.db.key('end_dates')
        with self.db.get('r+') as db:
            if key in db and combine:
                series = series.combine_first(db[key])
            db.put(key, series)

    def update_end_dates(self):
        with self.db.get('r+') as db:
            end_dates = Series({symbol: df.index[-1] for symbol, df in db['panel'].iteritems()})
            key = self.db.key('end_dates')
            db.put(key, end_dates)

    def update(self):
        self.log.info('Downloading...')
        self.updated = datetime.now(tz=self.timezone)
        __dict = self.socket.download()
        self.save_end_dates(__dict)
        panel = Panel(__dict, dtype=self.dtype)
        self.db.update(panel)
        self.log.info('Ponyachka molodets!')

    def __repr__(self):
        return '{0}\n{1}'.format(self.name, self.__class__.__name__)


class DB(Store):
    __keys = {
        'end_dates': '/end_dates',
        'data': '/panel'
    }

    def __init__(self, *args, **kwargs):
        super(DB, self).__init__(*args, **kwargs)
        self.provider = self.instance
        self.log = self.provider.log
        self.timezone = self.provider.timezone

    def key(self, key):
        return self.__keys[key]

    @property
    def first_date(self):
        return self.provider.first_date.strftime('%Y-%m-%d %H:%M')

    def update(self, panel, key=None):
        self.log.info('Updating DB...')
        if panel.shape[1] == 0:
            self.log.info('Empty Panel')
            return
        if key is None:
            key = self.__keys['data']
        with self.get('r+') as db:
            if key in db:
                first_date = panel.axes[1][-1] - timedelta(days=self.provider.days)
                db_panel = db[key].ix[:, first_date:]
                new_index = [
                    db_panel.axes[i].union(panel.axes[i])
                    for i in range(3)
                ]
                db_panel = db_panel.reindex(*new_index)
                db_panel.update(panel)
                panel = db_panel
            db.put(key, panel)


class Socket(object):
    def __init__(self, provider):
        self.provider = provider
        self.db = provider.db
        self.name = ''
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.host = "127.0.0.1"
        self.port = 9100
        self.sock = None
        self.log = self.provider.log
        self.chunk_size = self.provider.config.get('chunk_size', 10)
        self.sockets = provider.sockets

    columns = []

    __defaults = {
        'Interval': '60',
        'BeginDate BeginTime': '%Y%m%d %H%M%S',
        'EndDate EndTime': '%Y%m%d %H%M%S',
        'MaxDataPoints': '',
        'BeginFilterTime': '%H%M%S',
        'EndFilterTime': '%H%M%S',
        'DataDirection': '1',
        'RequestID': '',
        'DatapointsPerSend': '',
        'IntervalType': ''
    }

    __kwargs = {
        'symbol': 'Symbol',
        'i': 'Interval',
        'dt_begin': 'BeginDate BeginTime',
        'dt_end': 'EndDate EndTime',
        'ft_begin': 'BeginFilterTime',
        'ft_end': 'EndFilterTime',
        'direction': 'DataDirection',
        't': 'IntervalType'
    }

    keys = []

    def set_defaults(self, **kwargs):
        cfg_symbol = kwargs['symbol'].split('||')
        if len(cfg_symbol) > 1:
            symbol, options = cfg_symbol
            kwargs['symbol'] = symbol
            options = self.options(options)
            kwargs.update(options)
        freq = self.provider.config['freq']
        interval = int(freq) * 60
        kwargs.setdefault('i', interval)
        kwargs.setdefault('dt_begin', self.provider.end_date(kwargs['symbol']))
        kwargs.setdefault('direction', 1)
        return kwargs

    @staticmethod
    def options(string):
        return dict([
            [v for v in s.split('=')]
            for s in string.split('|')
        ])

    def config(self, **kwargs):
        cfg = dict()
        kwargs = self.set_defaults(**kwargs)
        if self.provider.between_time[0]:
            kwargs.setdefault('ft_begin', Timestamp(self.provider.between_time[0]))
        if self.provider.between_time[1]:
            kwargs.setdefault('ft_end', Timestamp(self.provider.between_time[1]))
        for __k, __v in kwargs.iteritems():
            __key = self.__kwargs[__k]
            if type(__v) is Timestamp:
                cfg[__key] = __v.strftime(self.__defaults[__key])
            elif __v is None:
                cfg[__key] = ''
            else:
                cfg[__key] = str(__v)
        return cfg

    def message(self, **kwargs):
        cfg = self.config(**kwargs)
        return '{0},'.format(self.name) + ','.join([cfg[__k] if __k in cfg.keys() else '' for __k in self.keys]) + '\n'

    def send(self, message):
        if self.sock is None:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
            self.sock.connect((self.host, self.port))
        self.sock.sendall(message)
        return self

    def download(self, end=None):
        messages = [(self.message(symbol=symbol, dt_end=end), symbol)
                    for symbol in self.provider.symbols]
        chunks = [
            (self.host, self.port, chunk)
            for chunk in np.array_split(messages, ceil(len(messages) / self.chunk_size))
        ]

        __dict = {}
        __counter = 0
        for processes in yield_chunks(chunks, self.sockets):
            pool = mp.Pool(processes=len(processes))
            results = pool.map(send, processes)
            pool.close()
            pool.join()
            res, counter = self.results(results, __counter)
            __counter = counter
            self.log.info('Completed: [{0:4}/{1:4}]'.format(counter, len(self.provider.symbols)))
            __dict.update(res)
        return __dict

    def results(self, results, counter):
        __dict = {}
        for chunk in results:
            for result in chunk:
                arr, symbol = result
                counter += 1
                if type(arr) is str:
                    self.log.error('[{0:4}/{1:4}] {2}: {3}'.format(counter, len(self.provider.symbols), symbol, arr))
                    continue
                __dict[symbol] = self.dataframe(arr)
                __len = len(__dict[symbol])
                self.log.debug('[{0:4}/{1:4}] {2:20} [{3:7}]'.format(counter, len(self.provider.symbols), symbol, __len))
        return __dict, counter

    def dataframe(self, arr):
        index = self.index(arr[:, 0:1])
        try:
            df = DataFrame(arr[:, 1:].astype(self.provider.dtype), index=index, columns=self.columns)
            if self.provider.as_indexer:
                index = self.provider.as_indexer.cleanup(df.index)
                df.reindex(index)
                move = self.provider.as_indexer.config['move']
                if move:
                    df = self.provider.as_indexer.move(df, move)
            return df
        except IndexError:
            print arr, arr.shape
            return None

    @staticmethod
    def index(arr):
        return pd.to_datetime(arr.ravel())


class HIT(Socket):
    """
        [Symbol] - Required - Max Length 30 characters.
        [Interval] - Required - The interval in seconds.
        [BeginDate BeginTime] - Required if [EndDate EndTime] not specified - Format CCYYMMDD HHmmSS - Earliest date/time to receive data for.
        [EndDate EndTime] - Required if [BeginDate BeginTime] not specified - Format CCYYMMDD HHmmSS - Most recent date/time to receive data for.
        [MaxDatapoints] - Optional - the maximum number of datapoints to be retrieved.
        [BeginFilterTime] - Optional - Format HHmmSS - Allows you to specify the earliest time of day (Eastern) for which to receive data.
        [EndFilterTime] - Optional - Format HHmmSS - Allows you to specify the latest time of day (Eastern) for which to receive data.
        [DataDirection] - Optional - '0' (default) for "newest to oldest" or '1' for "oldest to newest".
        [RequestID] - Optional - Will be sent back at the start of each line of data returned for this request.
        [DatapointsPerSend] - Optional - Specifies the number of datapoints that IQConnect.exe will queue before attempting to send across the socket to your app.
        [IntervalType] - Optional - 's' (default) for time intervals in seconds, 'v' for volume intervals, 't' for tick intervals

        TV = Total Volume
        PV = Period Volume
    """
    columns = ['H', 'L', 'O', 'C', 'TV', 'PV']

    keys = ['Symbol', 'Interval', 'BeginDate BeginTime', 'EndDate EndTime', 'MaxDataPoints',
            'BeginFilterTime', 'EndFilterTime', 'DataDirection', 'RequestID', 'DatapointsPerSend', 'IntervalType']

    def __init__(self, *args, **kwargs):
        super(HIT, self).__init__(*args, **kwargs)
        self.name = 'HIT'
        self.dataframes = {}

    def dataframe(self, arr):
        df = DataFrame(arr[:, 1:].astype(self.provider.dtype), index=self.index(arr[:, 0:1]), columns=self.columns)
        # if 'TV' in df.columns:
        #     df.drop('TV', axis=1, inplace=True)
        df['OHLC'] = df[['O', 'H', 'L', 'C']].mean(axis=1)
        df['HLC'] = df[['H', 'L', 'C']].mean(axis=1)
        df['HL'] = df[['H', 'L']].mean(axis=1)
        if self.provider.as_indexer is not False:
            print 'cleaning up...'
            index = self.provider.as_indexer.cleanup(df.index)
            df = df.reindex(index)
            move = self.provider.as_indexer.config.get('move')
            if move:
                print 'moving...'
                df = self.provider.as_indexer.move(df, move)
        return df


class HDT(HIT):
    """
        [Symbol] - Required - Max Length 30 characters.
        [BeginDate] - Required if [EndDate] not specified - Format CCYYMMDD - Earliest date to receive data for.
        [EndDate] - Required if [BeginDate] not specified - Format CCYYMMDD - Most recent date to receive data for.
        [MaxDatapoints] - Optional - the maximum number of datapoints to be retrieved.
        [DataDirection] - Optional - '0' (default) for "newest to oldest" or '1' for "oldest to newest".
        [RequestID] - Optional - Will be sent back at the start of each line of data returned for this request.
        [DatapointsPerSend] - Optional - Specifies the number of datapoints that IQConnect.exe will queue before attempting to send across the socket to your app.

        PV = Period Volume
        OI = Open Interest
    """
    columns = ['H', 'L', 'O', 'C', 'PV', 'OI']

    keys = ['Symbol', 'BeginDate', 'EndDate', 'MaxDataPoints', 'DataDirection', 'RequestID', 'DataPointsPerSend']

    __defaults = {
        'BeginDate': '%Y%m%d',
        'EndDate': '%Y%m%d',
        'MaxDataPoints': '',
        'DataDirection': '1',
        'RequestID': '',
        'DatapointsPerSend': ''
    }

    __kwargs = {
        'symbol': 'Symbol',
        'dt_begin': 'BeginDate',
        'dt_end': 'EndDate',
        'direction': 'DataDirection'
    }

    def __init__(self, *args, **kwargs):
        super(HDT, self).__init__(*args, **kwargs)
        self.name = 'HDT'
        self.freq = None

    def set_defaults(self, **kwargs):
        kwargs.setdefault('dt_begin', self.provider.end_date(kwargs['symbol']))
        kwargs.setdefault('direction', 1)
        return kwargs

    def config(self, **kwargs):
        cfg = dict()
        kwargs = self.set_defaults(**kwargs)
        for __k, __v in kwargs.iteritems():
            __key = self.__kwargs[__k]
            if type(__v) is Timestamp:
                cfg[__key] = __v.strftime(self.__defaults[__key])
            elif __v is None:
                cfg[__key] = ''
            else:
                cfg[__key] = str(__v)
        return cfg

    @staticmethod
    def index(arr):
        index = DatetimeIndex(arr.ravel())
        return DatetimeIndex([
            x.replace(hour=0, minute=0, second=0, microsecond=0)
            for x in index
        ])


def send(args):
    host, port, chunk = args
    result = []
    for msg, symbol in chunk:
        result.append(get_symbol(host, port, msg, symbol))
    return result


def get_symbol(host, port, msg, symbol, tries=0, max_tries=3):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, port))
    sock.send(msg)
    data = read(sock)
    sock.shutdown(0.0)
    if type(data) is str and data.startswith('Could not connect to History socket.'):
        if tries < max_tries:
            return get_symbol(host, port, msg, symbol, tries + 1)
        else:
            data += '[tries: {0}]'.format(tries)
    return data, symbol


def read(sock, receive_buffer=4096):
    b = []
    while True:
        chunk = sock.recv(receive_buffer)
        if chunk.startswith('E,'):
            return chunk.split(',')[1]
        ix = chunk.find('!')
        if ix == -1:
            b.append(chunk)
        elif ix == 0:
            break
        else:
            b.append(chunk[:ix])
            break
    b = ''.join(b)
    if b == '':
        return 'EMPTY'
    b = b.split(',\r\n')[:-1]
    return np.array([line.split(',') for i, line in enumerate(b)])