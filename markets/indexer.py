from datetime import datetime

from pytz import timezone
from pandas import DatetimeIndex, date_range, Series, Timestamp
from numpy import argmax, arange

from tools.tools import Access, Config, Path, Logger
from database.db import Store
from time import time


class TimeIndex(object):
    def __init__(self, name=None, config=None, freq=None, at_times=None, init=True):
        self.exchange = name

        self.name = '__indexes'
        self.sub_folder = ''

        self.config = Config(self, config=config)[self.exchange]
        self.freq = freq or '1T'
        self.path = Path(self)
        self.db = Store(self)

        self.key = '{0}/{1}'.format(self.exchange, self.freq)
        self.access = lambda *args, **kwargs: Access(self, *args, **kwargs)

        self.log = Logger(self, console=False, f='ERROR')
        self.years = self.config['years']
        self.format = self.config['format']
        self.tz_str = self.config['timezone']
        self.tz = timezone(self.tz_str)

        self.holidays = self.config.get('holidays')
        if init:
            self.series = self.__get_series()
            if at_times:
                self.series = self.__at_times(at_times)
                if len(self.series) == 0:
                    times = [t.strftime('%H:%M') for t in self.__get_series().groupby(lambda x: x.time()).first().index]
                    raise IndexError('Empty index at times {0}\nUse any of these: {1}'.format(at_times, times))
            self.array = self.series.index.to_pydatetime()
            self.dict = {dt: i for i, dt in enumerate(self.array)}

    @property
    def week(self):
        return self.config.get('week') if 'week' in self.config else [True] * 7

    @property
    def index(self):
        return self.series.index

    def __get_series(self):
        with self.db.get('r+') as store:
            if self.key in store:
                return store[self.key]
        return self.__create()

    def __at_times(self, times):
        source = self.series.index.copy()
        index = DatetimeIndex([])
        for t in times:
            index = index.union(source[source.indexer_at_time(t)])
        return Series(range(len(index)), index)

    def __create(self):
        index = self.initial(*self.years)
        index = self.cleanup(index)
        s = Series(arange(len(index)), index)
        if self.config.get('move'):
            s = self.move(s, self.config['move'])
        with self.access('r+'):
            with self.db.get('r+') as db:
                db.put(self.key, s)
                db.get_storer(self.key).attrs['freq'] = self.freq
                return s

    def initial(self, year_start, year_end):
        start = datetime(year=year_start, month=1, day=1)
        end = datetime(year=year_end + 1, month=1, day=1)
        return date_range(start=start, end=end, freq=self.freq)[1:]

    def __days(self):
        years = self.config['years']
        dt_start = datetime(year=years[0], month=1, day=1)
        dt_end = datetime(year=years[1] + 1, month=1, day=1)
        return date_range(dt_start, dt_end, freq='D')[1:]

    def cleanup(self, index):
        index = self.cleanup_week(index, self.week, self.freq)
        if self.holidays:
            index = self.cleanup_holidays(index, self.holidays, self.freq)
        return index

    @staticmethod
    def cleanup_week(index, week, freq=None):
        for i, day in enumerate(week):
            if day is True:
                continue
            elif day is False:
                index = index[index.dayofweek != i]
            elif type(day) is list and freq != 'D':
                day_index = index[index.dayofweek == i]
                index = index[index.dayofweek != i]
                for interval in day:
                    current = day_index.copy()
                    current = TimeIndex.__keep(current, *interval)
                    index = index.union(current)
        return index.order(ascending=True)

    @staticmethod
    def cleanup_holidays(index, holidays, freq=None):
        for dt in holidays:
            if type(dt) is str:
                index = index[index.date != Timestamp(dt).date()]
            elif type(dt) is dict and freq != 'D':
                __dt = Timestamp(dt['date']).date()
                dt_index = index[index.date == __dt]
                index = index[index.date != dt]
                if dt.get('start'):
                    dt_index = TimeIndex.__keep(dt_index, dt['start'], None)
                if dt.get('end'):
                    dt_index = TimeIndex.__keep(dt_index, None, dt['end'])
                for interval in dt:
                    current = dt_index.copy()
                    current = TimeIndex.__keep(current, *interval)
                    index = index.union(current)
        return index.order(ascending=True)

    @staticmethod
    def move(data, config):
        for c in config:
            __data = data[data.index.dayofweek == c['dayofweek']]
            if len(__data):
                __data = __data.shift(periods=c['periods'], freq=c['freq'])
                data = data[data.index.dayofweek != c['dayofweek']]
                data = data.append(__data).sort_index()
        return data

    @staticmethod
    def __keep(index, start, end):
        if start is None:
            start = '00:00'
        if end is None:
            end = '23:59:59.999999'
        return index[index.indexer_between_time(start, end, include_start=True, include_end=True)]

    def between(self, start, end):
        start = self.dict[self.index.asof(start)]
        end = self.dict[self.index.asof(end)]
        return self.index[start:end + 1]

    def ceil(self, dt):
        return self.array[argmax(self.array >= dt)]

    def floor(self, dt):
        return self.array[::-1][argmax(self.array[::-1] <= dt)]

    def shift(self, dt, shift=0):
        return self.array[self.dict[dt] + shift]

    def between_time(self, start=None, end=None, include_start=True, include_end=True):
        return self.index[self.index.indexer_between_time(start, end, include_start, include_end)]

    def at_times(self, t):
        return TimeIndex(self.exchange, freq=self.freq, at_times=t)

    def __len__(self):
        return len(self.index)

    def __iter__(self):
        return self.array.__iter__()

    def __repr__(self):
        return self.series.__str__()

    def __getitem__(self, __slice):
        start = __slice.start
        stop = __slice.stop
        if type(start) is str:
            start = Timestamp(start)
        if type(stop) is str:
            stop = Timestamp(stop)
        return self.between(start, stop)