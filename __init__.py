# -*- coding: utf-8 -*-
from __future__ import division

# print unicode('%FIRSTNAME_UPPERCASE%, Поздравляем Вас!').upper()
#todo-done: инициализация весов: http://take.ms/d5p9VK, http://www.cs.toronto.edu/~hinton/absps/momentum.pdf
#todo-done: сохранение сети
#todo-done: цикл с записью контекста и вызовом обучения
#todo-done: ПРОВЕРИТЬ CONTEXT.ADD в FORWARD
#todo-done: 23.10 Batches
#todo-done: 24.10 PLOT gradients
#todo-done: 24.10 Stochastic Gradient Descent optimizer with mini-batches:
#       http://www.youtube.com/playlist?list=PLuaP0x9HS2b1wiRVVtpDMkSrTtSC6R4Ad
#       http://itee.uq.edu.au/~mikael/papers/rn_dallas.pdf
#       https://class.coursera.org/neuralnets-2012-001/lecture/63
#todo-done: 25.10 SGD with Nesterov Momentum
#       http://www.cs.toronto.edu/~hinton/absps/momentum.pdf,
#todo-done: 23.11 "M", "N" in context
#todo-done: 24.11 backprop V through N
#todo-done: 28.11 Ask Sutskever about verifying Gv with finite difference
#todo-done: 28.11 check J_N * v !!!
#todo-done: 28.11 rop get ry_last = H_LoM * J_N * v
#todo-done: 28.11 backprop ry_last through N to obtain J_N' * H_LoM * J_N * v
#todo-done: Hessian Vector products
#       http://bcl.hamilton.ie/~barak/papers/nc-hessian.pdf
#todo-done: 08.12 CG
#todo-done: 08.12 Hessian Free optimizer
#todo-done: 12.12 Damped HF
#todo-done: Read about regularization:
#       1. Regularization: weight decay in first order gradient descent: w_1 = (w_0 - lambda*w_0) - alpha*grad
#       2. Damping: approaching first order optimization when lambda is large: (H + lambda*I)^-1 * J
#       3. Structural damping: (H + lambda*H)^-1 * J
#todo-done: 15.12 Rho + Fixed Damping
#todo-done: 15.12 x0 in Batch state
#todo-done: 16.12 Min total steps (lengths changed)
#todo-done: 21.12 Batch States (lambda, x0)
#todo-done: 21.12 CG backtracking
#todo-done: 22.12 Line search
#todo-done: 22.12 Reject epoch

# todo: Regularization (add "alpha" to damping)

# todo-maybe: CG preconditioner <- READ CAREFULLY

# import network
# import os, time
#
# params = {
#     "plot": {
#         "results": {
#             "learn": 0,
#             "test": 0,
#             "production": 0
#         },
#         "opacity": 0.7,
#         "type": "bar",  # "normal" | "bar" | False
#         "descent": True,
#         "gradients": False,  # layer
#         "distributions": {
#             "normed": True
#         }
#     },
#     "check": {
#         "gv": 0,
#         "gradient": 0
#     },
#     "timing": True
# }
#
#
# def init_networks(db, json):
#     for f in os.listdir(json):
#         paths = {
#             "db": db,
#             "json": os.path.join(json, f)
#         }
#         name = f.split('.')[0]
#         net = network.RNN(paths, params, name)
#         if net.config:
#             print "Layers:", [int(layer["neurons"]) for layer in net.config["layers"]]
#         else:
#             print "Network [%s] - no config" % name